#include <thread>
#include <topo/topo.h>

#include <glm/gtc/quaternion.hpp>
#include <filesystem>
#include <queue>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <utils/scene_dyn.h>

static void check_working_dir() {
    bool hasBin = false;
    bool hasAssets = false;
    auto cwd = std::filesystem::current_path();

    while (!hasBin || !hasAssets) {
        hasBin = false;
        hasAssets = false;
        for (const auto entry : std::filesystem::directory_iterator(cwd)) {
            if (entry.is_directory()) {
                if (entry.path().filename() == "bin") {
                    hasBin = true;
                }
                if (entry.path().filename() == "assets") {
                    hasAssets = true;
                }
            }
        }

        if (hasBin && hasAssets) {
            break;
        }

        auto parentPath = cwd.parent_path();
        if (cwd == parentPath) {
            fprintf(stderr, "Bad working directory\n");
            abort();
        }

        cwd = parentPath;
    }

    std::filesystem::current_path(cwd);
}

int main(int argc, char **argv) {
    check_working_dir();

    topo_instance_t topoInstance = nullptr;
    topo_instance_descriptor topoDescriptor = {
        "test_app",
        1280,
        720,
        1,
    };

    topo_create(&topoInstance, &topoDescriptor);
    topo_mesh_handle hMesh;

    {
        int lenWidth, lenHeight, numChannels;
        auto *image = stbi_load("assets/thumbs-up-cat.png", &lenWidth, &lenHeight, &numChannels, 4);
        topo_image_descriptor testImage;
        testImage.format = kTopo_FormatRGBA32_SRGB;
        testImage.width = lenWidth;
        testImage.height = lenWidth;
        testImage.id = "assets/thumbs-up-cat.png";
        topo_borrow_memory_ex(topoInstance, lenWidth * lenHeight * 4, (uint8_t **)&testImage.pData, image);
        topo_image_t handle;
        topo_create_image(topoInstance, &testImage, &handle);
        stbi_image_free(image);

        topo_material_descriptor testMaterial;
        testMaterial.kind = kTopoMaterialKind_UnlitTextured;
        testMaterial.unlitTextured.imageBaseColor = handle;
        testMaterial.id = "test";
        topo_material_t hMaterial;
        topo_create_material(topoInstance, &testMaterial, &hMaterial);

        const glm::vec3 position[]
            = { { -1.0f, -1.0f, 0.0f }, { 1.0f, -1.0f, 0.0f }, { -1.0f, 1.0f, 0.0f }, { 1.0f, 1.0f, 0.0f } };
        const glm::vec3 tangent[] = { { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 } };
        const glm::vec3 bitangent[] = { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } };
        const glm::vec3 normal[] = { { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 } };
        const glm::vec2 texcoord[] = { { 0.0f, 1.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1.0f, 0.0f } };
        const uint16_t index[] = { 0, 2, 1, 2, 3, 1 };

        TopoMeshDescriptor *dMesh;
        topo_borrow_memory_ex(topoInstance, &dMesh);
        topo_borrow_memory_ex(topoInstance, &dMesh->id, "test_mesh");

        TopoVertexBuffer *buffers;
        topo_borrow_memory_ex(topoInstance, 5, &buffers);

        // Position
        topo_borrow_memory_ex(topoInstance, sizeof(position), (uint8_t **)&buffers[0].pVertexData, position);
        buffers[0].sizVertexData = sizeof(position);
        buffers[0].sizVertexStride = 3 * sizeof(float);
        // Tangents
        topo_borrow_memory_ex(topoInstance, sizeof(tangent), (uint8_t **)&buffers[1].pVertexData, tangent);
        buffers[1].sizVertexData = sizeof(tangent);
        buffers[1].sizVertexStride = 3 * sizeof(float);
        // Bitangents
        topo_borrow_memory_ex(topoInstance, sizeof(bitangent), (uint8_t **)&buffers[2].pVertexData, bitangent);
        buffers[2].sizVertexData = sizeof(bitangent);
        buffers[2].sizVertexStride = 3 * sizeof(float);
        // Normals
        topo_borrow_memory_ex(topoInstance, sizeof(normal), (uint8_t **)&buffers[3].pVertexData, normal);
        buffers[3].sizVertexData = sizeof(normal);
        buffers[3].sizVertexStride = 3 * sizeof(float);
        // Texcoords
        topo_borrow_memory_ex(topoInstance, sizeof(texcoord), (uint8_t **)&buffers[4].pVertexData, texcoord);
        buffers[4].sizVertexData = sizeof(texcoord);
        buffers[4].sizVertexStride = 2 * sizeof(float);

        dMesh->numVertexBuffers = 5;
        dMesh->pVertexBuffers = buffers;

        TopoVertexAttribute *attributes;
        topo_borrow_memory_ex(topoInstance, 5, &attributes);
        attributes[0].idxVertexBuffer = 0;
        attributes[0].offset = 0;
        attributes[0].semantic = TopoVES_Position;
        attributes[0].type = TopoVET_F32x3;
        attributes[1].idxVertexBuffer = 1;
        attributes[1].offset = 0;
        attributes[1].semantic = TopoVES_Tangent;
        attributes[1].type = TopoVET_F32x3;
        attributes[2].idxVertexBuffer = 2;
        attributes[2].offset = 0;
        attributes[2].semantic = TopoVES_Bitangent;
        attributes[2].type = TopoVET_F32x3;
        attributes[3].idxVertexBuffer = 3;
        attributes[3].offset = 0;
        attributes[3].semantic = TopoVES_Normal;
        attributes[3].type = TopoVET_F32x3;
        attributes[4].idxVertexBuffer = 4;
        attributes[4].offset = 0;
        attributes[4].semantic = TopoVES_Texcoord;
        attributes[4].type = TopoVET_F32x2;
        dMesh->numAttributes = 5;
        dMesh->pAttributes = attributes;

        topo_submesh *submeshes;
        topo_borrow_memory_ex(topoInstance, 1, &submeshes);
        submeshes[0].indexBuffer.numIndices = 6;
        submeshes[0].indexBuffer.type = topo_index_buffer::UINT16;
        topo_borrow_memory_ex(
            topoInstance, sizeof(index), (uint8_t **)&submeshes[0].indexBuffer.pIndices, index);
        submeshes[0].isIndexed = true;
        submeshes[0].primitiveTopology = kTopoPT_TriangleList;
        submeshes[0].material = hMaterial;
        dMesh->numSubmeshes = 1;
        dMesh->pSubmeshes = submeshes;

        topo_create_mesh(topoInstance, dMesh, &hMesh);
    }

    auto imScene = makeDynScene(topoInstance);

    topo_transform templateTransform;
    templateTransform.rotation[0] = 1;
    templateTransform.rotation[1] = 0;
    templateTransform.rotation[2] = 0;
    templateTransform.rotation[3] = 0;
    templateTransform.scale[0] = 1;
    templateTransform.scale[1] = 1;
    templateTransform.scale[2] = 1;
    templateTransform.position[0] = 0;
    templateTransform.position[1] = 0;
    templateTransform.position[2] = -1;

    topo_transform transformCamera;
    transformCamera.rotation[0] = 1;
    transformCamera.rotation[1] = 0;
    transformCamera.rotation[2] = 0;
    transformCamera.rotation[3] = 0;
    transformCamera.scale[0] = 1;
    transformCamera.scale[1] = 1;
    transformCamera.scale[2] = 1;
    transformCamera.position[0] = 0;
    transformCamera.position[1] = 0;
    transformCamera.position[2] = 2;

    TopoLightSource ambientLight = {};
    ambientLight.kind = TopoLSK_Ambient;
    ambientLight.color[0] = 1;
    ambientLight.color[1] = 1;
    ambientLight.color[2] = 1;
    ambientLight.intensity = 10;

    TopoCamera mainCamera = {};
    mainCamera.transform = transformCamera;
    mainCamera.fieldOfView = glm::radians(90.0f);
    mainCamera.nearClip = 0.01f;
    mainCamera.farClip = 1024.0f;

    float turns = 0.0f;
    bool exiting = false;
    while (!exiting) {
        if (imScene->beginScene()) {
            imScene->beginObjectGroup();
            imScene->addToObjectGroup(ambientLight);
            for (float z = -1; z > -1024; z -= 1.0f) {
                for (int x = -1; x <= 1; x += 1) {
                    auto transform = templateTransform;
                    transform.position[0] = x;
                    transform.position[2] = z;
                    transform.scale[0] = 0.25f;
                    transform.scale[1] = 0.25f;
                    transform.scale[2] = 1.0f;
                    imScene->addToObjectGroup(hMesh, transform);
                }
            }
            imScene->endObjectGroup();

            imScene->addCamera(mainCamera);
            imScene->setMainCamera(0);

            imScene->submitScene();
        }

        turns += 1.0f / 120.0f;
        auto qRot = glm::quat(glm::vec3(0.0f, turns * 2 * glm::pi<float>(), 0.0f));
        templateTransform.rotation[0] = qRot.w;
        templateTransform.rotation[1] = qRot.x;
        templateTransform.rotation[2] = qRot.y;
        templateTransform.rotation[3] = qRot.z;

        std::this_thread::sleep_for(std::chrono::milliseconds(16));
        topo_exiting(topoInstance, &exiting);
    }

    imScene.reset();
    topo_destroy(&topoInstance);
    return 0;
}