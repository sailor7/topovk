#pragma once

#include <arena.h>
#include <xxhash.h>

#include <tracy/Tracy.hpp>

struct Id {
    size_t lenId;
    const char *id;
    XXH64_hash_t hash;
};

XXH64_hash_t hash_id(XXH64_hash_t hashSeed, const char *id, size_t len);

template <typename T> struct ListNode {
    T *next = nullptr;
    Id id = { 0, nullptr, 0 };
};

template <typename T> bool lookup_node(XXH64_hash_t hashSeed, const char *id, T *start, T **out_node) {
    ZoneScoped;
    *out_node = nullptr;
    auto lenId = strlen(id);
    auto idHash = hash_id(hashSeed, id, lenId);
    T *cur = start;

    while (cur != nullptr) {
        if (cur->id.hash == idHash && cur->id.lenId == lenId && strncmp(cur->id.id, id, lenId) == 0) {
            break;
        }
        cur = cur->next;
    }

    if (cur == nullptr) {
        return false;
    }

    *out_node = cur;
    return true;
}

template <typename T> bool lookup_node(XXH64_hash_t hashSeed, const Id &id, T *start, T **out_node) {
    ZoneScoped;
    *out_node = nullptr;
    T *cur = start;

    while (cur != nullptr) {
        if (cur->id.hash == id.hash && cur->id.lenId == id.lenId
            && memcmp(cur->id.id, id.id, id.lenId) == 0) {
            break;
        }
        cur = cur->next;
    }

    if (cur == nullptr) {
        return false;
    }

    *out_node = cur;
    return true;
}

void create_ids(
    arena_t *arena,
    XXH64_hash_t hashSeed,
    size_t numIds,
    const char *const *id_strings,
    Id *out_ids);
