#include "vk_shaders.h"
#include "list.h"
#include "vk_buffers.h"
#include "vk_instance.h"
#include <arena.h>

#include <fmt/core.h>
#include <new>

#include <shaderc/shaderc.h>

#include <tracy/Tracy.hpp>

#define TAG "[topo::shaders] "

struct ShaderModuleNode : ListNode<ShaderModuleNode> {
    VkShaderModule handle;
};

struct ShaderPipelineNode : ListNode<ShaderPipelineNode> {
    ShaderPipelineDescriptor *descriptor;

    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
    uint32_t numDescriptorSets;
    VkDescriptorSetLayout descriptorSetLayouts[4];

    size_t numUniformBuffers = 0;
    // Per-frame uniform buffers shared by all invocations of this shader
    BmUniformBuffer *uniformBuffers = nullptr;

    size_t numDrawUniformBuffers[NUM_MAX_FRAMES_IN_FLIGHT] = { 0, 0 };
    arena_t arenaDrawUniformBuffers[NUM_MAX_FRAMES_IN_FLIGHT]
        = { arena_t::kilobytes(64), arena_t::kilobytes(64) };
};

struct PassInfo {
    size_t numColorAttachments;
};

struct PassNode : ListNode<PassNode> {
    PassDescriptor descriptor;
    PassInfo info;

    VkRenderPass handle;
};

struct ShaderManager {
    struct topo_instance *topo;

    // For data that is persistent for the entire lifetime of the SM
    arena_t arenaPersistent = { arena_t::kilobytes(128) };
    // For temporary data
    arena_t arenaTemp = { arena_t::megabytes(8) };

    XXH64_hash_t hashSeed;

    ShaderModuleNode *modules = nullptr;
    ShaderPipelineNode *pipelines = nullptr;
    PassNode *passes = nullptr;
};

static void create_ids(ShaderManager_t sm, size_t numIds, const char *id_strings[], Id *out_ids) {
    create_ids(&sm->arenaPersistent, sm->hashSeed, numIds, id_strings, out_ids);
}

bool sm_create(ShaderManager_t *out_sm, struct topo_instance *instance) {
    if (out_sm == nullptr || instance == nullptr) {
        return false;
    }

    auto *sm = new ShaderManager();
    sm->topo = instance;

    *out_sm = sm;
    return true;
}

bool sm_destroy(ShaderManager_t *sm) {
    if (sm == nullptr || (*sm) == nullptr)
        return false;

    auto *curModule = (*sm)->modules;
    auto *curPipeline = (*sm)->pipelines;
    auto *curPass = (*sm)->passes;
    auto device = (*sm)->topo->device.handle;

    while (curModule != nullptr) {
        vkDestroyShaderModule(device, curModule->handle, nullptr);
        curModule = curModule->next;
    }

    while (curPipeline != nullptr) {
        vkDestroyPipeline(device, curPipeline->pipeline, nullptr);
        vkDestroyPipelineLayout(device, curPipeline->pipelineLayout, nullptr);

        for (uint32_t idxLayout = 0; idxLayout < 4; idxLayout++) {
            if (curPipeline->descriptorSetLayouts[idxLayout] != VK_NULL_HANDLE) {
                vkDestroyDescriptorSetLayout(device, curPipeline->descriptorSetLayouts[idxLayout], nullptr);
            }
        }

        curPipeline = curPipeline->next;
    }

    while (curPass != nullptr) {
        vkDestroyRenderPass(device, curPass->handle, nullptr);
        curPass = curPass->next;
    }

    delete (*sm);
    *sm = nullptr;
    return true;
}

bool sm_load_shaders(
    ShaderManager_t sm,
    size_t numShaders,
    ShaderModuleNode *out_shaders,
    const char *cachePaths[],
    const char *paths[]) {
    // TODO: spir-v caching
    auto compiler = shaderc_compiler_initialize();

    for (size_t idxShader = 0; idxShader < numShaders; idxShader++) {
        auto *path = paths[idxShader];
        auto *file = fopen(path, "rb");
        if (!file) {
            fprintf(stderr, TAG "Failed to open file '%s' for reading!\n", path);
            continue;
        }

        void *source = nullptr;
        sm->arenaTemp.push(0, &source);
        printf(TAG "Reading '%s'\n", paths[idxShader]);
        size_t lenSource = 0;
        while (!feof(file)) {
            void *readBuffer;
            sm->arenaTemp.push(1024, &readBuffer);
            lenSource += fread(readBuffer, 1, 1024, file);
        }
        printf(TAG "Compiling '%s'\n", paths[idxShader]);

        auto result = shaderc_compile_into_spv(
            compiler, (char *)source, lenSource, shaderc_shader_kind::shaderc_glsl_default_vertex_shader,
            paths[idxShader], "main", nullptr);

        if (result && shaderc_result_get_compilation_status(result) == shaderc_compilation_status_success) {
            auto lenBinary = shaderc_result_get_length(result);
            auto *binary = shaderc_result_get_bytes(result);

            VkShaderModuleCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = lenBinary;
            createInfo.pCode = (uint32_t *)binary;
            if (vkCreateShaderModule(
                    sm->topo->device.handle, &createInfo, nullptr, &out_shaders[idxShader].handle)
                != VK_SUCCESS) {
                fprintf(stderr, TAG "Failed to create shader module from '%s'!\n", path);
            }
        } else {
            fprintf(stderr, TAG "Failed to compile shader '%s':\n", path);
            fprintf(stderr, "%s\n", shaderc_result_get_error_message(result));
        }

        shaderc_result_release(result);
        sm->arenaTemp.clear();
    }

    shaderc_compiler_release(compiler);
    return true;
}
template <typename T> bool sm_lookup_node(ShaderManager_t sm, const char *id, T *start, T **out_node) {
    return lookup_node(sm->hashSeed, id, start, out_node);
}

bool sm_lookup_module(ShaderManager_t sm, const char *id, ShaderModuleNode **out_node) {
    return sm_lookup_node(sm, id, sm->modules, out_node);
}

bool sm_lookup_pipeline(ShaderManager_t sm, const char *id, ShaderPipelineNode **out_node) {
    ZoneScoped;
    return sm_lookup_node(sm, id, sm->pipelines, out_node);
}

bool sm_lookup_pass(ShaderManager_t sm, const char *id, PassNode **out_node) {
    return sm_lookup_node(sm, id, sm->passes, out_node);
}
bool sm_create_pipeline(
    ShaderManager_t sm,
    const ShaderPipelineDescriptor *descriptor,
    VkPipeline *pipeline,
    VkPipelineLayout *pipelineLayout,
    VkDescriptorSetLayout *descriptorSetLayout) {
    ShaderModuleNode *moduleVert, *moduleFrag;
    PassNode *pass;
    sm_lookup_module(sm, descriptor->vertexShaderId, &moduleVert);
    sm_lookup_module(sm, descriptor->fragmentShaderId, &moduleFrag);
    sm_lookup_pass(sm, descriptor->passId, &pass);

    *pipeline = nullptr;
    *pipelineLayout = nullptr;

    VkPipelineShaderStageCreateInfo shaderStages[] = { {}, {} };
    auto &vertShaderStageInfo = shaderStages[0];
    vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = moduleVert->handle;
    vertShaderStageInfo.pName = "main";

    auto &fragShaderStageInfo = shaderStages[1];
    fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = moduleFrag->handle;
    fragShaderStageInfo.pName = "main";

    // Vertex bindings
    VkVertexInputBindingDescription *vertexInputBindingDesc = nullptr;
    VkVertexInputAttributeDescription *vertexInputAttrDesc = nullptr;
    sm->arenaTemp.pushElements(5, &vertexInputBindingDesc);
    sm->arenaTemp.pushElements(5, &vertexInputAttrDesc);

    VkPipelineInputAssemblyStateCreateInfo inputAssembly {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    VkViewport viewport {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float)sm->topo->extSwap.width;
    viewport.height = (float)sm->topo->extSwap.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor {};
    scissor.offset = { 0, 0 };
    scissor.extent = sm->topo->extSwap;

    VkPipelineViewportStateCreateInfo viewportState {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace
        = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f;
    rasterizer.depthBiasClamp = 0.0f;
    rasterizer.depthBiasSlopeFactor = 0.0f;

    VkPipelineMultisampleStateCreateInfo multisampling {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;

    std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments(pass->info.numColorAttachments);

    for (size_t idxColorBlendAttachment = 0; idxColorBlendAttachment < pass->info.numColorAttachments;
         idxColorBlendAttachment++) {
        auto &colorBlendAttachment = colorBlendAttachments[idxColorBlendAttachment];
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
            | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
        colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
    }

    VkPipelineDepthStencilStateCreateInfo depthStencil = {};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f;
    depthStencil.maxDepthBounds = 1.0f;
    depthStencil.stencilTestEnable = VK_FALSE;

    VkPipelineColorBlendStateCreateInfo colorBlending {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount = pass->info.numColorAttachments;
    colorBlending.pAttachments = colorBlendAttachments.data();
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    VkDynamicState dynamicStates[] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
    VkPipelineDynamicStateCreateInfo dynamicState {};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = 2;
    dynamicState.pDynamicStates = dynamicStates;

    memset(descriptorSetLayout, 0, 4 * sizeof(VkDescriptorSetLayout));
    for (uint32_t i = 0; i < descriptor->numDescriptorSets; i++) {
        auto &set = descriptor->arrDescriptorSets[i];

        VkDescriptorSetLayoutCreateInfo layoutInfo {};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = set.numBindings;
        layoutInfo.pBindings = set.arrBindings;

        if (vkCreateDescriptorSetLayout(
                sm->topo->device.handle, &layoutInfo, nullptr, &descriptorSetLayout[i])
            != VK_SUCCESS) {
            fprintf(stderr, TAG "Failed to create the descriptor set layout\n");
            return false;
        }
    }

    VkPipelineLayoutCreateInfo pipelineLayoutInfo {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = descriptor->numDescriptorSets;
    pipelineLayoutInfo.pSetLayouts = descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    if (vkCreatePipelineLayout(sm->topo->device.handle, &pipelineLayoutInfo, nullptr, pipelineLayout)
        != VK_SUCCESS) {
        fprintf(stderr, TAG "Failed to create the pipeline layout\n");
        return false;
    }

    VkGraphicsPipelineCreateInfo pipelineInfo {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = descriptor->vertexInputState;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = &depthStencil;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr;
    pipelineInfo.layout = *pipelineLayout;
    pipelineInfo.renderPass = pass->handle;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    if (vkCreateGraphicsPipelines(
            sm->topo->device.handle, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, pipeline)
        != VK_SUCCESS) {
        fprintf(stderr, TAG "Failed to create the pipeline\n");
        vkDestroyPipelineLayout(sm->topo->device.handle, *pipelineLayout, nullptr);
        return false;
    }

    return true;
}

bool sm_create_pipeline(ShaderManager_t sm, const char *name, const ShaderPipelineDescriptor *descriptor) {
    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
    VkDescriptorSetLayout descriptorSetLayout[4];
    if (!sm_create_pipeline(sm, descriptor, &pipeline, &pipelineLayout, descriptorSetLayout)) {
        return false;
    }

    printf(TAG "Pipeline '%s' created %p\n", name, pipeline);

    ShaderPipelineNode *node;
    sm->arenaPersistent.pushElements(1, &node);
    new (node) ShaderPipelineNode {};
    create_ids(sm, 1, &name, &node->id);
    node->pipeline = pipeline;
    node->pipelineLayout = pipelineLayout;
    node->numDescriptorSets = descriptor->numDescriptorSets;
    memcpy(node->descriptorSetLayouts, descriptorSetLayout, 4 * sizeof(VkDescriptorSetLayout));
    node->next = sm->pipelines;
    sm->arenaPersistent.pushElements(1, &node->descriptor);
    memcpy(node->descriptor, descriptor, sizeof(*node->descriptor));
    sm->arenaPersistent.pushElements(
        descriptor->numDescriptorSets, (DescriptorSet **)&node->descriptor->arrDescriptorSets);
    memcpy(
        (DescriptorSet *)node->descriptor->arrDescriptorSets, descriptor->arrDescriptorSets,
        descriptor->numDescriptorSets * sizeof(DescriptorSet));
    sm->pipelines = node;

    auto sizPassId = strlen(descriptor->passId) + 1;
    auto sizVertexShaderId = strlen(descriptor->vertexShaderId) + 1;
    auto sizFragmentShaderId = strlen(descriptor->fragmentShaderId) + 1;
    sm->arenaPersistent.pushElements(sizPassId, &node->descriptor->passId);
    sm->arenaPersistent.pushElements(sizVertexShaderId, &node->descriptor->vertexShaderId);
    sm->arenaPersistent.pushElements(sizFragmentShaderId, &node->descriptor->fragmentShaderId);
    memcpy((char *)node->descriptor->passId, descriptor->passId, sizPassId);
    memcpy((char *)node->descriptor->vertexShaderId, descriptor->vertexShaderId, sizVertexShaderId);
    memcpy((char *)node->descriptor->fragmentShaderId, descriptor->fragmentShaderId, sizFragmentShaderId);

    for (uint32_t i = 0; i < NUM_MAX_FRAMES_IN_FLIGHT; i++) {
        node->arenaDrawUniformBuffers[i] = arena_t(arena_t::kilobytes(64));
    }

    node->numUniformBuffers = NUM_MAX_FRAMES_IN_FLIGHT;
    sm->arenaPersistent.pushElements(node->numUniformBuffers, &node->uniformBuffers);
    for (uint32_t i = 0; i < node->numUniformBuffers; i++) {
        node->uniformBuffers[i] = uint32_t(-1);
    }

    return true;
}

bool sm_load_shaders(
    ShaderManager_t sm,
    size_t numShaders,
    const char *names[],
    const char *cachePaths[],
    const char *paths[]) {
    // TODO: spir-v caching
    auto compiler = shaderc_compiler_initialize();

    ShaderModuleNode *modules = nullptr;
    Id *ids = nullptr;
    sm->arenaPersistent.pushElements(numShaders, &modules);
    sm->arenaTemp.pushElements(numShaders, &ids);

    create_ids(sm, numShaders, names, ids);

    for (size_t idxShader = 0; idxShader < numShaders; idxShader++) {
        auto *path = paths[idxShader];
        auto *file = fopen(path, "rb");
        if (!file) {
            fprintf(stderr, TAG "Failed to open file '%s' for reading!\n", path);
            continue;
        }

        char *source = nullptr;
        printf(TAG "Reading '%s'\n", paths[idxShader]);
        fseek(file, 0, SEEK_END);
        auto lenSource = ftell(file);
        sm->arenaTemp.pushElements(lenSource + 1, &source);
        fseek(file, 0, SEEK_SET);
        fread(source, 1, lenSource, file);
        source[lenSource] = '\0';
        printf(TAG "Compiling '%s'\n", paths[idxShader]);

        auto result = shaderc_compile_into_spv(
            compiler, (char *)source, lenSource, shaderc_glsl_infer_from_source,
            paths[idxShader], "main", nullptr);

        if (result && shaderc_result_get_compilation_status(result) == shaderc_compilation_status_success) {
            auto lenBinary = shaderc_result_get_length(result);
            auto *binary = shaderc_result_get_bytes(result);

            VkShaderModuleCreateInfo createInfo {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = lenBinary;
            createInfo.pCode = (uint32_t *)binary;

            if (vkCreateShaderModule(
                    sm->topo->device.handle, &createInfo, nullptr, &modules[idxShader].handle)
                != VK_SUCCESS) {
                fprintf(stderr, TAG "Failed to create shader module from '%s'!\n", path);
            }
        } else {
            fprintf(stderr, TAG "Failed to compile shader '%s':\n", path);
            fprintf(stderr, "%s\n", shaderc_result_get_error_message(result));
        }

        shaderc_result_release(result);
    }

    for (size_t idxShader = 0; idxShader < numShaders; idxShader++) {
        modules[idxShader].id = ids[idxShader];
        modules[idxShader].next = &modules[idxShader + 1];
    }
    modules[numShaders - 1].next = sm->modules;
    sm->modules = &modules[0];

    sm->arenaTemp.clear();

    shaderc_compiler_release(compiler);
    return true;
}

bool sm_get_pipeline(ShaderManager_t sm, ShaderPipeline *out_pipeline, const char *id) {
    ZoneScoped;
    ShaderPipelineNode *node;
    if (!sm_lookup_pipeline(sm, id, &node)) {
        return false;
    }

    out_pipeline->pipeline = node->pipeline;
    out_pipeline->layout = node->pipelineLayout;
    out_pipeline->numDescriptorSets = node->numDescriptorSets;
    memcpy(out_pipeline->descriptorSetLayout, node->descriptorSetLayouts, 4 * sizeof(VkDescriptorSetLayout));

    return true;
}

bool sm_get_pass_uniform_buffer(
    ShaderManager_t sm,
    uint32_t idxFrame,
    const char *id,
    size_t size,
    BmUniformBuffer *out_handle) {
    ZoneScoped;
    ShaderPipelineNode *node;
    if (!sm_lookup_pipeline(sm, id, &node)) {
        fmt::print(TAG "sm_get_pass_uniform_buffer: Couldn\'t find pipeline '{}'!\n", id);
        return false;
    }

    if (idxFrame >= node->numUniformBuffers) {
        fmt::print(
            TAG "sm_get_pass_uniform_buffer: idxFrame is {} but numUniformBuffers is {}\n", idxFrame,
            node->numUniformBuffers);
        return false;
    }

    if (node->uniformBuffers[idxFrame] == uint32_t(-1)) {
        uint32_t stride;
        if (!bm_make_uniform_buffer(
                sm->topo->device.bufferManager, size, 1, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                &node->uniformBuffers[idxFrame], &stride)) {
            fmt::print(TAG "sm_get_pass_uniform_buffer: bm_make_uniform_buffer has failed\n");
            return false;
        }
    }

    *out_handle = node->uniformBuffers[idxFrame];
    return true;
}

bool sm_get_draw_uniform_buffer(
    ShaderManager_t sm,
    uint32_t idxFrame,
    const char *id,
    size_t sizElement,
    size_t numElements,
    BmUniformBuffer *out_handle,
    uint32_t *out_stride,
    VkBufferUsageFlags usage) {
    ZoneScoped;
    ShaderPipelineNode *node;
    if (!sm_lookup_pipeline(sm, id, &node)) {
        return false;
    }

    BmUniformBuffer *pHandle;
    node->arenaDrawUniformBuffers[idxFrame].pushElements(1, &pHandle);
    node->numDrawUniformBuffers[idxFrame]++;

    if (!bm_make_uniform_buffer(sm->topo->device.bufferManager, sizElement, numElements, usage, pHandle, out_stride)) {
        // NOTE(danielm): the buffer handle is temporarily leaked here
        assert(0);
        return false;
    }

    *out_handle = *pHandle;
    return true;
}

bool sm_release_uniform_buffers(ShaderManager_t sm, uint32_t idxFrame, const char *id) {
    ShaderPipelineNode *node;
    if (!sm_lookup_pipeline(sm, id, &node)) {
        return false;
    }

    if (node->uniformBuffers[idxFrame] == uint32_t(-1)) {
        return true;
    }

    bm_free_uniform_buffer(sm->topo->device.bufferManager, node->uniformBuffers[idxFrame]);
    node->uniformBuffers[idxFrame] = uint32_t(-1);

    // Free the per-draw uniform buffers
    for (uint32_t i = 0; i < node->numDrawUniformBuffers[idxFrame]; i++) {
        auto handle = ((BmUniformBuffer *)node->arenaDrawUniformBuffers[idxFrame].buffer)[i];
        bm_free_uniform_buffer(sm->topo->device.bufferManager, handle);
    }

    node->arenaDrawUniformBuffers[idxFrame].clear();

    return true;
}

bool sm_release_uniform_buffers(ShaderManager_t sm, uint32_t idxFrame) {
    auto *bm = sm->topo->device.bufferManager;
    auto *cur = sm->pipelines;

    while (cur != nullptr) {
        // Free the per-pass uniform buffers
        if (cur->uniformBuffers[idxFrame] != uint32_t(-1)) {
            bm_free_uniform_buffer(bm, cur->uniformBuffers[idxFrame]);
            cur->uniformBuffers[idxFrame] = uint32_t(-1);
        }

		// Free the per-draw uniform buffers
		for (uint32_t i = 0; i < cur->numDrawUniformBuffers[idxFrame]; i++) {
			auto handle = ((BmUniformBuffer *)cur->arenaDrawUniformBuffers[idxFrame].buffer)[i];
			bm_free_uniform_buffer(bm, handle);
		}

		cur->numDrawUniformBuffers[idxFrame] = 0;
		cur->arenaDrawUniformBuffers[idxFrame].clear();

        cur = cur->next;
    }

    return true;
}

static void init_color_attachment(VkAttachmentDescription &colorAttachment) {
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
}

static void init_color_attachment_ref(VkAttachmentReference &colorAttachmentRef, size_t idxAttachment) {
    colorAttachmentRef.attachment = idxAttachment;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
}

bool sm_create_pass(ShaderManager_t sm, const PassDescriptor *descriptor, PassInfo &info, VkRenderPass *handle) {
    auto *instance = sm->topo;
    *handle = nullptr;
    std::vector<VkAttachmentDescription> attachments(2);
    std::vector<VkAttachmentReference> attachmentRefs(2);
    size_t numColorAttachments = 1;
    size_t numDepthAttachments = 1;
    auto &colorAttachment = attachments[0];
    auto &colorAttachmentRef = attachmentRefs[0];

    switch (descriptor->kind) {
    case PassKind::World: {
        numColorAttachments = 4;
        attachments.resize(numColorAttachments + numDepthAttachments);
        attachmentRefs.resize(numColorAttachments + numDepthAttachments);
        for (size_t idxAttachment = 0; idxAttachment < numColorAttachments; idxAttachment++) {
            init_color_attachment(attachments[idxAttachment]);
            init_color_attachment_ref(attachmentRefs[idxAttachment], idxAttachment);
            attachments[idxAttachment].format = descriptor->world.outputFormats[idxAttachment];
            attachments[idxAttachment].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        }
        break;
    }
    case PassKind::Blit: {
        numColorAttachments = 1;
        init_color_attachment(colorAttachment);
        init_color_attachment_ref(colorAttachmentRef, 0);
        colorAttachment.format = descriptor->blit.outputFormat;
        if (descriptor->blit.isTargetSwapchain) {
            colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        } else {
            colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        }
        break;
    }
    }

    info.numColorAttachments = numColorAttachments;

    auto &depthAttachment = attachments[numColorAttachments];
    depthAttachment.format = VK_FORMAT_D32_SFLOAT;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    assert(numDepthAttachments == 1);
    auto &depthAttachmentRef = attachmentRefs[numColorAttachments];
    depthAttachmentRef.attachment = numColorAttachments;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    // Here we attach the framebuffers
    subpass.colorAttachmentCount = numColorAttachments;
    subpass.pColorAttachments = attachmentRefs.data();
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    VkRenderPassCreateInfo renderPassInfo {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = attachments.size();
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;

    VkSubpassDependency dependency {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    // Wait on the color/depth attachment output stage, i.e. when the swapchain
    // finishes reading from it
    dependency.srcStageMask
        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dependency.srcAccessMask = 0;
    // Operations that wait on this are the writes to the color/depth attachment
    dependency.dstStageMask
        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dependency.dstAccessMask
        = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vkCreateRenderPass(instance->device.handle, &renderPassInfo, nullptr, handle) != VK_SUCCESS) {
        return false;
    }

    return true;
}
bool sm_create_pass(ShaderManager_t sm, const char *id, const PassDescriptor *descriptor) {
    VkRenderPass handle;
    PassInfo passInfo;

    if (!sm_create_pass(sm, descriptor, passInfo, &handle)) {
        fprintf(stderr, TAG "Failed to create render pass '%s'\n", id);
        return false;
    }

    printf(TAG "Created pass '%s' handle %p\n", id, handle);

    PassNode *node;
    if (!sm->arenaPersistent.pushElements(1, &node)) {
        return false;
    }

    create_ids(sm, 1, &id, &node->id);
    node->handle = handle;
    node->descriptor = *descriptor;
    node->next = sm->passes;
    node->info = passInfo;
    sm->passes = node;

    return true;
}

bool sm_get_pass(ShaderManager_t sm, const char *id, Pass *pass) {
    PassNode *node;
    if (!sm_lookup_pass(sm, id, &node)) {
        return false;
    }

    pass->handle = node->handle;
    return true;
}

bool sm_swapchain_resized(ShaderManager_t sm) {
    printf(TAG "Swapchain resized; recreating passes and pipelines\n");
    auto device = sm->topo->device.handle;
    auto *curPass = sm->passes;
    while (curPass != nullptr) {
        printf(TAG "Destroying pass handle %p", curPass->handle);
        vkDestroyRenderPass(device, curPass->handle, nullptr);

        curPass->handle = nullptr;
        PassInfo passInfo;
        if (!sm_create_pass(sm, &curPass->descriptor, passInfo, &curPass->handle)) {
            abort();
        }

        printf(TAG "Recreated pass handle '%p'\n", curPass->handle);

        curPass = curPass->next;
    }

    auto *curPipeline = sm->pipelines;
    while (curPipeline != nullptr) {
        printf(TAG "Destroying pipeline handle %p", curPipeline->pipeline);
        vkDestroyPipeline(device, curPipeline->pipeline, nullptr);

        curPipeline->pipeline = nullptr;
        curPipeline->pipelineLayout = nullptr;
        if (!sm_create_pipeline(
                sm, curPipeline->descriptor, &curPipeline->pipeline, &curPipeline->pipelineLayout,
                curPipeline->descriptorSetLayouts)) {
            abort();
        }

        printf(TAG "Recreated pipeline handle '%p'\n", curPipeline->pipeline);

        curPipeline = curPipeline->next;
    }

    return true;
}