#include "vk_draw.h"
#include "vk_instance.h"
#include "vk_textures.h"

struct DrawUniforms {
    glm::mat4 matModel;
};

struct PassUniforms {
    glm::mat4 matViewProj;
};

static bool GetDrawUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(DrawUniforms);
    return true;
}

static bool GetPassUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(PassUniforms);
    return true;
}

static void
convert(void *dst, uint32_t offset, uint32_t stride, const Transform *transforms, uint32_t numTransforms) {
    auto *cursor = ((uint8_t *)dst) + offset;

    for (uint32_t idxTransform = 0; idxTransform < numTransforms; idxTransform++) {
        auto &in = transforms[idxTransform];
        auto matScale = glm::scale(glm::mat4(1.0f), in.scale);
        auto matRotate = glm::mat4_cast(in.rotation);
        auto matTranslate = glm::translate(glm::mat4(1.0f), in.position);
        *((glm::mat4 *)cursor) = matTranslate * matRotate * matScale;

        cursor += stride;
    }
}

static void convert(glm::mat4 &out, const topo_camera &in) {
    auto t = in.transform;
    Transform t2;
    t2.position = glm::vec3(t.position[0], t.position[1], t.position[2]);
    t2.rotation = glm::quat(t.rotation[0], t.rotation[1], t.rotation[2], t.rotation[3]);
    t2.scale = glm::vec3(t.scale[0], t.scale[1], t.scale[2]);
    convert(&out, 0, 0, &t2, 1);
}

static bool
ComputePerDrawUniformData(void *user, const DrawPass *pass, DrawBatch *cur, void *pUniformBuffer) {
    ZoneScoped;
    assert(cur != nullptr && pUniformBuffer != nullptr);

    convert(
        pUniformBuffer, offsetof(DrawUniforms, matModel), cur->perDrawUniformsStride, cur->transforms,
        cur->numDraws);

    return true;
}

static bool
ComputePerPassUniformData(void *user, const DrawPass *pass, DrawBatch *batch, void *pUniformBuffer) {
    auto *instance = pass->instance;

    glm::mat4 matView, matProj;

    convert(matView, pass->camera);
    matProj = glm::perspective(
        pass->camera.fieldOfView, instance->extSwap.width / (float)instance->extSwap.height,
        pass->camera.nearClip, pass->camera.farClip);

    ((PassUniforms *)pUniformBuffer)->matViewProj = matProj * matView;

    return true;
}

static bool
UpdatePerMaterialUniforms(void *user, const DrawPass *pass, const ClientMaterial &material, VkDescriptorSet set) {
    auto *instance = pass->instance;
    auto &dev = instance->device;
    auto sm = dev.shaderManager;
    auto tm = dev.textureManager;

    auto &texBaseColor = dev.images.entries[uint64_t(material.unlitTextured.imageBaseColor)];

    // Write the descriptor sets
    VkDescriptorImageInfo imageInfo {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkWriteDescriptorSet writes[1] = { {} };

    // Write textures
    tm_get_sampler_handle(tm, texBaseColor.tmHandle, &imageInfo.imageView, &imageInfo.sampler);
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = set;
    writes[0].dstBinding = 0;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[0].pImageInfo = &imageInfo;
    vkUpdateDescriptorSets(dev.handle, 1, &writes[0], 0, nullptr);
    return true;
}

static bool InitPipelineDescriptor(
    void *user,
    arena_t &arena,
    const ClientMaterial &material,
    ShaderPipelineDescriptor &descriptor) {
    descriptor.passId = "pass_world";
    descriptor.vertexShaderId = "textured/vert";
    descriptor.fragmentShaderId = "textured/frag";
    descriptor.drawsToSwapchain = false;
    DescriptorSet *sets;
    arena.pushElements(3, &sets);
    descriptor.numDescriptorSets = 3;
    descriptor.arrDescriptorSets = sets;

    VkDescriptorSetLayoutBinding *uboLayoutBindings;
    arena.pushElements(3, &uboLayoutBindings);

    uboLayoutBindings[0].binding = 0;
    uboLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBindings[0].descriptorCount = 1;
    uboLayoutBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    uboLayoutBindings[1].binding = 0;
    uboLayoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    uboLayoutBindings[1].descriptorCount = 1;
    uboLayoutBindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    uboLayoutBindings[2].binding = 0;
    uboLayoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    uboLayoutBindings[2].descriptorCount = 1;
    uboLayoutBindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    sets[0].numBindings = 1;
    sets[0].arrBindings = &uboLayoutBindings[0];

    sets[1].numBindings = 1;
    sets[1].arrBindings = &uboLayoutBindings[1];

    sets[2].numBindings = 1;
    sets[2].arrBindings = &uboLayoutBindings[2];
    return true;
}

static MaterialDrawHandlerOps drawHandler = {
    GetDrawUniformSize, ComputePerDrawUniformData, ComputePerPassUniformData,
    GetPassUniformSize, UpdatePerMaterialUniforms, InitPipelineDescriptor,
};

REGISTER_MATERIAL_DRAW_HANDLER(kTopoMaterialKind_TextureColor, &drawHandler, nullptr);
