#pragma once

#include <atomic>
#include <cstdint>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>

#include <vulkan/vulkan.h>

#include <topo/topo.h>

#include "vk_descriptor_pool_list.h"
#include "vk_forward.h"

#include "list.h"

struct PhysicalDevice {
    VkPhysicalDevice handle;
    bool isGpu;
    bool isDiscrete;
    bool supportsGraphics;
    bool supportsTransfer;
    bool supportsCompute;
    bool supportsPresent;
    uint32_t idxQueueGraphics;
    uint32_t idxQueueTransfer;
    uint32_t idxQueueCompute;
    uint32_t idxQueuePresent;
    bool supportsAllRequiredExtensions;
};

struct SwapchainSupportDetails {
    VkSurfaceCapabilitiesKHR caps;

    uint32_t numFormats;
    VkSurfaceFormatKHR *formats;

    uint32_t numPresentModes;
    VkPresentModeKHR *presentModes;
};

struct Swapchain {
    VkSwapchainKHR handle;
    arena_t arena { arena_t::megabytes(8) };

    uint32_t idxPrevFrame = 0;
    uint32_t idxCurrentFrame = 0;
    uint32_t numImages;
    VkImage *images;
    VkImageView *imageViews;
};

struct PresentSync {
    arena_t arena { arena_t::kilobytes(4) };
    // Signaled when an image has been acquired from the swapchain and is ready
    // for use as a render target
    VkSemaphore *imageAcquired;
    // Signaled when the rendering into the image has finished and it's ready
    // for presentation
    VkSemaphore *imageReady;
    // Blocks the host until there is an in-flight frame
    VkFence *frameInFlight;
};

struct ClientImage {
    bool used;

    volatile bool pending;
    Texture tmHandle;
};

struct Images {
    uint32_t idxNextUnused = 0;
    ClientImage entries[NUM_MAX_IMAGES];
};

typedef enum {
    Material_UnlitColor,
    Material_UnlitTextured,
    Material_RenderTargetReceiver,
    Material_NormalMapped,
} MaterialKind;

struct ClientMaterial {
    bool used;

    Id id;

    MaterialKind kind;

    union {
        struct {
            float colorR, colorG, colorB;
        } unlitColor;
        struct {
            topo_image_t imageBaseColor;
        } unlitTextured;
        struct {
            RenderTarget_t renderTarget;
        } renderTargetReceiver;
        struct {
            topo_image_t imageBaseColor;
            topo_image_t imageNormalMap;
            topo_image_t imageMetallicRoughness;
        } normalMapped;
    };
};

struct Materials {
    uint32_t idxNextUnused = 0;
    ClientMaterial entries[NUM_MAX_MATERIALS];
};

struct ClientMesh {
    bool used;
    volatile bool pending;

    topo_mesh_handle handle;
};

struct Meshes {
    uint32_t idxNextUnused = 0;
    ClientMesh entries[NUM_MAX_MESHES];
};

struct FullscreenQuad {
    topo_mesh_handle hMesh;
    topo_material_t hMaterial;
};

struct LogicalDevice {
    VkDevice handle;
    arena_t arenaPersistent { arena_t::megabytes(4) };
    arena_t arenaSwapchain { arena_t::megabytes(4) };

    VkQueue queueGraphics;
    VkQueue queuePresent;

    SwapchainSupportDetails swapchainSupportDetails;
    Swapchain currentSwapchain;

    VkCommandPool cmdPool;
    VkCommandBuffer *cmdBuffers;

    DescriptorPoolList descriptorPoolLists[NUM_MAX_FRAMES_IN_FLIGHT];

    PresentSync presentSync;

    BufferManager_t bufferManager;
    TextureManager_t textureManager;
    ShaderManager_t shaderManager;
    MeshManager_t meshManager;

    Images images;
    Materials materials;
    Meshes meshes;

    VkFramebuffer *framebuffers;
    Texture *depthBuffers;

    FullscreenQuad fullscreenQuad;
};

struct topo_instance {
    std::thread rendererThread;
    bool shutdown = false;
    bool windowExiting = false;
    arena_t arenaSystem { arena_t::kilobytes(256) };
    arena_t arenaFrame { arena_t::megabytes(16) };
    arena_t arenaScene { arena_t::megabytes(8) };

    SDL_Window *pWindow;
    VkInstance pVkInst;
    VkSurfaceKHR surface;
    PhysicalDevice physicalDevice;
    LogicalDevice device;

    uint32_t idxSurfaceFormat;
    uint32_t idxPresentMode;
    VkExtent2D extSwap;

    std::mutex queuedTasksLock;
    std::queue<std::function<void()>> queuedTasks;

    std::mutex sceneCvLock;
    std::condition_variable sceneCv;

    std::atomic<const topo_scene *> sceneCurrent = nullptr;
    std::atomic<const topo_scene *> sceneIncoming = nullptr;
    std::atomic<const topo_scene *> sceneOutgoing = nullptr;
};
