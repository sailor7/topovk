#pragma once

#include <cstdint>

constexpr uint32_t NUM_MAX_FRAMES_IN_FLIGHT = 2;
constexpr uint32_t NUM_MAX_IMAGES = 2048;
constexpr uint32_t NUM_MAX_MATERIALS = 8192;
constexpr uint32_t NUM_MAX_MESHES = 8192;

typedef struct BufferManager *BufferManager_t;
typedef uint32_t BmVertexBuffer;
typedef uint32_t BmIndexBuffer;
typedef uint32_t BmUniformBuffer;
typedef uint32_t BmImage;

typedef struct TextureManager *TextureManager_t;
typedef uint32_t Texture;

typedef struct MeshManager *MeshManager_t;
typedef uint32_t MmMesh;

typedef struct topo_mesh *topo_mesh_handle;

typedef struct ShaderManager *ShaderManager_t;

typedef struct RenderTarget *RenderTarget_t;

struct SDL_Window;
typedef struct topo_instance *topo_instance_t;

struct arena_t;
