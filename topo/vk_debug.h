#pragma once

#include <vulkan/vulkan_core.h>

#ifdef __cplusplus
extern "C" {
#endif

VKAPI_ATTR VkResult VKAPI_CALL
vkTryInitDebugUtils(VkInstance instance);

VKAPI_ATTR VkResult VKAPI_CALL
vkTrySetDebugUtilsObjectNameTOPO(VkDevice device, const VkDebugUtilsObjectNameInfoEXT *pNameInfo);

VKAPI_ATTR VkResult VKAPI_CALL
vkTrySetDebugUtilsObjectTagTOPO(VkDevice device, const VkDebugUtilsObjectTagInfoEXT *pTagInfo);

VKAPI_ATTR VkResult VKAPI_CALL
vkTryBeginDebugUtilsLabelTOPO(VkCommandBuffer commandBuffer, const char *label);

VKAPI_ATTR VkResult VKAPI_CALL
vkTryEndDebugUtilsLabelTOPO(VkCommandBuffer commandBuffer);

#ifdef __cplusplus
}
#endif

