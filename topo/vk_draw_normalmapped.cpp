#include "vk_draw.h"
#include "vk_instance.h"
#include "vk_textures.h"

struct DrawUniforms {
    glm::mat4 matModel;
};

struct PassUniforms {
    glm::mat4 matViewProj;
};

static bool GetDrawUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(DrawUniforms);
    return true;
}

static bool GetPassUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(PassUniforms);
    return true;
}

static void
convert(void *dst, uint32_t offset, uint32_t stride, const Transform *transforms, uint32_t numTransforms) {
    auto *cursor = ((uint8_t *)dst) + offset;

    for (uint32_t idxTransform = 0; idxTransform < numTransforms; idxTransform++) {
        auto &in = transforms[idxTransform];
        auto matScale = glm::scale(glm::mat4(1.0f), in.scale);
        auto matRotate = glm::mat4_cast(in.rotation);
        auto matTranslate = glm::translate(glm::mat4(1.0f), in.position);
        *((glm::mat4 *)cursor) = matTranslate * matRotate * matScale;

        cursor += stride;
    }
}

static void convert(glm::mat4 &out, const topo_camera &in) {
    auto t = in.transform;
    Transform t2;
    t2.position = glm::vec3(t.position[0], t.position[1], t.position[2]);
    t2.rotation = glm::quat(t.rotation[0], t.rotation[1], t.rotation[2], t.rotation[3]);
    t2.scale = glm::vec3(t.scale[0], t.scale[1], t.scale[2]);
    convert(&out, 0, 0, &t2, 1);
}

static bool
ComputePerDrawUniformData(void *user, const DrawPass *pass, DrawBatch *cur, void *pUniformBuffer) {
    assert(cur != nullptr && pUniformBuffer != nullptr);

    convert(
        pUniformBuffer, offsetof(DrawUniforms, matModel), cur->perDrawUniformsStride, cur->transforms,
        cur->numDraws);

    return true;
}

static bool
ComputePerPassUniformData(void *user, const DrawPass *pass, DrawBatch *batch, void *pUniformBuffer) {
    auto *instance = pass->instance;

    glm::mat4 matView, matProj;

    convert(matView, pass->camera);
    matProj = glm::perspective(
        pass->camera.fieldOfView, instance->extSwap.width / (float)instance->extSwap.height,
        pass->camera.nearClip, pass->camera.farClip);

    ((PassUniforms *)pUniformBuffer)->matViewProj = matProj * matView;

    return true;
}

static bool UpdatePerMaterialUniforms(
    void *user,
    const DrawPass *pass,
    const ClientMaterial &material,
    VkDescriptorSet set) {
    auto *instance = pass->instance;
    auto &dev = instance->device;
    auto sm = dev.shaderManager;
    auto tm = dev.textureManager;

    auto &texBaseColor = dev.images.entries[uint64_t(material.normalMapped.imageBaseColor)];
    auto &texNormalMap = dev.images.entries[uint64_t(material.normalMapped.imageNormalMap)];
    auto &texMetallicRoughness = dev.images.entries[uint64_t(material.normalMapped.imageMetallicRoughness)];

    // Write the descriptor sets
    VkDescriptorImageInfo imageInfoColor {};
    VkDescriptorImageInfo imageInfoNormal {};
    VkDescriptorImageInfo imageInfoMetalRough {};
    imageInfoColor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfoNormal.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfoMetalRough.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    Texture textures[3] = { texBaseColor.tmHandle, texNormalMap.tmHandle, texMetallicRoughness.tmHandle };
    VkImageView views[3];
    VkSampler samplers[3];

    tm_get_texture_handles(tm, 3, textures, views, samplers);
    imageInfoColor.imageView = views[0];
    imageInfoColor.sampler = samplers[0];
    imageInfoNormal.imageView = views[1];
    imageInfoNormal.sampler = samplers[1];
    imageInfoMetalRough.imageView = views[2];
    imageInfoMetalRough.sampler = samplers[2];

    VkWriteDescriptorSet writes[3] = { {}, {} };

    // Write textures
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = set;
    writes[0].dstBinding = 0;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[0].pImageInfo = &imageInfoColor;

    writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[1].dstSet = set;
    writes[1].dstBinding = 1;
    writes[1].descriptorCount = 1;
    writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[1].pImageInfo = &imageInfoNormal;

    writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[2].dstSet = set;
    writes[2].dstBinding = 2;
    writes[2].descriptorCount = 1;
    writes[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[2].pImageInfo = &imageInfoMetalRough;

    vkUpdateDescriptorSets(dev.handle, 3, &writes[0], 0, nullptr);

    return true;
}

static bool InitPipelineDescriptor(
    void *user,
    arena_t &arena,
    const ClientMaterial &material,
    ShaderPipelineDescriptor &descriptor) {
    descriptor.passId = "pass_world";
    descriptor.vertexShaderId = "normalmapped/vert";
    descriptor.fragmentShaderId = "normalmapped/frag";
    descriptor.drawsToSwapchain = false;
    DescriptorSet *sets;
    arena.pushElements(3, &sets);
    descriptor.numDescriptorSets = 3;
    descriptor.arrDescriptorSets = sets;

    VkDescriptorSetLayoutBinding *uboLayoutBindings;
    arena.pushElements(5, &uboLayoutBindings);

    uboLayoutBindings[0].binding = 0;
    uboLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBindings[0].descriptorCount = 1;
    uboLayoutBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    uboLayoutBindings[1].binding = 0;
    uboLayoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    uboLayoutBindings[1].descriptorCount = 1;
    uboLayoutBindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    uboLayoutBindings[2].binding = 0;
    uboLayoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    uboLayoutBindings[2].descriptorCount = 1;
    uboLayoutBindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    uboLayoutBindings[3].binding = 1;
    uboLayoutBindings[3].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    uboLayoutBindings[3].descriptorCount = 1;
    uboLayoutBindings[3].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    uboLayoutBindings[4].binding = 2;
    uboLayoutBindings[4].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    uboLayoutBindings[4].descriptorCount = 1;
    uboLayoutBindings[4].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    sets[0].numBindings = 1;
    sets[0].arrBindings = &uboLayoutBindings[0];

    sets[1].numBindings = 1;
    sets[1].arrBindings = &uboLayoutBindings[1];

    sets[2].numBindings = 3;
    sets[2].arrBindings = &uboLayoutBindings[2];
    return true;
}

static MaterialDrawHandlerOps drawHandler = {
    GetDrawUniformSize,
    ComputePerDrawUniformData,
    ComputePerPassUniformData,
    GetPassUniformSize,
    UpdatePerMaterialUniforms,
    InitPipelineDescriptor,
};

REGISTER_MATERIAL_DRAW_HANDLER(kTopoMaterialKind_NormalMapped, &drawHandler, nullptr);
