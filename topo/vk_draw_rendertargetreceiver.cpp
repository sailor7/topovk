#include "vk_buffers.h"
#include "vk_draw.h"
#include "vk_instance.h"
#include "vk_rendertarget.h"
#include "vk_textures.h"

struct DrawUniforms {
    glm::mat4 matModel;
};

struct PassUniforms {
    glm::mat4 matViewProj;
};

struct LightInfo {
    glm::vec4 viewPosition;

    glm::vec4 ambientColor;
    unsigned hasAmbient;
    float ambientPower;

    unsigned numPointLights;
    unsigned numAreaLights;
};

struct PointLights {
    glm::vec4 origin;
    glm::vec4 color;
    // Intensity in lm/sr
    float intensity;
    float pad0[3];
};

static bool GetDrawUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(DrawUniforms);
    return true;
}

static bool GetPassUniformSize(void *user, uint32_t *out_size) {
    *out_size = sizeof(PassUniforms);
    return true;
}

static void
convert(void *dst, uint32_t offset, uint32_t stride, const Transform *transforms, uint32_t numTransforms) {
    auto *cursor = ((uint8_t *)dst) + offset;

    for (uint32_t idxTransform = 0; idxTransform < numTransforms; idxTransform++) {
        auto &in = transforms[idxTransform];
        auto matScale = glm::scale(glm::mat4(1.0f), in.scale);
        auto matRotate = glm::mat4_cast(in.rotation);
        auto matTranslate = glm::translate(glm::mat4(1.0f), in.position);
        *((glm::mat4 *)cursor) = matTranslate * matRotate * matScale;

        cursor += stride;
    }
}

static void convert(glm::mat4 &out, const topo_camera &in) {
    auto t = in.transform;
    Transform t2;
    t2.position = glm::vec3(t.position[0], t.position[1], t.position[2]);
    t2.rotation = glm::quat(t.rotation[0], t.rotation[1], t.rotation[2], t.rotation[3]);
    t2.scale = glm::vec3(t.scale[0], t.scale[1], t.scale[2]);
    convert(&out, 0, 0, &t2, 1);
}

static bool
ComputePerDrawUniformData(void *user, const DrawPass *pass, DrawBatch *cur, void *pUniformBuffer) {
    assert(cur != nullptr && pUniformBuffer != nullptr);

    auto *cursor = (uint8_t *)pUniformBuffer;
    for (uint32_t idxDraw = 0; idxDraw < cur->numDraws; idxDraw++) {
        auto *du = ((DrawUniforms *)cursor);
        du->matModel = glm::mat4(1.0f);
        cursor += cur->perDrawUniformsStride;
    }

    return true;
}

static bool
ComputePerPassUniformData(void *user, const DrawPass *pass, DrawBatch *batch, void *pUniformBuffer) {
    auto *instance = pass->instance;

    ((PassUniforms *)pUniformBuffer)->matViewProj = glm::mat4(1.0f);

    return true;
}

static bool UpdatePerMaterialUniforms(
    void *user,
    const DrawPass *pass,
    const ClientMaterial &material,
    VkDescriptorSet set) {
    auto *instance = pass->instance;
    auto &dev = instance->device;
    auto sm = dev.shaderManager;
    auto tm = dev.textureManager;
    auto rt = material.renderTargetReceiver.renderTarget;
    arena_t temp { arena_t::kilobytes(4) };
    auto *bm = dev.bufferManager;
    auto idxCurrentFrame = dev.currentSwapchain.idxCurrentFrame;
    uint32_t stride;
    BmUniformBuffer uboLightInfo, uboPointLights;

    // KLUDGE(danielm): 
    auto hMesh = pass->instance->device.fullscreenQuad.hMesh;
    auto *pMesh = (Mesh *)hMesh;
    auto *pipelineId = pMesh->subMeshes[0].namePipeline;

    // Count the number of light sources
    LightInfo lightInfo;

    lightInfo.viewPosition[0] = pass->camera.transform.position[0];
    lightInfo.viewPosition[1] = pass->camera.transform.position[1];
    lightInfo.viewPosition[2] = pass->camera.transform.position[2];
    lightInfo.viewPosition[3] = 0;

	lightInfo.numPointLights = 0;
	lightInfo.numAreaLights = 0;
	lightInfo.hasAmbient = 0;

	auto *scene = pass->instance->sceneCurrent.load();
	if (scene != nullptr) {
		for (size_t idxLight = 0; idxLight < scene->numLights; idxLight++) {
			switch (scene->lights[idxLight].kind) {
				case kTopoLight_Point: {
				lightInfo.numPointLights += 1;
				break;
			}
			case kTopoLight_Area: {
				// lightInfo.numAreaLights += 1;
				break;
			}
			}
		}
	}

    // Upload the light source list

    size_t idxAreaLight = 0;
    size_t idxPointLight = 0;

    size_t sizPointLights = lightInfo.numPointLights * sizeof(PointLights);

    sm_get_draw_uniform_buffer(
        sm, idxCurrentFrame, pipelineId, sizPointLights, 1, &uboPointLights, &stride,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
    
    if (scene != nullptr) {
		void *pPointLights;
		size_t discard;
		bm_map_uniform_buffer(bm, uboPointLights, &pPointLights, &discard);

        auto *pointLights = (PointLights *)pPointLights;

        if (scene->pNext != nullptr) {
            auto *scene2 = scene->pNext;
            for (size_t idxGroup = 0; idxGroup < scene2->numObjectGroups; idxGroup++) {
                auto *lights = scene2->pObjectGroups[idxGroup].pLights;
                auto numLights = scene2->pObjectGroups[idxGroup].numLights;
                for (size_t idxLight = 0; idxLight < numLights; idxLight++) {
                    auto &Lsrc = lights[idxLight];
                    switch (lights[idxLight].kind) {
                    case kTopoLight_Ambient: {
                        lightInfo.hasAmbient = 1;
                        lightInfo.ambientColor.r = Lsrc.color[0];
                        lightInfo.ambientColor.g = Lsrc.color[1];
                        lightInfo.ambientColor.b = Lsrc.color[2];
                        lightInfo.ambientPower = Lsrc.intensity;
                        break;
                    }
                    case kTopoLight_Point: {
                        auto &L = pointLights[idxPointLight];
                        L.origin
                            = { Lsrc.point.position[0], Lsrc.point.position[1], Lsrc.point.position[2], 0 };
                        L.color = { Lsrc.color[0], Lsrc.color[1], Lsrc.color[2], 0 };
                        L.intensity = Lsrc.intensity;
                        idxPointLight += 1;
                        break;
                    }
                    }
                }
            }
        } else {
            for (size_t idxLight = 0; idxLight < scene->numLights; idxLight++) {
                auto &Lsrc = scene->lights[idxLight];
                switch (scene->lights[idxLight].kind) {
                case kTopoLight_Ambient: {
                    lightInfo.hasAmbient = 1;
                    lightInfo.ambientColor.r = Lsrc.color[0];
                    lightInfo.ambientColor.g = Lsrc.color[1];
                    lightInfo.ambientColor.b = Lsrc.color[2];
                    lightInfo.ambientPower = Lsrc.intensity;
                    break;
                }
                case kTopoLight_Point: {
                    auto &L = pointLights[idxPointLight];
                    L.origin = { Lsrc.point.position[0], Lsrc.point.position[1], Lsrc.point.position[2], 0 };
                    L.color = { Lsrc.color[0], Lsrc.color[1], Lsrc.color[2], 0 };
                    L.intensity = Lsrc.intensity;
                    idxPointLight += 1;
                    break;
                }
                }
            }
        }

        bm_flush_and_unmap_uniform_buffer(bm, uboPointLights);
    }

	void *pLightInfo;
	size_t discard;
    sm_get_draw_uniform_buffer(
            sm, idxCurrentFrame, pipelineId, sizeof(LightInfo), 1, &uboLightInfo, &stride);

	bm_map_uniform_buffer(bm, uboLightInfo, &pLightInfo, &discard);
    *((LightInfo *)pLightInfo) = lightInfo;
    bm_flush_and_unmap_uniform_buffer(bm, uboLightInfo);


    // Write light data descriptors
    VkDescriptorBufferInfo bufferInfos[2] = { {}, {} };

    bufferInfos[0].offset = 0;
    bufferInfos[0].range = VK_WHOLE_SIZE;
    bm_get_uniform_buffer_handle(bm, uboLightInfo, &bufferInfos[0].buffer);

    bufferInfos[1].offset = 0;
    bufferInfos[1].range = VK_WHOLE_SIZE;
    bm_get_uniform_buffer_handle(bm, uboPointLights, &bufferInfos[1].buffer);

    VkWriteDescriptorSet writes[2] = { {}, {} };

    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = set;
    writes[0].dstBinding = 4;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writes[0].pBufferInfo = &bufferInfos[0];

    writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[1].dstSet = set;
    writes[1].dstBinding = 5;
    writes[1].descriptorCount = 1;
    writes[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    writes[1].pBufferInfo = &bufferInfos[1];

    vkUpdateDescriptorSets(dev.handle, 2, &writes[0], 0, nullptr);

    // Bind the G-buffer textures
    rt_bind(rt, dev.currentSwapchain.idxCurrentFrame, dev.handle, temp, set);

    return true;
}

static bool InitPipelineDescriptor(
    void *user,
    arena_t &arena,
    const ClientMaterial &material,
    ShaderPipelineDescriptor &descriptor) {

    descriptor.passId = "pass_blit_to_swapchain";
    descriptor.vertexShaderId = "lighting/vert";
    descriptor.fragmentShaderId = "lighting/frag";
    descriptor.drawsToSwapchain = true;
    DescriptorSet *sets;
    arena.pushElements(3, &sets);
    descriptor.numDescriptorSets = 3;
    descriptor.arrDescriptorSets = sets;

    VkDescriptorSetLayoutBinding *uboLayoutBindings;

    // Set 0
    arena.pushElements(1, &uboLayoutBindings);
    sets[0].numBindings = 1;
    sets[0].arrBindings = uboLayoutBindings;
    uboLayoutBindings[0].binding = 0;
    uboLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBindings[0].descriptorCount = 1;
    uboLayoutBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    // Set 1
    arena.pushElements(1, &uboLayoutBindings);
    sets[1].numBindings = 1;
    sets[1].arrBindings = uboLayoutBindings;
    uboLayoutBindings[0].binding = 0;
    uboLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    uboLayoutBindings[0].descriptorCount = 1;
    uboLayoutBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    // Set 2
    arena.pushElements(6, &uboLayoutBindings);

    // Textures
    for (int i = 0; i < 4; i++) {
        uboLayoutBindings[i].binding = i;
        uboLayoutBindings[i].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        uboLayoutBindings[i].descriptorCount = 1;
        uboLayoutBindings[i].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    }

    // Point lights
    uboLayoutBindings[4].binding = 4;
    uboLayoutBindings[4].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBindings[4].descriptorCount = 1;
    uboLayoutBindings[4].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    // Point lights
    uboLayoutBindings[5].binding = 5;
    uboLayoutBindings[5].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    uboLayoutBindings[5].descriptorCount = 1;
    uboLayoutBindings[5].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    sets[2].numBindings = 6;
    sets[2].arrBindings = uboLayoutBindings;

    return true;
}

static MaterialDrawHandlerOps drawHandler = {
    GetDrawUniformSize, ComputePerDrawUniformData, ComputePerPassUniformData,
    GetPassUniformSize, UpdatePerMaterialUniforms, InitPipelineDescriptor,
};

REGISTER_MATERIAL_DRAW_HANDLER(kTopoMaterialKind_RenderTargetReceiver, &drawHandler, nullptr);
