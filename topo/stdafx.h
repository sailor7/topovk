#pragma once

#include <cassert>
#include <cstdint>

#include <array>
#include <atomic>
#include <functional>
#include <mutex>
#include <new>
#include <queue>
#include <thread>

#include <vulkan/vulkan.h>

#include <fmt/core.h>

#include <SDL.h>

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>
