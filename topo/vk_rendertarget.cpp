#include "vk_rendertarget.h"
#include "vk_debug.h"
#include "vk_instance.h"
#include "vk_shaders.h"
#include "vk_textures.h"

static void SaveString(arena_t &arena, const char **dst, const char* src) {
    char *temp = nullptr;
    arena.pushElements(strlen(src) + 1, &temp);
    strcpy(temp, src);
    *dst = temp;
}

static void FormatString(arena_t &arena, const char **out, const char *format, ...) {
    assert(out != nullptr);
    assert(format != nullptr);
    va_list va;
    va_start(va, format);
    auto sizBuf = vsnprintf(nullptr, 0, format, va) + 1;
    va_end(va);
    char *dst = nullptr;
    arena.pushElements(sizBuf, &dst);
    va_start(va, format);
    auto sizBuf1 = vsnprintf(dst, sizBuf, format, va) + 1;
    va_end(va);
    assert(sizBuf == sizBuf1);
    *out = dst;
}

struct RenderTarget {
    RenderTarget(arena_t &&arenaTemp, arena_t &&arenaPersistent)
        : arenaTemp(std::move(arenaTemp))
        , arenaPersistent(std::move(arenaPersistent)) { }

    arena_t arenaTemp;
    arena_t arenaPersistent;

    topo_instance_t instance = nullptr;
    RenderTargetInfo info;

    Texture *textures = nullptr;
    VkImageView *views = nullptr;

    VkFramebuffer hFramebuffers[NUM_MAX_FRAMES_IN_FLIGHT];

    VkImageLayout currentLayout;
};

static topo_result rt_free_owned_resources(RenderTarget* rt) {
    auto instance = rt->instance;
    VkDevice hDevice = instance->device.handle;
    auto &info = rt->info;
    const auto numTexturesTotalPerFrame = uint32_t(info.numColorAttachments) + info.numDepthAttachments;
    const auto numTexturesTotal = numTexturesTotalPerFrame * NUM_MAX_FRAMES_IN_FLIGHT;

    for (uint32_t idxFB = 0; idxFB < NUM_MAX_FRAMES_IN_FLIGHT; idxFB++) {
        if (rt->hFramebuffers[idxFB] != VK_NULL_HANDLE) {
            vkDestroyFramebuffer(hDevice, rt->hFramebuffers[idxFB], nullptr);
            rt->hFramebuffers[idxFB] = VK_NULL_HANDLE;
            for (uint32_t idxTexture = 0; idxTexture < numTexturesTotalPerFrame; idxTexture++) {
                tm_free(
                    instance->device.textureManager,
                    rt->textures[idxFB * numTexturesTotalPerFrame + idxTexture]);
            }
        }
    }

    memset(rt->textures, 0, numTexturesTotal * sizeof(Texture *));
    memset(rt->views, 0, numTexturesTotal * sizeof(VkImageView *));

    rt->arenaTemp.clear();

    return kTopo_OK;
}

static topo_result rt_recreate(RenderTarget *rt) {
    auto instance = rt->instance;
    VkDevice hDevice = instance->device.handle;
    VkFramebuffer hFramebuffer;
    VkResult result;


    auto &info = rt->info;
    const auto numTexturesTotalPerFrame = uint32_t(info.numColorAttachments) + info.numDepthAttachments;
    const auto numTexturesTotal = numTexturesTotalPerFrame * NUM_MAX_FRAMES_IN_FLIGHT;

    // Destroy old framebuffers and their attachments
    for (uint32_t idxFB = 0; idxFB < NUM_MAX_FRAMES_IN_FLIGHT; idxFB++) {
        if (rt->hFramebuffers[idxFB] != VK_NULL_HANDLE) {
            vkDestroyFramebuffer(hDevice, rt->hFramebuffers[idxFB], nullptr);
            rt->hFramebuffers[idxFB] = VK_NULL_HANDLE;
            for (uint32_t idxTexture = 0; idxTexture < numTexturesTotalPerFrame; idxTexture++) {
                tm_free(
                    instance->device.textureManager,
                    rt->textures[idxFB * numTexturesTotalPerFrame + idxTexture]);
            }
        }
    }

    memset(rt->textures, 0, numTexturesTotal * sizeof(Texture *));
    memset(rt->views, 0, numTexturesTotal * sizeof(VkImageView *));

    rt->arenaTemp.clear();

    Pass pass;
    if (!sm_get_pass(instance->device.shaderManager, rt->info.compatiblePassId, &pass)) {
        assert(!"Invalid pass");
        return kTopo_Failure;
    }

    unsigned idxNextTex = 0;

    char bufObjectLabel[128];

    for (uint32_t idxFB = 0; idxFB < NUM_MAX_FRAMES_IN_FLIGHT; idxFB++) {
        for (uint32_t idxColorAttachment = 0; idxColorAttachment < info.numColorAttachments;
             idxColorAttachment++) {
            const char *idTexture = nullptr;
            FormatString(rt->arenaTemp, &idTexture, "%s_tex%u", info.id, idxNextTex);

            assert(idTexture != nullptr);
            auto &attachmentInfo = info.pColorAttachments[idxColorAttachment];
            SamplerInfo samplerInfo;
            samplerInfo.addressModeU = TextureAddressMode::REPEAT;
            samplerInfo.addressModeV = TextureAddressMode::REPEAT;
            samplerInfo.addressModeW = TextureAddressMode::REPEAT;
            samplerInfo.anisotropy = TextureAnisotropy::OFF;
            samplerInfo.filterMag = TextureFilter::LINEAR;
            samplerInfo.filterMin = TextureFilter::LINEAR;
            tm_make_color_buffer(
                instance->device.textureManager, idTexture, info.width, info.height,
                attachmentInfo.pixelFormat, samplerInfo, &rt->textures[idxNextTex]);
            idxNextTex++;
        }

        assert(info.numDepthAttachments < 2);
        if (info.numDepthAttachments == 1) {
            const char *idTexture = nullptr;
            FormatString(rt->arenaTemp, &idTexture, "%s_tex%u", info.id, idxNextTex);

            assert(idTexture != nullptr);
            SamplerInfo samplerInfo;
            samplerInfo.addressModeU = TextureAddressMode::REPEAT;
            samplerInfo.addressModeV = TextureAddressMode::REPEAT;
            samplerInfo.addressModeW = TextureAddressMode::REPEAT;
            samplerInfo.anisotropy = TextureAnisotropy::OFF;
            samplerInfo.filterMag = TextureFilter::LINEAR;
            samplerInfo.filterMin = TextureFilter::LINEAR;
            tm_make_depth_buffer(
                instance->device.textureManager, idTexture, info.width, info.height, samplerInfo,
                &rt->textures[idxNextTex]);
            idxNextTex++;
        }

        auto idxTexturesBase = idxFB * numTexturesTotalPerFrame;
        tm_get_view_handles(
            instance->device.textureManager, numTexturesTotalPerFrame, &rt->textures[idxTexturesBase],
            &rt->views[idxTexturesBase]);

        VkFramebufferCreateInfo framebufferCreateInfo;
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.pNext = nullptr;
        framebufferCreateInfo.renderPass = pass.handle;
        framebufferCreateInfo.width = info.width;
        framebufferCreateInfo.height = info.height;
        framebufferCreateInfo.layers = 1;
        framebufferCreateInfo.flags = 0;
        framebufferCreateInfo.attachmentCount = numTexturesTotalPerFrame;
        framebufferCreateInfo.pAttachments = &rt->views[idxTexturesBase];

        result = vkCreateFramebuffer(hDevice, &framebufferCreateInfo, nullptr, &rt->hFramebuffers[idxFB]);
        assert(result == VK_SUCCESS);
        if (result != VK_SUCCESS) {
            assert(!"Failed to create framebuffer");
            return kTopo_Failure;
        }

        snprintf(bufObjectLabel, 127, "%s_fb%u\n", info.id, idxFB);
        VkDebugUtilsObjectNameInfoEXT nameInfo = {};
        nameInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
        nameInfo.objectHandle = (uint64_t)rt->hFramebuffers[idxFB];
        nameInfo.objectType = VK_OBJECT_TYPE_FRAMEBUFFER;
        nameInfo.pObjectName = bufObjectLabel;
        vkTrySetDebugUtilsObjectNameTOPO(hDevice, &nameInfo);
    }

    return kTopo_OK;
}

topo_result rt_create(topo_instance_t instance, const RenderTargetInfo& info, RenderTarget_t* out) {
    arena_t arenaTemp { arena_t::kilobytes(4) };
    arena_t arenaPersistent = { arena_t::kilobytes(4) };

    auto numTexturesTotal = NUM_MAX_FRAMES_IN_FLIGHT * (uint32_t(info.numColorAttachments) + info.numDepthAttachments);

    Texture *textures = nullptr;
    VkImageView *views = nullptr;

    arenaPersistent.pushElements(numTexturesTotal, &textures);
    arenaPersistent.pushElements(numTexturesTotal, &views);

    memset(textures, 0, numTexturesTotal * sizeof(Texture *));
    memset(views, 0, numTexturesTotal * sizeof(VkImageView *));

    RenderTargetInfo savedInfo = info;
    arenaPersistent.pushElements(savedInfo.numColorAttachments, &savedInfo.pColorAttachments);
    memcpy(
        (ColorAttachment *)savedInfo.pColorAttachments, info.pColorAttachments,
        sizeof(ColorAttachment) * savedInfo.numColorAttachments);

    SaveString(arenaPersistent, &savedInfo.id, info.id);
    SaveString(arenaPersistent, &savedInfo.compatiblePassId, info.compatiblePassId);

    auto *rt
        = new RenderTarget(std::move(arenaTemp), std::move(arenaPersistent));

    rt->instance = instance;
    rt->info = savedInfo;
    rt->textures = textures;
    rt->views = views;
    for (uint32_t i = 0; i < NUM_MAX_FRAMES_IN_FLIGHT; i++) {
        rt->hFramebuffers[i] = VK_NULL_HANDLE;
    }

    auto res = rt_recreate(rt);

    if (res != kTopo_OK) {
        delete rt;
        return res;
    }

    *out = rt;

    return res;
}

topo_result rt_resize(RenderTarget_t renderTarget, uint32_t width, uint32_t height) {
    renderTarget->info.width = width;
    renderTarget->info.height = height;

    return rt_recreate(renderTarget);
}

topo_result rt_destroy(RenderTarget_t renderTarget) {
    rt_free_owned_resources(renderTarget);
    delete renderTarget;

    return kTopo_OK;
}

topo_result rt_prepare_for_display(RenderTarget_t renderTarget, uint32_t idxImage, VkCommandBuffer cmdBuffer) {
    auto &info = renderTarget->info;
    auto numTexturesTotalPerFrame = (uint32_t(info.numColorAttachments) + info.numDepthAttachments);
    for (size_t idxAttachment = 0; idxAttachment < info.numColorAttachments; idxAttachment++) {
        auto hTexture = renderTarget->textures[idxImage * numTexturesTotalPerFrame + idxAttachment];
        tm_transition_layout(
            renderTarget->instance->device.textureManager, hTexture, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            cmdBuffer);
    }
    return kTopo_OK;
}

topo_result
rt_prepare_for_rendering(RenderTarget_t renderTarget, uint32_t idxImage, VkCommandBuffer cmdBuffer) {
    auto &info = renderTarget->info;
    auto numTexturesTotalPerFrame = (uint32_t(info.numColorAttachments) + info.numDepthAttachments);
    for (size_t idxAttachment = 0; idxAttachment < info.numColorAttachments; idxAttachment++) {
        auto hTexture = renderTarget->textures[idxImage * numTexturesTotalPerFrame + idxAttachment];
        tm_transition_layout(
            renderTarget->instance->device.textureManager, hTexture, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            cmdBuffer);
    }
    return kTopo_OK;
}

topo_result
rt_bind(RenderTarget_t renderTarget, uint32_t idxImage, VkRenderPassBeginInfo &renderPassBeginInfo) {
    renderPassBeginInfo.framebuffer = renderTarget->hFramebuffers[idxImage];
    return kTopo_OK;
}

topo_result rt_bind(RenderTarget_t renderTarget, uint32_t idxImage, uint32_t idxAttachment, VkDescriptorImageInfo& imageInfo) {
    auto &info = renderTarget->info;
    auto numTexturesTotalPerFrame = (uint32_t(info.numColorAttachments) + info.numDepthAttachments);
    auto hTexture = renderTarget->textures[idxImage * numTexturesTotalPerFrame + idxAttachment];
    tm_get_sampler_handle(renderTarget->instance->device.textureManager, hTexture, &imageInfo.imageView, &imageInfo.sampler);
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    return kTopo_OK;
}

topo_result rt_bind(
    RenderTarget_t renderTarget,
    uint32_t idxImage,
    VkDevice dev,
    arena_t &temp,
    VkDescriptorSet set) {
    auto &info = renderTarget->info;
    auto numColorAttachments = info.numColorAttachments;
    auto numTexturesTotalPerFrame = (uint32_t(numColorAttachments) + info.numDepthAttachments);

    VkDescriptorImageInfo *imageInfos = nullptr;
    temp.pushElements(numColorAttachments, &imageInfos);

    for (size_t idxAttachment = 0; idxAttachment < numColorAttachments; idxAttachment++) {
        auto &imageInfo = imageInfos[idxAttachment];
        auto hTexture = renderTarget->textures[idxImage * numTexturesTotalPerFrame + idxAttachment];
        auto res = tm_get_sampler_handle(
            renderTarget->instance->device.textureManager, hTexture, &imageInfo.imageView,
            &imageInfo.sampler);
        assert(res);
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    VkWriteDescriptorSet writes[1] = { {}, };

    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = set;
    writes[0].dstBinding = 0;
    writes[0].descriptorCount = numColorAttachments;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[0].pImageInfo = imageInfos;

    vkUpdateDescriptorSets(dev, 1, &writes[0], 0, nullptr);
    return kTopo_OK;
}
