#include <fmt/core.h>

#include <topo/topo.h>

#include "vk_common.h"
#include "vk_instance.h"
#include "vk_mesh.h"
#include "vk_textures.h"

#define TAG "[topo::res] "

template <typename T, size_t limit, typename A> T *find_free_slot(A &arr, uint32_t &idx) {
    T *slot = nullptr;
    idx = uint32_t(-1);

    auto &entries = arr.entries;

    for (uint32_t i = 0; i < arr.idxNextUnused && slot == nullptr; i++) {
        if (!entries[i].used) {
            slot = &entries[i];
            idx = i;
        }
    }

    if (slot == nullptr) {
        assert(arr.idxNextUnused != limit);
        slot = &entries[arr.idxNextUnused];
        idx = arr.idxNextUnused;
        arr.idxNextUnused++;
    }

    assert(idx != uint32_t(-1));
    assert(slot != nullptr);
    return slot;
}

static void free_borrowed_memory(void *ptr) { delete[](uint8_t *) ptr; }

TOPO_API TopoResult topo_create_image(
    TopoInstance instance,
    const TopoImageDescriptor *descriptor,
    TopoImage *out_handle) {
    if (instance == nullptr || descriptor == nullptr || out_handle == nullptr)
        return Topo_Failure;

    uint32_t idxImage;
    auto *image = find_free_slot<ClientImage, NUM_MAX_IMAGES>(instance->device.images, idxImage);
    image->used = true;
    image->pending = true;

    *out_handle = (TopoImage)idxImage;
    TopoImageDescriptor descriptorCopy = *descriptor;

    size_t sizData;
    switch (descriptorCopy.format) {
    case TopoIF_RGBA32:
    case TopoIF_RGBA32_SRGB:
    case TopoIF_BGRA32:
    case TopoIF_BGRA32_SRGB: {
        sizData = descriptorCopy.width * descriptorCopy.height * 4;
        break;
    }
    default: {
        assert(!"Unhandled pixel format");
        break;
    }
    }

    VkFormat format;
    switch (descriptorCopy.format) {
    case TopoIF_RGBA32: {
        format = VK_FORMAT_R8G8B8A8_UNORM;
        break;
    }
    case TopoIF_RGBA32_SRGB: {
        format = VK_FORMAT_R8G8B8A8_SRGB;
        break;
    }
    case TopoIF_BGRA32: {
        format = VK_FORMAT_B8G8R8A8_UNORM;
        break;
    }
    case TopoIF_BGRA32_SRGB: {
        format = VK_FORMAT_B8G8R8A8_SRGB;
        break;
    }
    }

    auto task = [idxImage, instance, descriptorCopy, format]() -> void {
        Texture hImage;
        {
            SamplerInfo samplerInfo = {};
            samplerInfo.filterMin = TextureFilter::LINEAR;
            samplerInfo.filterMag = TextureFilter::LINEAR;
            samplerInfo.anisotropy = TextureAnisotropy::ON;
            samplerInfo.addressModeU = TextureAddressMode::REPEAT;
            samplerInfo.addressModeV = TextureAddressMode::REPEAT;
            samplerInfo.addressModeW = TextureAddressMode::REPEAT;
            if (!tm_make(
                    instance->device.textureManager, descriptorCopy.width, descriptorCopy.height, format,
                    descriptorCopy.pData, samplerInfo, &hImage)) {
                fmt::print(TAG "Failed to create texture\n");
            }

            instance->device.images.entries[idxImage].tmHandle = hImage;
            instance->device.images.entries[idxImage].pending = false;

            free_borrowed_memory((void *)descriptorCopy.pData);
        }
    };

    {
        std::scoped_lock G(instance->queuedTasksLock);
        instance->queuedTasks.push(std::move(task));
    }

    return kTopo_OK;
}

TOPO_API topo_result topo_create_material(
    topo_instance_t instance,
    const topo_material_descriptor *descriptor,
    topo_material_t *out_handle) {
    if (instance == nullptr || descriptor == nullptr || out_handle == nullptr)
        return kTopo_Failure;

    auto &materials = instance->device.materials;
    uint32_t idxMaterial;
    auto *material = find_free_slot<ClientMaterial, NUM_MAX_MATERIALS>(materials, idxMaterial);

    material->used = true;

    *out_handle = (topo_material_t)idxMaterial;

    auto lenId = strlen(descriptor->id);
    char *id;
    instance->arenaScene.pushElements(lenId + 1, &id);
    memcpy(id, descriptor->id, lenId + 1);

    material->id.id = id;
    material->id.lenId = lenId;
    material->id.hash = hash_id(0, id, lenId);

    switch (descriptor->kind) {
    case kTopoMaterialKind_UnlitColor: {
        material->kind = Material_UnlitColor;
        material->unlitColor.colorR = descriptor->unlitColor.colorR;
        material->unlitColor.colorG = descriptor->unlitColor.colorG;
        material->unlitColor.colorB = descriptor->unlitColor.colorB;
        break;
    }
    case kTopoMaterialKind_UnlitTextured: {
        material->kind = Material_UnlitTextured;
        material->unlitTextured.imageBaseColor = descriptor->unlitTextured.imageBaseColor;
        break;
    }
    case kTopoMaterialKind_RenderTargetReceiver: {
        material->kind = Material_RenderTargetReceiver;
        material->renderTargetReceiver.renderTarget = (RenderTarget_t)descriptor->rtReceiver.renderTarget;
        break;
    }
    case kTopoMaterialKind_NormalMapped: {
        material->kind = Material_NormalMapped;
        material->normalMapped.imageBaseColor = descriptor->normalMapped.imageBaseColor;
        material->normalMapped.imageNormalMap = descriptor->normalMapped.imageNormalMap;
        material->normalMapped.imageMetallicRoughness = descriptor->normalMapped.imageMetallicRoughness;
        break;
    }
    }

    *out_handle = topo_material_t(idxMaterial);

    return kTopo_OK;
}

TOPO_API topo_result
topo_create_mesh(topo_instance_t instance, const topo_mesh_descriptor *descriptor, topo_mesh_handle *out) {
    if (!instance || !descriptor || !out) {
        return kTopo_Failure;
    }

    auto &meshes = instance->device.meshes;
    uint32_t idxMesh;
    auto *mesh = find_free_slot<ClientMesh, NUM_MAX_MESHES>(meshes, idxMesh);

    mesh->used = true;
    mesh->pending = true;
    *out = topo_mesh_handle(idxMesh);

    auto task = [instance, descriptor, idxMesh]() -> void {
        auto &mesh = instance->device.meshes.entries[idxMesh];
        if (!mm_make(instance->device.meshManager, descriptor, &mesh.handle)) {
            fmt::print(TAG "Failed to make mesh\n");
        }

        mesh.pending = false;

        auto *vertexBuffers = descriptor->pVertexBuffers;
        for (uint32_t i = 0; i < descriptor->numVertexBuffers; i++) {
            free_borrowed_memory((void *)vertexBuffers[i].pVertexData);
        }
        free_borrowed_memory((void *)vertexBuffers);

        free_borrowed_memory((void *)descriptor->pAttributes);

        for (uint32_t i = 0; i < descriptor->numSubmeshes; i++) {
            free_borrowed_memory((void *)descriptor->pSubmeshes[i].indexBuffer.pIndices);
        }

        free_borrowed_memory((void *)descriptor->pSubmeshes);
        free_borrowed_memory((void *)descriptor);
    };

    {
        std::scoped_lock G(instance->queuedTasksLock);
        instance->queuedTasks.push(std::move(task));
    }

    return kTopo_OK;
}

TOPO_API topo_result topo_destroy_mesh(topo_instance_t instance, topo_mesh_handle *handle) {
    if (!instance || !handle) {
        return kTopo_Failure;
    }
    
    auto &meshes = instance->device.meshes;
    auto hMesh = size_t(*handle);
    auto &mesh = meshes.entries[hMesh];

    if (!mesh.used) {
        return kTopo_Failure;
    }

    assert((*handle) != topo_mesh_handle(-1));

    while (mesh.pending) { }
    auto hMMHandle = mesh.handle;
    assert(hMMHandle != nullptr);

    auto task = [instance, hMMHandle]() -> void {
        mm_release(instance->device.meshManager, hMMHandle);
    };

    {
        std::scoped_lock G(instance->queuedTasksLock);
        instance->queuedTasks.push(std::move(task));
    }

    mesh.used = false;
    mesh.pending = false;
    mesh.handle = topo_mesh_handle(0);
    *handle = topo_mesh_handle(-1);
    return kTopo_OK;
}

TOPO_API topo_result topo_destroy_material(topo_instance_t instance, topo_material_t *handle) {
    if (!instance || !handle) {
        return kTopo_Failure;
    }

    auto &materials = instance->device.materials;
    auto &material = materials.entries[size_t(*handle)];

    if (!material.used) {
        return kTopo_Failure;
    }

    material.used = false;

    return kTopo_OK;
}

TOPO_API topo_result topo_destroy_image(topo_instance_t instance, topo_image_t *handle) {
    if (!instance || !handle) {
        return kTopo_Failure;
    }

    auto &images = instance->device.images;
    auto &image = images.entries[size_t(*handle)];

    if (!image.used) {
        return kTopo_Failure;
    }

    auto hTmHandle = image.tmHandle;

    image.used = false;
    image.tmHandle = 0;
    image.pending = false;

    auto task = [instance, hTmHandle]() -> void {
        tm_release(instance->device.textureManager, hTmHandle);
    };

    {
        std::scoped_lock G(instance->queuedTasksLock);
        instance->queuedTasks.push(std::move(task));
    }

    return kTopo_OK;
}

TOPO_API topo_result topo_borrow_memory(topo_instance_t instance, size_t size, void **out_ptr) {
    if (!instance || !out_ptr)
        return kTopo_Failure;

    *out_ptr = new uint8_t[size];
    return kTopo_OK;
}
