#include <topo/topo.h>

#include "vk_buffers.h"
#include "vk_common.h"
#include "vk_debug.h"
#include "vk_descriptor_pool_list.h"
#include "vk_instance.h"
#include "vk_mesh.h"
#include "vk_rendertarget.h"
#include "vk_shaders.h"
#include "vk_textures.h"

#include <SDL_vulkan.h>

#include <algorithm>
#include <condition_variable>
#include <fmt/core.h>
#include <mutex>
#include <thread>

#include <tracy/Tracy.hpp>

#define TAG "[topo::core] "

static int sdl_init_counter = 0;
static const char *gRequiredExtensions[]
    = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_EXT_MEMORY_BUDGET_EXTENSION_NAME,
        VK_KHR_MAINTENANCE3_EXTENSION_NAME, nullptr };
static const char *gWantedInstanceExtensions[]
    = { VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME, nullptr };
static const uint32_t gNumWantedInstanceExtensions
    = sizeof(gWantedInstanceExtensions) / sizeof(gWantedInstanceExtensions[0]) - 1;

static bool collectInstanceExtensions(
    topo_instance_t instance,
    const char ***pppInstanceExtensions,
    uint32_t *pNumInstanceExtensions) {
    uint32_t numRequiredInstanceExtensions = 0;
    if (!SDL_Vulkan_GetInstanceExtensions(instance->pWindow, &numRequiredInstanceExtensions, nullptr)) {
        fprintf(stderr, TAG "Couldn't get number of instance extensions\n");
        return false;
    }

    uint32_t numInstExtProps = 0;
    VkExtensionProperties *pExtensionProps = nullptr;
    vkEnumerateInstanceExtensionProperties(NULL, &numInstExtProps, pExtensionProps);
    instance->arenaSystem.pushElements(numInstExtProps, &pExtensionProps);
    vkEnumerateInstanceExtensionProperties(NULL, &numInstExtProps, pExtensionProps);

    for (uint32_t idxReqExt = 0; idxReqExt < gNumWantedInstanceExtensions; idxReqExt++) {
        bool found = false;
        for (uint32_t idxProp = 0; idxProp < numInstExtProps; idxProp++) {
            if (strcmp(pExtensionProps[idxProp].extensionName, gWantedInstanceExtensions[idxReqExt]) == 0) {
                found = true;
                break;
            }
        }

        if (!found) {
            fmt::print(TAG "Instance extension {} is not supported\n", gWantedInstanceExtensions[idxReqExt]);
            return false;
        }
    }

    uint32_t numTotalInstanceExtensions = numRequiredInstanceExtensions + gNumWantedInstanceExtensions;

    const char **ppInstanceExtensions = nullptr;
    instance->arenaSystem.push(
        numTotalInstanceExtensions * sizeof(const char *), (void **)&ppInstanceExtensions);

    if (!SDL_Vulkan_GetInstanceExtensions(
            instance->pWindow, &numRequiredInstanceExtensions, ppInstanceExtensions)) {
        fprintf(stderr, TAG "Couldn't get required instance extensions\n");
        return false;
    }

    for (uint32_t idxWantedExt = 0; idxWantedExt < gNumWantedInstanceExtensions; idxWantedExt++) {
        ppInstanceExtensions[numRequiredInstanceExtensions + idxWantedExt]
            = gWantedInstanceExtensions[idxWantedExt];
    }

    fmt::print(TAG "Instance extensions used: ");
    for (uint32_t idxExt = 0; idxExt < numTotalInstanceExtensions; idxExt++) {
        fmt::print("{} ", ppInstanceExtensions[idxExt]);
    }
    fmt::print("\n");

    *pppInstanceExtensions = ppInstanceExtensions;
    *pNumInstanceExtensions = numTotalInstanceExtensions;

    return true;
}

static bool create_vk_instance(topo_instance_t instance, const topo_instance_descriptor *descriptor) {
    VkApplicationInfo appInfo {};
    VkInstanceCreateInfo instInfo {};
    uint32_t numLayers = 0;
    VkLayerProperties *pLayerProps = nullptr;
    const char **pLayerNames = nullptr;

    const char **ppInstanceExtensions;
    uint32_t numInstanceExtensions;
    if (!collectInstanceExtensions(instance, &ppInstanceExtensions, &numInstanceExtensions)) {
        return false;
    }

    if (vkEnumerateInstanceLayerProperties(&numLayers, pLayerProps) != VK_SUCCESS) {
        fprintf(stderr, TAG "Couldn't get number of layers\n");
        return false;
    }

    instance->arenaSystem.push(numLayers * sizeof(VkLayerProperties), (void **)&pLayerProps);

    if (vkEnumerateInstanceLayerProperties(&numLayers, pLayerProps) != VK_SUCCESS) {
        fprintf(stderr, TAG "Couldn't get list of layers\n");
        return false;
    }

    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = descriptor->title;
    appInfo.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.pEngineName = "topovk.easimer.net";
    appInfo.engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instInfo.pApplicationInfo = &appInfo;
    instInfo.enabledExtensionCount = numInstanceExtensions;
    instInfo.ppEnabledExtensionNames = ppInstanceExtensions;
    instInfo.pNext = nullptr;

    static const char *gDebugLayers[]
        = { "VK_LAYER_KHRONOS_validation", "VK_LAYER_RENDERDOC_Capture", nullptr };

    if (descriptor->enable_validation) {
        instance->arenaSystem.push(numLayers * sizeof(const char *), (void **)&pLayerNames);

        uint32_t numEnabledLayers = 0;
        printf(TAG "Layers available (* enabled):\n");
        for (unsigned idxLayer = 0; idxLayer < numLayers; idxLayer++) {
            printf("- %s", pLayerProps[idxLayer].layerName);
            for (unsigned idxDebugLayer = 0; gDebugLayers[idxDebugLayer]; idxDebugLayer++) {
                if (strcmp(pLayerProps[idxLayer].layerName, gDebugLayers[idxDebugLayer]) == 0) {
                    pLayerNames[numEnabledLayers] = pLayerProps[idxLayer].layerName;
                    numEnabledLayers++;
                    printf(" *");
                    break;
                }
            }
            printf("\n");
        }
        printf("\n");
        instInfo.ppEnabledLayerNames = pLayerNames;
        instInfo.enabledLayerCount = numEnabledLayers;
    } else {
        instInfo.ppEnabledLayerNames = nullptr;
        instInfo.enabledLayerCount = 0;
    }

    auto res = vkCreateInstance(&instInfo, nullptr, &instance->pVkInst);
    if (res != VK_SUCCESS) {
        instance->arenaSystem.clear();
        return false;
    }

    instance->arenaSystem.clear();

    if (!SDL_Vulkan_CreateSurface(instance->pWindow, instance->pVkInst, &instance->surface)) {
        return false;
    }

    vkTryInitDebugUtils(instance->pVkInst);

    return true;
}

static bool pick_physical_device(topo_instance_t instance, const topo_instance_descriptor *descriptor) {
    uint32_t numDevices = 0;
    VkPhysicalDevice *pDevices = nullptr;
    PhysicalDevice *pDeviceProps = nullptr;

    if (vkEnumeratePhysicalDevices(instance->pVkInst, &numDevices, pDevices) != VK_SUCCESS) {
        fprintf(stderr, TAG "Couldn't get number of physical devices\n");
        return false;
    }

    instance->arenaSystem.push(numDevices * sizeof(VkPhysicalDevice), (void **)&pDevices);

    if (vkEnumeratePhysicalDevices(instance->pVkInst, &numDevices, pDevices) != VK_SUCCESS) {
        fprintf(stderr, TAG "Couldn't get list of physical devices\n");
        return false;
    }

    instance->arenaSystem.push(numDevices * sizeof(PhysicalDevice), (void **)&pDeviceProps);
    for (uint32_t idxDev = 0; idxDev < numDevices; idxDev++) {
        VkPhysicalDeviceProperties devProps;
        VkPhysicalDeviceFeatures devFeatures;
        uint32_t numQueueFamilyProps = 0;
        VkQueueFamilyProperties *pQueueFams = nullptr;
        uint32_t numExtensions = 0;
        VkExtensionProperties *pExtProps = nullptr;

        auto *props = &pDeviceProps[idxDev];
        memset(props, 0, sizeof(*props));
        props->handle = pDevices[idxDev];

        vkGetPhysicalDeviceQueueFamilyProperties(pDevices[idxDev], &numQueueFamilyProps, pQueueFams);
        instance->arenaSystem.push(
            numQueueFamilyProps * sizeof(VkQueueFamilyProperties), (void **)&pQueueFams);
        vkGetPhysicalDeviceQueueFamilyProperties(pDevices[idxDev], &numQueueFamilyProps, pQueueFams);

        for (uint32_t idxQueueFam = 0; idxQueueFam < numQueueFamilyProps; idxQueueFam++) {
            if (!props->supportsGraphics && pQueueFams[idxQueueFam].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                props->supportsGraphics = true;
                props->idxQueueGraphics = idxQueueFam;
                printf(TAG "Device #%u supports graphics (queue %u)\n", idxDev, idxQueueFam);
            }
            if (!props->supportsCompute && pQueueFams[idxQueueFam].queueFlags & VK_QUEUE_COMPUTE_BIT) {
                props->supportsCompute = true;
                props->idxQueueCompute = idxQueueFam;
                printf(TAG "Device #%u supports compute (queue %u)\n", idxDev, idxQueueFam);
            }
            if (!props->supportsTransfer && pQueueFams[idxQueueFam].queueFlags & VK_QUEUE_TRANSFER_BIT) {
                props->supportsTransfer = true;
                props->idxQueueTransfer = idxQueueFam;
                printf(TAG "Device #%u supports transfer (queue %u)\n", idxDev, idxQueueFam);
            }

            VkBool32 presentSupport = false;
            if (!props->supportsPresent) {
                vkGetPhysicalDeviceSurfaceSupportKHR(
                    pDevices[idxDev], idxQueueFam, instance->surface, &presentSupport);
                if (presentSupport) {
                    printf(TAG "Device #%u supports presentation (queue %u)\n", idxDev, idxQueueFam);
                    props->supportsPresent = true;
                    props->idxQueuePresent = idxQueueFam;
                }
            }
        }

        vkGetPhysicalDeviceProperties(pDevices[idxDev], &devProps);
        vkGetPhysicalDeviceFeatures(pDevices[idxDev], &devFeatures);

        props->isDiscrete = devProps.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
        props->isGpu = props->isDiscrete || devProps.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;

        vkEnumerateDeviceExtensionProperties(pDevices[idxDev], nullptr, &numExtensions, pExtProps);
        instance->arenaSystem.push(numExtensions * sizeof(pExtProps[0]), (void **)&pExtProps);
        vkEnumerateDeviceExtensionProperties(pDevices[idxDev], nullptr, &numExtensions, pExtProps);

        props->supportsAllRequiredExtensions = true;
        for (uint32_t idxReqExt = 0; gRequiredExtensions[idxReqExt] != nullptr; idxReqExt++) {
            uint32_t idxExt;
            for (idxExt = 0; idxExt < numExtensions; idxExt++) {
                if (strcmp(pExtProps[idxExt].extensionName, gRequiredExtensions[idxReqExt]) == 0) {
                    break;
                }
            }
            if (idxExt == numExtensions) {
                printf(
                    TAG "Device #%u doesn't support required extension '%s'\n", idxDev,
                    gRequiredExtensions[idxReqExt]);
                props->supportsAllRequiredExtensions = false;
                break;
            }
        }
    }

    PhysicalDevice *bestDevice = nullptr;
    for (uint32_t idxDev = 0; idxDev < numDevices; idxDev++) {
        if (!pDeviceProps[idxDev].isGpu) {
            printf("Device %u not a gpu\n", idxDev);
            continue;
        }

        if (!pDeviceProps[idxDev].supportsAllRequiredExtensions) {
            printf("Device %u ext support\n", idxDev);
            continue;
        }
        if (!pDeviceProps[idxDev].supportsGraphics) {
            printf("Device %u no gfx\n", idxDev);
            continue;
        }
        if (!pDeviceProps[idxDev].supportsCompute) {
            printf("Device %u no compute\n", idxDev);
            continue;
        }
        if (!pDeviceProps[idxDev].supportsTransfer) {
            printf("Device %u no transfer\n", idxDev);
            continue;
        }
        bestDevice = &pDeviceProps[idxDev];
    }

    if (bestDevice) {
        fprintf(stderr, TAG "Found usable GPU\n");
        instance->physicalDevice = *bestDevice;
        instance->arenaSystem.clear();
        return true;
    }

    fprintf(stderr, TAG "No usable GPU found\n");
    instance->arenaSystem.clear();

    return false;
}

#define InsertIntoSet(set, lenSet, sizSet, elem)                                                             \
    {                                                                                                        \
        uint32_t idx;                                                                                        \
        for (idx = 0; idx < lenSet; idx++) {                                                                 \
            if (set[idx] == elem)                                                                            \
                break;                                                                                       \
        }                                                                                                    \
        set[idx] = elem;                                                                                     \
        lenSet = idx + 1;                                                                                    \
    }

static bool create_logical_device(topo_instance_t instance, const topo_instance_descriptor *descriptor) {
    VkDeviceCreateInfo createInfo {};
    VkDeviceQueueCreateInfo *queueCreateInfo = nullptr;
    VkPhysicalDeviceFeatures deviceFeatures {};
    const float queuePriority = 1.0f;

    uint32_t *queueFamIndicies = nullptr;
    uint32_t numUniqueQueueFams = 0;

    instance->arenaSystem.push(4 * sizeof(uint32_t), (void **)&queueFamIndicies);
    InsertIntoSet(queueFamIndicies, numUniqueQueueFams, 4, instance->physicalDevice.idxQueueGraphics);
    InsertIntoSet(queueFamIndicies, numUniqueQueueFams, 4, instance->physicalDevice.idxQueuePresent);

    instance->arenaSystem.push(numUniqueQueueFams * sizeof(VkDeviceCreateInfo), (void **)&queueCreateInfo);

    for (uint32_t idxQueueFam = 0; idxQueueFam < numUniqueQueueFams; idxQueueFam++) {
        queueCreateInfo[idxQueueFam] = {};
        queueCreateInfo[idxQueueFam].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo[idxQueueFam].queueFamilyIndex = queueFamIndicies[idxQueueFam];
        queueCreateInfo[idxQueueFam].queueCount = 1;
        queueCreateInfo[idxQueueFam].pQueuePriorities = &queuePriority;
    }

    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfo;
    createInfo.queueCreateInfoCount = numUniqueQueueFams;

    deviceFeatures.samplerAnisotropy = VK_TRUE;

    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.ppEnabledExtensionNames = gRequiredExtensions;
    createInfo.enabledExtensionCount = 1;

    auto res = vkCreateDevice(instance->physicalDevice.handle, &createInfo, nullptr, &instance->device.handle)
        == VK_SUCCESS;

    if (res) {
        vkGetDeviceQueue(
            instance->device.handle, instance->physicalDevice.idxQueueGraphics, 0,
            &instance->device.queueGraphics);
        vkGetDeviceQueue(
            instance->device.handle, instance->physicalDevice.idxQueuePresent, 0,
            &instance->device.queuePresent);
    }

    instance->arenaSystem.clear();

    return res;
}

static bool query_swapchain_support(topo_instance_t instance) {
    auto *details = &instance->device.swapchainSupportDetails;
    memset(details, 0, sizeof(SwapchainSupportDetails));
    auto device = instance->physicalDevice.handle;
    auto surface = instance->surface;

    if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details->caps) != VK_SUCCESS) {
        return false;
    }

    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &details->numFormats, details->formats);
    instance->device.arenaSwapchain.push(
        details->numFormats * sizeof(details->formats[0]), (void **)&details->formats);
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &details->numFormats, details->formats);

    vkGetPhysicalDeviceSurfacePresentModesKHR(
        device, surface, &details->numPresentModes, details->presentModes);
    instance->device.arenaSwapchain.push(
        details->numPresentModes * sizeof(details->presentModes[0]), (void **)&details->presentModes);
    vkGetPhysicalDeviceSurfacePresentModesKHR(
        device, surface, &details->numPresentModes, details->presentModes);

    return true;
}

static bool pick_surface_format(topo_instance_t instance) {
    auto numFormats = instance->device.swapchainSupportDetails.numFormats;
    auto *formats = instance->device.swapchainSupportDetails.formats;
    for (uint32_t idxFormat = 0; idxFormat < numFormats; idxFormat++) {
        if (formats[idxFormat].format == VK_FORMAT_B8G8R8A8_SRGB
            && formats[idxFormat].colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR) {
            instance->idxSurfaceFormat = idxFormat;
            return true;
        }
    }

    instance->idxSurfaceFormat = 0;
    return true;
}

static bool pick_present_mode(topo_instance_t instance) {
    auto numModes = instance->device.swapchainSupportDetails.numPresentModes;
    auto *modes = instance->device.swapchainSupportDetails.presentModes;
    for (uint32_t idxMode = 0; idxMode < numModes; idxMode++) {
        if (modes[idxMode] == VK_PRESENT_MODE_FIFO_KHR) {
            instance->idxPresentMode = idxMode;
            return true;
        }
    }

    fprintf(stderr, TAG "WARNING: FIFO present mode is unsupported!\n");
    instance->idxPresentMode = 0;
    return true;
}

static bool choose_swap_extent(topo_instance_t instance) {
    auto &caps = instance->device.swapchainSupportDetails.caps;

    if (caps.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        instance->extSwap = caps.currentExtent;
        return true;
    }

    int width, height;
    SDL_Vulkan_GetDrawableSize(instance->pWindow, &width, &height);
    instance->extSwap = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };

    instance->extSwap.width
        = std::clamp(instance->extSwap.width, caps.minImageExtent.width, caps.maxImageExtent.width);
    instance->extSwap.height
        = std::clamp(instance->extSwap.height, caps.minImageExtent.height, caps.maxImageExtent.height);
    return true;
}

static bool create_swapchain(topo_instance_t instance) {
    auto &swapChainDetails = instance->device.swapchainSupportDetails;
    auto numImages = swapChainDetails.caps.minImageCount + 1;
    if (swapChainDetails.caps.maxImageCount > 0 && numImages > swapChainDetails.caps.maxImageCount) {
        numImages = swapChainDetails.caps.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = instance->surface;
    createInfo.minImageCount = numImages;
    createInfo.imageFormat = swapChainDetails.formats[instance->idxSurfaceFormat].format;
    createInfo.imageColorSpace = swapChainDetails.formats[instance->idxSurfaceFormat].colorSpace;
    createInfo.imageExtent = instance->extSwap;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    createInfo.preTransform = swapChainDetails.caps.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = swapChainDetails.presentModes[instance->idxPresentMode];
    createInfo.clipped = VK_TRUE;

    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

    uint32_t queueFams[2]
        = { instance->physicalDevice.idxQueueGraphics, instance->physicalDevice.idxQueuePresent };

    if (queueFams[0] != queueFams[1]) {
        // Different queue families; access will be concurrent
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFams;
    }

    auto &device = instance->device;

    auto res
        = vkCreateSwapchainKHR(instance->device.handle, &createInfo, nullptr, &device.currentSwapchain.handle)
        == VK_SUCCESS;

    if (res) {
        auto &swapchain = device.currentSwapchain;
        swapchain.arena.clear();
        vkGetSwapchainImagesKHR(device.handle, swapchain.handle, &swapchain.numImages, nullptr);
        // TODO(danielm): separate arena for the swapchain stuff since we may need to recreate it
        swapchain.arena.push(swapchain.numImages * sizeof(VkImage), (void **)&swapchain.images);
        swapchain.arena.push(swapchain.numImages * sizeof(VkImageView), (void **)&swapchain.imageViews);
        vkGetSwapchainImagesKHR(
            device.handle, device.currentSwapchain.handle, &swapchain.numImages, swapchain.images);

        for (uint32_t idxImage = 0; idxImage < swapchain.numImages; idxImage++) {
            VkImageViewCreateInfo viewCreateInfo {};
            viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            viewCreateInfo.image = swapchain.images[idxImage];
            viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            viewCreateInfo.format = createInfo.imageFormat;
            viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            viewCreateInfo.subresourceRange.baseMipLevel = 0;
            viewCreateInfo.subresourceRange.levelCount = 1;
            viewCreateInfo.subresourceRange.baseArrayLayer = 0;
            viewCreateInfo.subresourceRange.layerCount = 1;
            vkCreateImageView(device.handle, &viewCreateInfo, nullptr, &swapchain.imageViews[idxImage]);
        }
    }

    return res;
}

static bool create_swapchain_framebuffers(topo_instance_t instance) {
    auto &swapchain = instance->device.currentSwapchain;

    instance->device.arenaSwapchain.push(
        swapchain.numImages * sizeof(VkFramebuffer), (void **)&instance->device.framebuffers);

    Pass pass;
    sm_get_pass(instance->device.shaderManager, "pass_blit_to_swapchain", &pass);

    auto &depthBuffers = instance->device.depthBuffers;

    for (uint32_t idxImage = 0; idxImage < swapchain.numImages; idxImage++) {
        VkImageView ivDepth;
        VkSampler sDepth;
        tm_get_sampler_handle(instance->device.textureManager, depthBuffers[idxImage], &ivDepth, &sDepth);
        VkImageView attachments[] = { swapchain.imageViews[idxImage], ivDepth };

        VkFramebufferCreateInfo framebufferInfo {};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = pass.handle;
        framebufferInfo.attachmentCount = 2;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = instance->extSwap.width;
        framebufferInfo.height = instance->extSwap.height;
        framebufferInfo.layers = 1;

        if (vkCreateFramebuffer(
                instance->device.handle, &framebufferInfo, nullptr, &instance->device.framebuffers[idxImage])
            != VK_SUCCESS) {
            fprintf(stderr, TAG "vkCreateFramebuffer failed\n");
            return false;
        }
    }

    return true;
}

static bool create_command_pool(topo_instance_t instance) {
    auto &dev = instance->device;
    VkCommandPoolCreateInfo poolInfo {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    poolInfo.queueFamilyIndex = instance->physicalDevice.idxQueueGraphics;

    return vkCreateCommandPool(dev.handle, &poolInfo, nullptr, &dev.cmdPool) == VK_SUCCESS;
}

static bool create_command_buffer(topo_instance_t instance) {
    auto &dev = instance->device;
    dev.arenaPersistent.pushElements(NUM_MAX_FRAMES_IN_FLIGHT, &dev.cmdBuffers);
    VkCommandBufferAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = dev.cmdPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = NUM_MAX_FRAMES_IN_FLIGHT;
    return vkAllocateCommandBuffers(dev.handle, &allocInfo, dev.cmdBuffers) == VK_SUCCESS;
}

static void signal_failure(topo_result *res, std::condition_variable &cv, std::mutex &m) {
    *res = kTopo_Failure;
    std::lock_guard<std::mutex> G(m);
    cv.notify_one();
}

static void free_window(topo_instance_t instance) {
    printf(TAG "Destroying window\n");
    SDL_DestroyWindow(instance->pWindow);
}

static void free_vkinstance(topo_instance_t instance) {
    printf(TAG "Destroying surface\n");
    vkDestroySurfaceKHR(instance->pVkInst, instance->surface, nullptr);
    printf(TAG "Destroying Vulkan instance\n");
    vkDestroyInstance(instance->pVkInst, nullptr);
    free_window(instance);
}

static void free_device(topo_instance_t instance) {
    printf(TAG "Destroying logical device\n");
    vkDestroyDevice(instance->device.handle, nullptr);
    free_vkinstance(instance);
}

static void free_buffer_manager(topo_instance_t instance) {
    printf(TAG "Destroying buffer manager\n");
    bm_destroy(instance->device.bufferManager);
    free_device(instance);
}

static void free_texture_manager(topo_instance_t instance) {
    printf(TAG "Destroying texture manager\n");
    tm_destroy(&instance->device.textureManager);
    free_buffer_manager(instance);
}

static void free_swapchain_framebuffers(topo_instance_t instance) {
    auto &swapchain = instance->device.currentSwapchain;

    for (uint32_t i = 0; i < swapchain.numImages; i++) {
        vkDestroyFramebuffer(instance->device.handle, instance->device.framebuffers[i], nullptr);
    }

    free_texture_manager(instance);
}

static void free_swapchain(topo_instance_t instance) {
    printf(TAG "Destroying swapchain image views\n");
    auto &swapchain = instance->device.currentSwapchain;
    for (uint32_t idxView = 0; idxView < swapchain.numImages; idxView++) {
        vkDestroyImageView(instance->device.handle, swapchain.imageViews[idxView], nullptr);
    }
    printf(TAG "Destroying swapchain\n");
    vkDestroySwapchainKHR(instance->device.handle, swapchain.handle, nullptr);
    free_swapchain_framebuffers(instance);
}

static void free_cmd_pool(topo_instance_t instance) {
    printf(TAG "Destroying command pools\n");
    vkDestroyCommandPool(instance->device.handle, instance->device.cmdPool, nullptr);
    free_swapchain(instance);
}

static void free_descriptor_pool(topo_instance_t instance) {
    printf(TAG "Destroying descriptor pool\n");
    for (int i = 0; i < NUM_MAX_FRAMES_IN_FLIGHT; i++) {
        instance->device.descriptorPoolLists[i].destroy(instance->device.handle);
    }
    free_cmd_pool(instance);
}

static void free_sync_objects(topo_instance_t instance) {
    printf(TAG "Destroying sync objects\n");
    auto &dev = instance->device;
    for (uint32_t i = 0; i < NUM_MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(dev.handle, dev.presentSync.imageAcquired[i], nullptr);
        vkDestroySemaphore(dev.handle, dev.presentSync.imageReady[i], nullptr);
        vkDestroyFence(dev.handle, dev.presentSync.frameInFlight[i], nullptr);
    }
    free_descriptor_pool(instance);
}

static void begin_command_buffer(topo_instance_t instance) {
    VkResult res;
    auto &dev = instance->device;

    VkCommandBufferBeginInfo beginInfo {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = 0; // Optional
    beginInfo.pInheritanceInfo = nullptr; // Optional

    auto cmdBuffer = dev.cmdBuffers[dev.currentSwapchain.idxCurrentFrame];
    res = vkBeginCommandBuffer(cmdBuffer, &beginInfo);
    vkTryBeginDebugUtilsLabelTOPO(cmdBuffer, "Render_Main");
    assert(res == VK_SUCCESS);
}

static void end_command_buffer(topo_instance_t instance) {
    auto &dev = instance->device;
    auto cmdBuffer = dev.cmdBuffers[dev.currentSwapchain.idxCurrentFrame];
    vkTryEndDebugUtilsLabelTOPO(cmdBuffer);
    if (vkEndCommandBuffer(cmdBuffer) != VK_SUCCESS) {
        fprintf(stderr, TAG "vkEndCommandBuffer failed\n");
    }
}

static void
record_scene_draws(topo_instance_t instance, arena_t &temp, uint32_t idxImage, RenderTarget_t renderTarget) {
    ZoneScoped;
    VkResult res;
    auto &dev = instance->device;

    auto cmdBuffer = dev.cmdBuffers[dev.currentSwapchain.idxCurrentFrame];
    vkTryBeginDebugUtilsLabelTOPO(cmdBuffer, "Render_Scene");

    Pass passWorld;
    sm_get_pass(dev.shaderManager, "pass_world", &passWorld);

    VkRenderPassBeginInfo renderPassInfo {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = passWorld.handle;
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent = instance->extSwap;

    rt_prepare_for_rendering(renderTarget, dev.currentSwapchain.idxCurrentFrame, cmdBuffer);
    rt_bind(renderTarget, dev.currentSwapchain.idxCurrentFrame, renderPassInfo);

    VkClearValue clearValues[5];
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[1].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[2].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[3].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[4].depthStencil = { 1.0f, 0 };
    renderPassInfo.clearValueCount = 5;
    renderPassInfo.pClearValues = clearValues;

    if (instance->sceneCurrent != nullptr) {
        ZoneScopedN("Render pass");
        MMPass *pass;

        auto *scene = instance->sceneCurrent.load();

        if (scene->pNext != nullptr) {
            auto *scene2 = scene->pNext;
            mm_begin_pass(instance->device.meshManager, temp, scene2->pCameras[scene2->idxMainCamera], &pass);

            for (uint32_t idxGroup = 0; idxGroup < scene2->numObjectGroups; idxGroup++) {
                auto &group = scene2->pObjectGroups[idxGroup];
                for (uint32_t idxObj = 0; idxObj < group.numObjects; idxObj++) {
					auto idxMesh = size_t(group.pMeshes[idxObj]);
					assert(instance->device.meshes.entries[idxMesh].used);
					auto mesh = instance->device.meshes.entries[idxMesh].handle;
					auto &origTransform = group.pTransforms[idxObj];
					Transform transform;
					transform.position
						= glm::vec3(origTransform.position[0], origTransform.position[1], origTransform.position[2]);
					transform.rotation = glm::quat(
						origTransform.rotation[0], origTransform.rotation[1], origTransform.rotation[2],
						origTransform.rotation[3]);
					transform.scale
						= glm::vec3(origTransform.scale[0], origTransform.scale[1], origTransform.scale[2]);
					mm_draw(pass, temp, mesh, transform);
                }
            }

			mm_end_pass(pass, temp, cmdBuffer, renderPassInfo);
        } else {
			auto numObjects = scene->numObjects;
			mm_begin_pass(instance->device.meshManager, temp, scene->mainCamera, &pass);

			for (uint32_t idxObj = 0; idxObj < numObjects; idxObj++) {
				auto idxMesh = size_t(scene->meshes[idxObj]);
				assert(instance->device.meshes.entries[idxMesh].used);
				auto mesh = instance->device.meshes.entries[idxMesh].handle;
				auto &origTransform = scene->transforms[idxObj];
				Transform transform;
				transform.position
					= glm::vec3(origTransform.position[0], origTransform.position[1], origTransform.position[2]);
				transform.rotation = glm::quat(
					origTransform.rotation[0], origTransform.rotation[1], origTransform.rotation[2],
					origTransform.rotation[3]);
				transform.scale
					= glm::vec3(origTransform.scale[0], origTransform.scale[1], origTransform.scale[2]);
				mm_draw(pass, temp, mesh, transform);
			}

			mm_end_pass(pass, temp, cmdBuffer, renderPassInfo);
        }

    } else {
        printf("scene is null\n");
    }

    vkTryEndDebugUtilsLabelTOPO(cmdBuffer);
}

static void record_rendertarget_blit_to_swapchain(
    topo_instance_t instance,
    arena_t &temp,
    uint32_t idxImage,
    RenderTarget_t renderTarget) {
    ZoneScoped;
    VkResult res;
    auto &dev = instance->device;

    auto cmdBuffer = dev.cmdBuffers[dev.currentSwapchain.idxCurrentFrame];
    vkTryBeginDebugUtilsLabelTOPO(cmdBuffer, "Render_Blit");

    Pass passBlit;
    sm_get_pass(dev.shaderManager, "pass_blit_to_swapchain", &passBlit);

    VkRenderPassBeginInfo renderPassInfo {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = passBlit.handle;
    renderPassInfo.framebuffer = instance->device.framebuffers[idxImage];
    renderPassInfo.renderArea.offset = { 0, 0 };
    renderPassInfo.renderArea.extent = instance->extSwap;

    VkClearValue clearValues[2];
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[1].depthStencil = { 1.0f, 0 };
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues = clearValues;

    MMPass *pass;
    rt_prepare_for_display(renderTarget, dev.currentSwapchain.idxCurrentFrame, cmdBuffer);
    mm_begin_pass(dev.meshManager, temp, {}, &pass);
    mm_draw(pass, temp, instance->device.fullscreenQuad.hMesh, {});
    mm_end_pass(pass, temp, cmdBuffer, renderPassInfo);

    vkTryEndDebugUtilsLabelTOPO(cmdBuffer);
}

static bool create_sync_objects(topo_instance_t instance) {
    VkResult res;
    auto &dev = instance->device;
    auto &presentSync = dev.presentSync;

    VkSemaphoreCreateInfo semCreateInfo {};
    semCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceCreateInfo {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    presentSync.arena.pushElements(NUM_MAX_FRAMES_IN_FLIGHT, &presentSync.imageAcquired);
    presentSync.arena.pushElements(NUM_MAX_FRAMES_IN_FLIGHT, &presentSync.imageReady);
    presentSync.arena.pushElements(NUM_MAX_FRAMES_IN_FLIGHT, &presentSync.frameInFlight);

    for (uint32_t i = 0; i < NUM_MAX_FRAMES_IN_FLIGHT; i++) {
        res = vkCreateSemaphore(dev.handle, &semCreateInfo, nullptr, &presentSync.imageAcquired[i]);
        assert(res == VK_SUCCESS);
        if (res != VK_SUCCESS) {
            return false;
        }
        res = vkCreateSemaphore(dev.handle, &semCreateInfo, nullptr, &presentSync.imageReady[i]);
        assert(res == VK_SUCCESS);
        if (res != VK_SUCCESS) {
            return false;
        }

        res = vkCreateFence(dev.handle, &fenceCreateInfo, nullptr, &presentSync.frameInFlight[i]);
        assert(res == VK_SUCCESS);
        if (res != VK_SUCCESS) {
            return false;
        }
    }

    return true;
}

static bool create_depth_buffers(topo_instance_t instance) {
    auto numDepthBuffers = instance->device.currentSwapchain.numImages;
    instance->device.arenaSwapchain.pushElements(numDepthBuffers, &instance->device.depthBuffers);
    for (size_t idxImage = 0; idxImage < numDepthBuffers; idxImage++) {
        char buf[64];
        snprintf(buf, 63, "depth%llu", idxImage);
        buf[63] = '\0';
        SamplerInfo samplerInfo;
        samplerInfo.addressModeU = TextureAddressMode::REPEAT;
        samplerInfo.addressModeV = TextureAddressMode::REPEAT;
        samplerInfo.addressModeW = TextureAddressMode::REPEAT;
        samplerInfo.anisotropy = TextureAnisotropy::OFF;
        samplerInfo.filterMin = TextureFilter::POINT;
        samplerInfo.filterMag = TextureFilter::POINT;

        if (!tm_make_depth_buffer(
                instance->device.textureManager, buf, instance->extSwap.width, instance->extSwap.height,
                samplerInfo, &instance->device.depthBuffers[idxImage])) {
            return false;
        }
    }

    return true;
}

static void recreate_swapchain(topo_instance_t instance) {
    printf(TAG "Destroying swapchain image views\n");
    auto &swapchain = instance->device.currentSwapchain;
    for (uint32_t idxView = 0; idxView < swapchain.numImages; idxView++) {
        vkDestroyImageView(instance->device.handle, swapchain.imageViews[idxView], nullptr);
    }
    printf(TAG "Destroying swapchain\n");
    vkDestroySwapchainKHR(instance->device.handle, swapchain.handle, nullptr);

    printf(TAG "Destroying descriptor pool\n");
    instance->device.descriptorPoolLists->destroy(instance->device.handle);

    auto numDepthBuffers = instance->device.currentSwapchain.numImages;
    for (size_t idxImage = 0; idxImage < numDepthBuffers; idxImage++) {
        tm_free(instance->device.textureManager, instance->device.depthBuffers[idxImage]);
        instance->device.depthBuffers[idxImage] = Texture(-1);
    }

    instance->device.arenaSwapchain.clear();

    if (!query_swapchain_support(instance)) {
        fprintf(stderr, TAG "Couldn't query swapchain support\n");
        abort();
    }

    if (!pick_surface_format(instance) || !pick_present_mode(instance) || !choose_swap_extent(instance)
        || !create_swapchain(instance)) {
        fprintf(stderr, TAG "Couldn't create swapchain\n");
        abort();
    }

    create_depth_buffers(instance);

    if (!create_swapchain_framebuffers(instance)) {
        fprintf(stderr, TAG "Couldn't create swapchain framebuffers\n");
        abort();
    }
}

TOPO_API topo_result topo_create(topo_instance_t *out_instance, const topo_instance_descriptor *descriptor) {
    topo_result ret = kTopo_Failure;
    if (out_instance == nullptr || descriptor == nullptr) {
        return kTopo_Failure;
    }

    std::condition_variable cv;
    std::mutex lock;

    if (sdl_init_counter == 0) {
        SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO);
    }
    sdl_init_counter++;

    topo_instance_t instance = new struct topo_instance;

    if (instance == nullptr) {
        return kTopo_Failure;
    }

    instance->shutdown = false;
    ret = kTopo_Pending;

    instance->rendererThread = std::thread([&, instance]() {
        printf(TAG "Creating window\n");
        instance->pWindow = SDL_CreateWindow(
            descriptor->title, 128, 128, descriptor->window_width, descriptor->window_height,
            SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
        if (instance->pWindow == nullptr) {
            fprintf(stderr, TAG "Couldn't create window\n");
            cv.notify_one();
            return;
        }

        SDL_ShowWindow(instance->pWindow);

        printf(TAG "Creating Vulkan instance\n");
        if (!create_vk_instance(instance, descriptor)) {
            fprintf(stderr, TAG "Couldn't create Vulkan instance\n");
            free_window(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!pick_physical_device(instance, descriptor)) {
            fprintf(stderr, TAG "Couldn't pick a physical device\n");
            free_vkinstance(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!create_logical_device(instance, descriptor)) {
            fprintf(stderr, TAG "Couldn't create a logical device\n");
            free_vkinstance(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!bm_create(&instance->device.bufferManager, instance)) {
            fprintf(stderr, TAG "Couldn't create buffer manager\n");
            free_device(instance);
            signal_failure(&ret, cv, lock);
        }

        if (!tm_create(&instance->device.textureManager, instance)) {
            fprintf(stderr, TAG "Couldn't create texture manager\n");
            bm_destroy(instance->device.bufferManager);
            free_device(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!query_swapchain_support(instance)) {
            fprintf(stderr, TAG "Couldn't query swapchain support\n");
            free_buffer_manager(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!pick_surface_format(instance) || !pick_present_mode(instance) || !choose_swap_extent(instance)
            || !create_swapchain(instance)) {
            fprintf(stderr, TAG "Couldn't create swapchain\n");
            free_buffer_manager(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!create_command_pool(instance)) {
            fprintf(stderr, TAG "Couldn't create cmd pool\n");
            free_swapchain(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!create_command_buffer(instance)) {
            fprintf(stderr, TAG "Couldn't create cmd buffer\n");
            free_cmd_pool(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!sm_create(&instance->device.shaderManager, instance)) {
            fprintf(stderr, TAG "Couldn't create shader manager\n");
            free_descriptor_pool(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!mm_create(instance, &instance->device.meshManager)) {
            fprintf(stderr, TAG "Couldn't create mesh manager\n");
            sm_destroy(&instance->device.shaderManager);
            free_descriptor_pool(instance);
            signal_failure(&ret, cv, lock);
        }

        {
            PassDescriptor passDesc;
            passDesc.kind = PassKind::World;
            passDesc.world.outputFormats[0] = VK_FORMAT_R32G32B32A32_SFLOAT;
            passDesc.world.outputFormats[1] = VK_FORMAT_R32G32B32A32_SFLOAT;
            passDesc.world.outputFormats[2] = VK_FORMAT_R32G32B32A32_SFLOAT;
            passDesc.world.outputFormats[3] = VK_FORMAT_R32G32B32A32_SFLOAT;
            sm_create_pass(instance->device.shaderManager, "pass_world", &passDesc);
        }
        {
            PassDescriptor passDesc;
            passDesc.kind = PassKind::Blit;
            passDesc.blit.outputFormat
                = instance->device.swapchainSupportDetails.formats[instance->idxSurfaceFormat].format;
            passDesc.blit.isTargetSwapchain = true;
            sm_create_pass(instance->device.shaderManager, "pass_blit_to_swapchain", &passDesc);
        }

        const char *names[] = {
            "textured/vert", "textured/frag", "color/vert",        "color/frag",
            "lighting/vert", "lighting/frag", "normalmapped/vert", "normalmapped/frag",
        };
        const char *paths[] = {
            "assets/s_textured.vert",     "assets/s_textured.frag",     "assets/s_color.vert",
            "assets/s_color.frag",        "assets/s_lighting.vert",     "assets/s_lighting.frag",
            "assets/s_normalmapped.vert", "assets/s_normalmapped.frag",
        };
        const char *cachePaths[] = {
            "assets/s_textured.vert.spv", "assets/s_textured.frag.spv", "assets/s_color.vert.spv",
            "assets/s_color.frag.spv",    "assets/s_lighting.vert.spv", "assets/s_lighting.frag.spv",
            "assets/s_normalmapped.vert", "assets/s_normalmapped.frag",
        };
        sm_load_shaders(instance->device.shaderManager, 8, names, cachePaths, paths);

        if (!create_depth_buffers(instance)) {
            fprintf(stderr, TAG "Couldn't create depth buffers\n");
            sm_destroy(&instance->device.shaderManager);
            free_cmd_pool(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!create_swapchain_framebuffers(instance)) {
            fprintf(stderr, TAG "Couldn't create swapchain framebuffers\n");
            sm_destroy(&instance->device.shaderManager);
            free_cmd_pool(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        if (!create_sync_objects(instance)) {
            fprintf(stderr, TAG "Couldn't create sync objects\n");
            sm_destroy(&instance->device.shaderManager);
            free_swapchain(instance);
            signal_failure(&ret, cv, lock);
            return;
        }

        {
            std::lock_guard<std::mutex> G(lock);
            ret = kTopo_OK;
            *out_instance = instance;
            cv.notify_one();
        }

        fmt::print(TAG "Creating rendertarget\n");
        RenderTargetInfo rtInfo;
        rtInfo.compatiblePassId = "pass_world";
        rtInfo.id = "rt0";
        rtInfo.width = instance->extSwap.width;
        rtInfo.height = instance->extSwap.height;
        rtInfo.numColorAttachments = 4;
        rtInfo.numDepthAttachments = 1;
        ColorAttachment colorAttachments[4];
        colorAttachments[0].pixelFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
        colorAttachments[1].pixelFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
        colorAttachments[2].pixelFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
        colorAttachments[3].pixelFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
        rtInfo.pColorAttachments = colorAttachments;
        RenderTarget_t rt;
        rt_create(instance, rtInfo, &rt);

        // Create fullscreen quad
        {
            topo_material_descriptor mat;
            mat.kind = kTopoMaterialKind_RenderTargetReceiver;
            mat.id = "__fullscreen_quad";
            mat.rtReceiver.renderTarget = (topo_rendertarget_t)rt;
            topo_material_t hMat;
            topo_create_material(instance, &mat, &hMat);

            topo_mesh_descriptor meshDescriptor;
            meshDescriptor.id = "__fullscreen_quad";
            const glm::vec2 pos[8] = {
                glm::vec2(-1.0f, -1.0f), glm::vec2(0, 1),

                glm::vec2(-1.0f, +1.0f), glm::vec2(0, 0),

                glm::vec2(+1.0f, +1.0f), glm::vec2(1, 0),

                glm::vec2(+1.0f, -1.0f), glm::vec2(1, 1),
            };
            const uint16_t indices[6] = { 0, 1, 2, 0, 2, 3 };

            topo_vertex_buffer buffers[1];
            buffers[0].pVertexData = pos;
            buffers[0].sizVertexData = sizeof(pos);
            buffers[0].sizVertexStride = 2 * sizeof(glm::vec2);
            meshDescriptor.numVertexBuffers = 1;
            meshDescriptor.pVertexBuffers = buffers;

            topo_vertex_attribute attributes[2];
            attributes[0].idxVertexBuffer = 0;
            attributes[0].offset = 0;
            attributes[0].semantic = kTopoVES_Position;
            attributes[0].type = kTopoVET_F32x2;
            attributes[1].idxVertexBuffer = 0;
            attributes[1].offset = sizeof(glm::vec2);
            attributes[1].semantic = kTopoVES_Texcoord;
            attributes[1].type = kTopoVET_F32x2;
            meshDescriptor.numAttributes = 2;
            meshDescriptor.pAttributes = attributes;

            topo_submesh submeshes[1];
            submeshes[0].indexBuffer.numIndices = 6;
            submeshes[0].indexBuffer.pIndices = indices;
            submeshes[0].indexBuffer.type = topo_index_buffer::UINT16;
            submeshes[0].isIndexed = true;
            submeshes[0].material = hMat;
            submeshes[0].primitiveTopology = kTopoPT_TriangleList;
            meshDescriptor.numSubmeshes = 1;
            meshDescriptor.pSubmeshes = submeshes;

            topo_mesh_handle hFullscreenQuad;
            mm_make(instance->device.meshManager, &meshDescriptor, &hFullscreenQuad);

            instance->device.fullscreenQuad.hMesh = hFullscreenQuad;
            instance->device.fullscreenQuad.hMaterial = hMat;
        }

        printf(TAG "Renderer thread is entering main loop\n");
        auto &presentSync = instance->device.presentSync;
        VkResult res;
        arena_t temp(arena_t::megabytes(8));
        while (!instance->shutdown) {
            ZoneScoped;
            temp.clear();

            if (instance->sceneIncoming != nullptr) {
                const topo_scene *expected = nullptr;
                if (instance->sceneOutgoing.compare_exchange_strong(expected, instance->sceneCurrent)) {
                    instance->sceneCurrent = nullptr;
                    assert(instance->sceneIncoming != nullptr);
                    instance->sceneCurrent.store(instance->sceneIncoming.load());
                    {
                        // Wake up the client thread if it's currently waiting
                        // in topoSetSceneAndWait
                        std::unique_lock G(instance->sceneCvLock);
                        instance->sceneCv.notify_one();
                    }
                    instance->sceneIncoming.store(nullptr);
                    assert(instance->sceneCurrent != nullptr);
                }
            }

			if (!instance->queuedTasks.empty()) {
				uint32_t timeout = 63;
				std::scoped_lock G(instance->queuedTasksLock);
				while (!instance->queuedTasks.empty() && timeout < 64) {
					ZoneScoped;
					auto func = std::move(instance->queuedTasks.front());
					instance->queuedTasks.pop();
					func();
					timeout--;
				}
			}

			mm_free_released_resources(instance->device.meshManager);
			tm_free_released_resources(instance->device.textureManager, instance->arenaFrame);
            bm_free_released_allocations(instance->device.bufferManager);

            SDL_Event ev;
            bool windowResized = false;
            bool windowIsMinimized = false;
            do {
                while (SDL_PollEvent(&ev)) {
                    switch (ev.type) {
                    case SDL_QUIT: {
                        instance->windowExiting = true;
                        break;
                    }
                    case SDL_WINDOWEVENT: {
                        switch (ev.window.type) {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                        case SDL_WINDOWEVENT_RESIZED:
                            windowResized = true;
                            break;
                        case SDL_WINDOWEVENT_MINIMIZED:
                            windowIsMinimized = true;
                            break;
                        case SDL_WINDOWEVENT_RESTORED:
                            windowIsMinimized = false;
                            break;
                        }
                        break;
                    }
                    }
                }
                if (windowIsMinimized) {
                    SDL_WaitEvent(nullptr);
                }
            } while (windowIsMinimized);

            auto &swapchain = instance->device.currentSwapchain;

            vkWaitForFences(
                instance->device.handle, 1, &presentSync.frameInFlight[swapchain.idxCurrentFrame], VK_TRUE,
                UINT64_MAX);
            vkResetFences(instance->device.handle, 1, &presentSync.frameInFlight[swapchain.idxCurrentFrame]);
            instance->device.descriptorPoolLists[swapchain.idxCurrentFrame].reset(instance->device.handle);
            sm_release_uniform_buffers(
                instance->device.shaderManager, instance->device.currentSwapchain.idxCurrentFrame);

            uint32_t idxImage;
            vkAcquireNextImageKHR(
                instance->device.handle, swapchain.handle, UINT64_MAX,
                presentSync.imageAcquired[swapchain.idxCurrentFrame], VK_NULL_HANDLE, &idxImage);
            vkResetCommandBuffer(instance->device.cmdBuffers[swapchain.idxCurrentFrame], 0);

            begin_command_buffer(instance);
            record_scene_draws(instance, temp, idxImage, rt);
            record_rendertarget_blit_to_swapchain(instance, temp, idxImage, rt);
            end_command_buffer(instance);

            VkSubmitInfo submitInfo {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

            VkSemaphore imageAcquireSemaphores[] = { presentSync.imageAcquired[swapchain.idxCurrentFrame] };
            VkSemaphore imageReadySemaphores[] = { presentSync.imageReady[swapchain.idxCurrentFrame] };
            VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = imageAcquireSemaphores;
            submitInfo.pWaitDstStageMask = waitStages;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &instance->device.cmdBuffers[swapchain.idxCurrentFrame];
            submitInfo.signalSemaphoreCount = 1;
            submitInfo.pSignalSemaphores = imageReadySemaphores;
            vkQueueSubmit(
                instance->device.queueGraphics, 1, &submitInfo,
                presentSync.frameInFlight[swapchain.idxCurrentFrame]);

            VkPresentInfoKHR presentInfo {};
            presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
            presentInfo.waitSemaphoreCount = 1;
            presentInfo.pWaitSemaphores = imageReadySemaphores;
            VkSwapchainKHR swapChains[] = { instance->device.currentSwapchain.handle };
            presentInfo.swapchainCount = 1;
            presentInfo.pSwapchains = swapChains;
            presentInfo.pImageIndices = &idxImage;
            presentInfo.pResults = nullptr;

            res = vkQueuePresentKHR(instance->device.queuePresent, &presentInfo);
            FrameMark;

            swapchain.idxPrevFrame = swapchain.idxCurrentFrame;
            swapchain.idxCurrentFrame = (swapchain.idxCurrentFrame + 1) % NUM_MAX_FRAMES_IN_FLIGHT;

            if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR || windowResized) {
                fmt::print(TAG "Swapchain is out-of-date\n");
                vkDeviceWaitIdle(instance->device.handle);
                recreate_swapchain(instance);
                sm_swapchain_resized(instance->device.shaderManager);
                rt_resize(rt, instance->extSwap.width, instance->extSwap.height);
            } else if (res != VK_SUCCESS) {
                abort();
            }
        }

        if (!instance->queuedTasks.empty()) {
            std::scoped_lock G(instance->queuedTasksLock);
            while (!instance->queuedTasks.empty()) {
                auto func = std::move(instance->queuedTasks.front());
                instance->queuedTasks.pop();
                func();
            }
        }

        vkDeviceWaitIdle(instance->device.handle);

        mm_free_released_resources(instance->device.meshManager);
        tm_free_released_resources(instance->device.textureManager, instance->arenaFrame);
        bm_free_released_allocations(instance->device.bufferManager);

        rt_destroy(rt);
        sm_destroy(&instance->device.shaderManager);
        free_sync_objects(instance);
    });

    std::unique_lock<std::mutex> G(lock);
    while (ret == kTopo_Pending) {
        cv.wait(G);
    }

    printf(TAG "Renderer thread signaled ready state\n");

    return ret;
}

TOPO_API topo_result topo_destroy(topo_instance_t *instance) {
    if (instance == nullptr || *instance == nullptr)
        return kTopo_OK;

    printf(TAG "Shutting down renderer thread\n");
    (*instance)->shutdown = true;
    (*instance)->rendererThread.join();

    delete (*instance);
    *instance = nullptr;

    sdl_init_counter--;
    if (sdl_init_counter == 0) {
        SDL_Quit();
    }

    return kTopo_OK;
}

TOPO_API topo_result topo_exiting(topo_instance_t instance, bool *exiting) {
    if (instance == nullptr || exiting == nullptr)
        return kTopo_Failure;
    *exiting = instance->windowExiting;
    return kTopo_OK;
}

TOPO_API topo_result
topo_set_scene(topo_instance_t instance, const topo_scene *nextScene, const topo_scene **prevScene) {
    if (!instance || !nextScene || !prevScene) {
        return kTopo_Failure;
    }

    *prevScene = nullptr;

    if (instance->sceneOutgoing) {
        *prevScene = instance->sceneOutgoing;
        instance->sceneOutgoing = nullptr;
    }

    instance->sceneIncoming = nextScene;

    return kTopo_OK;
}

TOPO_1_1 TopoResult
topoSetSceneAndWait(TopoInstance hInstance, const TopoScene *pNextScene, const TopoScene **ppPrevScene) {
    if (!hInstance || !pNextScene || !ppPrevScene) {
        return Topo_Failure;
    }

    *ppPrevScene = nullptr;

    if (hInstance->sceneOutgoing) {
        *ppPrevScene = hInstance->sceneOutgoing;
        hInstance->sceneOutgoing = nullptr;
    }

    {
        std::unique_lock G(hInstance->sceneCvLock);
        hInstance->sceneIncoming = pNextScene;
        if (hInstance->sceneCurrent != pNextScene) {
            hInstance->sceneCv.wait(G);
        }
    }

    return Topo_OK;
}

TOPO_API topo_result topo_wait_for_frame(topo_instance_t instance) {
    if (!instance) {
        return kTopo_Failure;
    }

    if (instance->sceneCurrent == nullptr) {
        return kTopo_Failure;
    }

    while (instance->sceneOutgoing != nullptr) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    return kTopo_OK;
}

TOPO_1_1 TopoResult topoPollOutcomingScene(TopoInstance hInstance, const TopoScene** ppPrevScene) {
    if (!hInstance || !ppPrevScene) {
        return Topo_Failure;
    }

    *ppPrevScene = nullptr;

    if (hInstance->sceneOutgoing) {
        *ppPrevScene = hInstance->sceneOutgoing;
        hInstance->sceneOutgoing = nullptr;

        return Topo_OK;
    }

    return Topo_EmptyResult;
}
