#include "vk_debug.h"

static VkInstance gCurrentInstance = nullptr;

PFN_vkSetDebugUtilsObjectNameEXT _vkSetDebugUtilsObjectNameEXT = nullptr;
PFN_vkSetDebugUtilsObjectTagEXT _vkSetDebugUtilsObjectTagEXT = nullptr;
PFN_vkCmdBeginDebugUtilsLabelEXT _vkCmdBeginDebugUtilsLabelEXT = nullptr;
PFN_vkCmdEndDebugUtilsLabelEXT _vkCmdEndDebugUtilsLabelEXT = nullptr;

template <typename F> static void linkVkFunction(VkInstance instance, const char *symbol, F &dst) {
    dst = (F)vkGetInstanceProcAddr(instance, symbol);
}

VKAPI_ATTR VkResult VKAPI_CALL vkTryInitDebugUtils(VkInstance instance) {
    if (gCurrentInstance == instance) {
        return VK_SUCCESS;
    }

    linkVkFunction(instance, "vkSetDebugUtilsObjectNameEXT", _vkSetDebugUtilsObjectNameEXT);
    linkVkFunction(instance, "vkSetDebugUtilsObjectTagEXT", _vkSetDebugUtilsObjectTagEXT);
    linkVkFunction(instance, "vkCmdBeginDebugUtilsLabelEXT", _vkCmdBeginDebugUtilsLabelEXT);
    linkVkFunction(instance, "vkCmdEndDebugUtilsLabelEXT", _vkCmdEndDebugUtilsLabelEXT);

    gCurrentInstance = instance;
    return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
vkTrySetDebugUtilsObjectNameTOPO(VkDevice device, const VkDebugUtilsObjectNameInfoEXT *pNameInfo) {
    if (gCurrentInstance == nullptr) {
        return VK_NOT_READY;
    }

    if (_vkSetDebugUtilsObjectNameEXT == nullptr) {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }

    return _vkSetDebugUtilsObjectNameEXT(device, pNameInfo);
}

VKAPI_ATTR VkResult VKAPI_CALL
vkTrySetDebugUtilsObjectTagTOPO(VkDevice device, const VkDebugUtilsObjectTagInfoEXT *pTagInfo) {
    if (gCurrentInstance == nullptr) {
        return VK_NOT_READY;
    }

    if (_vkSetDebugUtilsObjectTagEXT == nullptr) {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }

    return _vkSetDebugUtilsObjectTagEXT(device, pTagInfo);
}

VKAPI_ATTR VkResult VKAPI_CALL
vkTryBeginDebugUtilsLabelTOPO(VkCommandBuffer commandBuffer, const char *label) {
    if (gCurrentInstance == nullptr) {
        return VK_NOT_READY;
    }

    if (_vkCmdBeginDebugUtilsLabelEXT == nullptr) {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }

    VkDebugUtilsLabelEXT labelInfo = {};
    labelInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
    labelInfo.pLabelName = label;
    _vkCmdBeginDebugUtilsLabelEXT(commandBuffer, &labelInfo);

    return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL vkTryEndDebugUtilsLabelTOPO(VkCommandBuffer commandBuffer) {
    if (gCurrentInstance == nullptr) {
        return VK_NOT_READY;
    }

    if (_vkCmdBeginDebugUtilsLabelEXT == nullptr) {
        return VK_ERROR_FEATURE_NOT_PRESENT;
    }

    _vkCmdEndDebugUtilsLabelEXT(commandBuffer);

    return VK_SUCCESS;
}
