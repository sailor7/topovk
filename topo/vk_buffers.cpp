#include "vk_buffers.h"
#include "vk_instance.h"

#include "alloc.h"
#include "arena.h"

#include <fmt/format.h>

#include <tracy/Tracy.hpp>

#define TAG "[topo::buffers] "

struct DeviceMemory {
    VkDeviceMemory handle;
    uint32_t type;
    VkMemoryPropertyFlags flags;
    size_t sizBuffer;
    alloc_t allocator;
};

struct Empty { };

struct ImageDetails {
    uint32_t width, height, depth;
};

template <typename Handle, typename Index, typename Details = Empty> struct MemoryAllocation {
    MemoryAllocation<Handle, Index, Details> *nextUnused;
    Index index;

    Handle handle;
    DeviceMemory *devMem;
    alloc_handle allocHandle;
    /** Offset into the device memory */
    VkDeviceSize offset;
    VkDeviceSize size;

    bool stagingBufferPresent;
    DeviceMemory *stagingBufferMemory;
    alloc_handle stagingBufferAllocation;
    VkBuffer stagingBufferHandle;
    VkFence stagingBufferFence;

    bool stagingBufferCopyPending;
    VkCommandBuffer stagingBufferCopyCmdBuf;

    bool isReleased = false;
    size_t idxLastFrameUsed = 0;

    Details details;
};

using Buffer = MemoryAllocation<VkBuffer, BmVertexBuffer>;
using Image = MemoryAllocation<VkImage, BmImage, ImageDetails>;

template <typename Allocation> struct AllocationList {
    uint32_t length = 0;
    arena_t arenaBuffers { arena_t::kilobytes(512) };
    Allocation *nextUnused = nullptr;
};

using BufferAllocationList = AllocationList<Buffer>;
using ImageAllocationList = AllocationList<Image>;

struct StagingBuffer {
    bool present = false;

    VkBuffer handle;
    DeviceMemory *memory;
    alloc_handle allocHandle;
    VkDeviceSize offset;
    VkDeviceSize size;
    VkFence fence;
};

struct BufferManager {
    arena_t arenaPersistent { arena_t::kilobytes(8) };
    topo_instance *topo = nullptr;

    BufferAllocationList vertexBuffers;
    BufferAllocationList indexBuffers;
    BufferAllocationList uniformBuffers;
    size_t numLiveUniformBuffers = 0;
    ImageAllocationList images;

    StagingBuffer stagingBuffer;

    size_t numDeviceMemories = 0;
    arena_t arenaDeviceMemories { arena_t::kilobytes(8) };

    VkPhysicalDeviceLimits limits;
};

static bool bm_find_memory_type(
    BufferManager_t bm,
    uint32_t *typeFilter,
    VkMemoryPropertyFlags *props,
    uint32_t *idxMemory) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(bm->topo->physicalDevice.handle, &memProperties);

    for (uint32_t idxProp = 0; idxProp < memProperties.memoryTypeCount; idxProp++) {
        if ((*typeFilter & (1 << idxProp))
            && (memProperties.memoryTypes[idxProp].propertyFlags & *props) == *props) {
            *idxMemory = idxProp;
            *typeFilter = (1 << idxProp);
            *props = memProperties.memoryTypes[idxProp].propertyFlags;
            return true;
        }
    }

    return false;
}

bool bm_create(BufferManager_t *out, topo_instance *topo) {
    if (!out || !topo)
        return false;

    VkPhysicalDeviceProperties props = {};
    vkGetPhysicalDeviceProperties(topo->physicalDevice.handle, &props);

    auto *bm = new BufferManager;
    bm->topo = topo;
    bm->limits = props.limits;

    *out = bm;
    return true;
}

static void bm_destroy_bufferlist(BufferManager_t bm, BufferAllocationList &list) {
    auto dev = bm->topo->device.handle;

    auto *buffers = (Buffer *)list.arenaBuffers.buffer;
    for (size_t idxBuf = 0; idxBuf < list.length; idxBuf++) {
        if (buffers[idxBuf].handle != VK_NULL_HANDLE) {
            alloc_free(buffers[idxBuf].devMem->allocator, buffers[idxBuf].allocHandle);
            vkDestroyBuffer(dev, buffers[idxBuf].handle, nullptr);
        }
    }
}

static void bm_destroy_imagelist(BufferManager_t bm, ImageAllocationList &list) {
    auto dev = bm->topo->device.handle;

    auto *images = (Image *)list.arenaBuffers.buffer;
    for (size_t idxBuf = 0; idxBuf < list.length; idxBuf++) {
        if (images[idxBuf].handle != VK_NULL_HANDLE) {
            alloc_free(images[idxBuf].devMem->allocator, images[idxBuf].allocHandle);
            vkDestroyImage(dev, images[idxBuf].handle, nullptr);
        }
    }
}

static void bm_free_staging_buffer(BufferManager_t bm) {
    auto device = bm->topo->device.handle;
    auto &stagingBuffer = bm->stagingBuffer;

    if (stagingBuffer.present) {
        stagingBuffer.present = false;
        fmt::print("{} Freeing staging buffer\n", TAG);

        // TODO: block here once we are doing async copies
        vkDestroyBuffer(device, stagingBuffer.handle, nullptr);
        stagingBuffer.handle = VK_NULL_HANDLE;
        vkDestroyFence(device, stagingBuffer.fence, nullptr);
        stagingBuffer.fence = VK_NULL_HANDLE;
        alloc_free(stagingBuffer.memory->allocator, stagingBuffer.allocHandle);
    }
}

bool bm_destroy(BufferManager_t bm) {
    if (!bm)
        return false;

    auto dev = bm->topo->device.handle;

    bm_destroy_bufferlist(bm, bm->vertexBuffers);
    bm_destroy_bufferlist(bm, bm->indexBuffers);
    bm_destroy_bufferlist(bm, bm->uniformBuffers);
    bm_destroy_imagelist(bm, bm->images);

    if (bm->stagingBuffer.present) {
        bm_free_staging_buffer(bm);
    }

    auto *memories = (DeviceMemory *)bm->arenaDeviceMemories.buffer;
    for (size_t idxMem = 0; idxMem < bm->numDeviceMemories; idxMem++) {
        alloc_destroy(&memories[idxMem].allocator);
        vkFreeMemory(dev, memories[idxMem].handle, nullptr);
    }

    delete bm;
    return true;
}

static bool bm_find_suitable_device_memory(
    BufferManager_t bm,
    VkMemoryRequirements reqs,
    VkMemoryPropertyFlags flags,
    DeviceMemory **mem) {
    ZoneScoped;

    if (reqs.size == 0) {
        reqs.size = 64;
    }

    size_t idxMemory;
    auto *alreadyCreatedMemories = (DeviceMemory *)bm->arenaDeviceMemories.buffer;
    for (idxMemory = 0; idxMemory < bm->numDeviceMemories; idxMemory++) {
        auto &cur = alreadyCreatedMemories[idxMemory];
        bool hasEnoughSpace = false;
        alloc_would_fit(cur.allocator, reqs.size, reqs.alignment, &hasEnoughSpace);
        if ((cur.type & reqs.memoryTypeBits) && (cur.flags & flags) == flags && hasEnoughSpace) {
            break;
        }
    }

    if (idxMemory != bm->numDeviceMemories) {
        *mem = &alreadyCreatedMemories[idxMemory];
        return true;
    }

    DeviceMemory *newMemory;
    bm->arenaDeviceMemories.pushElements(1, &newMemory);
    bm->numDeviceMemories++;

    uint32_t idxDevMemory;
    auto typeBits = reqs.memoryTypeBits;
    if (!bm_find_memory_type(bm, &typeBits, &flags, &idxDevMemory)) {
        abort();
    }

    VkMemoryAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = arena_t::megabytes(64);
    allocInfo.memoryTypeIndex = idxDevMemory;

    auto res = vkAllocateMemory(bm->topo->device.handle, &allocInfo, nullptr, &newMemory->handle);

    if (res != VK_SUCCESS) {
        fmt::print(TAG "[!!!] vkAllocateMemory failure [!!!]\n");
        fmt::print("rc {}, size {}, req {}\n", res, allocInfo.allocationSize, reqs.memoryTypeBits);
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(bm->topo->physicalDevice.handle, &props);

        VkPhysicalDeviceMaintenance3PropertiesKHR props2m3;
        props2m3.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES_KHR;
        props2m3.pNext = nullptr;
        VkPhysicalDeviceProperties2KHR props2;
        props2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2_KHR;
        props2.pNext = &props2m3;

        vkGetPhysicalDeviceProperties2(bm->topo->physicalDevice.handle, &props2);

        fmt::print("Max advertised memory alloc size: {}\n", props2m3.maxMemoryAllocationSize);

        VkPhysicalDeviceMemoryProperties2 memProps2;
        VkPhysicalDeviceMemoryBudgetPropertiesEXT memBudget;
        memProps2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
        memProps2.pNext = &memBudget;
        memBudget.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT;
        memBudget.pNext = nullptr;
        vkGetPhysicalDeviceMemoryProperties2(bm->topo->physicalDevice.handle, &memProps2);
        auto &memProps = memProps2.memoryProperties;
        for (uint32_t i = 0; i < memProps.memoryHeapCount; i++) {
            fmt::print("Heap {} budget {} usage {}\n", i, memBudget.heapBudget[i], memBudget.heapUsage[i]);
        }
        if (bm->numDeviceMemories >= props.limits.maxMemoryAllocationCount) {
            fmt::print("Reached max memory alloc count ({})\n", props.limits.maxMemoryAllocationCount);
        }

        for (uint32_t idxHeap = 0; idxHeap < memProps.memoryHeapCount; idxHeap++) {
            fmt::print(
                "Heap {} flags {} size {}\n", idxHeap, memProps.memoryHeaps[idxHeap].flags,
                memProps.memoryHeaps[idxHeap].size);
        }

        for (uint32_t idxType = 0; idxType < memProps.memoryTypeCount; idxType++) {
            fmt::print(
                "Type {} idx {} prop flags {}\n", idxType, memProps.memoryTypes[idxType].heapIndex,
                memProps.memoryTypes[idxType].propertyFlags);
        }

        auto *memories = (DeviceMemory *)bm->arenaDeviceMemories.buffer;
        for (size_t idxMem = 0; idxMem < bm->numDeviceMemories - 1; idxMem++) {
            size_t sizTotal, sizFree, sizAllocated;
            float fragmentation;
            alloc_get_metrics(memories[idxMem].allocator, &sizTotal, &sizAllocated, &sizFree, &fragmentation);
            fmt::print(
                "Allocator: dev mem {} size {} total {} free {} allocated {} frag {}\n",
                (void *)memories[idxMem].handle, memories[idxMem].sizBuffer, sizTotal, sizFree, sizAllocated,
                fragmentation);
        }

        abort();
    }

    newMemory->sizBuffer = allocInfo.allocationSize;
    alloc_create(&newMemory->allocator, allocInfo.allocationSize);
    newMemory->type = typeBits;
    newMemory->flags = flags;

    auto tag = fmt::format("DMEM({})", (void *)newMemory->handle);
    alloc_tag(newMemory->allocator, tag.c_str());

    *mem = newMemory;
    return true;
}

static bool bm_allocate_from(
    DeviceMemory *devMem,
    const VkMemoryRequirements &reqs,
    uint32_t *offset,
    alloc_handle *handle) {
    ZoneScoped;
    alloc_handle hAlloc;
    if (!alloc_allocate(devMem->allocator, reqs.size, reqs.alignment, &hAlloc)) {
        assert(!"out of memory");
        return false;
    }

    if (offset != nullptr) {
        if (!alloc_get_offset(devMem->allocator, hAlloc, offset)) {
            std::abort();
        }
    }

    *handle = hAlloc;
    return true;
}

template <typename Handle, typename Index, typename Details>
static bool
bm_free_staging_buffer(BufferManager_t bm, MemoryAllocation<Handle, Index, Details> &memAllocation) {
    if (!memAllocation.stagingBufferPresent) {
        return true;
    }

    if (memAllocation.stagingBufferCopyPending) {
        vkWaitForFences(bm->topo->device.handle, 1, &memAllocation.stagingBufferFence, VK_TRUE, UINT64_MAX);
        vkResetFences(bm->topo->device.handle, 1, &memAllocation.stagingBufferFence);
        vkFreeCommandBuffers(
            bm->topo->device.handle, bm->topo->device.cmdPool, 1, &memAllocation.stagingBufferCopyCmdBuf);
        memAllocation.stagingBufferCopyPending = false;
    }

    auto device = bm->topo->device.handle;

    vkDestroyFence(device, memAllocation.stagingBufferFence, nullptr);
    vkDestroyBuffer(device, memAllocation.stagingBufferHandle, nullptr);
    alloc_free(memAllocation.stagingBufferMemory->allocator, memAllocation.stagingBufferAllocation);
    memAllocation.stagingBufferMemory = nullptr;
    memAllocation.stagingBufferAllocation = alloc_handle(-1);
    memAllocation.stagingBufferHandle = VK_NULL_HANDLE;
    memAllocation.stagingBufferPresent = false;

    return true;
}

template <typename Handle, typename Index, typename Details>
static bool
bm_make_staging_buffer(BufferManager_t bm, MemoryAllocation<Handle, Index, Details> &memAllocation) {
    if (memAllocation.stagingBufferPresent) {
        return true;
    }

    auto device = bm->topo->device.handle;

    VkBuffer bufferStaging;
    VkBufferCreateInfo bufferInfo {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = memAllocation.size;
    bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(device, &bufferInfo, nullptr, &bufferStaging) != VK_SUCCESS) {
        fprintf(stderr, TAG "Failed to create VkBuffer for the staging buffer\n");
        return false;
    }

    VkMemoryRequirements memRequirementsStaging;
    uint32_t offStaging = 0;
    vkGetBufferMemoryRequirements(device, bufferStaging, &memRequirementsStaging);

    memRequirementsStaging.size = memAllocation.size;
    bm_find_suitable_device_memory(
        bm, memRequirementsStaging, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, &memAllocation.stagingBufferMemory);
    bm_allocate_from(
        memAllocation.stagingBufferMemory, memRequirementsStaging, &offStaging,
        &memAllocation.stagingBufferAllocation);
    vkBindBufferMemory(device, bufferStaging, memAllocation.stagingBufferMemory->handle, offStaging);
    memAllocation.stagingBufferHandle = bufferStaging;

    VkFenceCreateInfo fenceCreateInfo {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    if (vkCreateFence(device, &fenceCreateInfo, nullptr, &memAllocation.stagingBufferFence) != VK_SUCCESS) {
        fprintf(stderr, TAG "Failed to create the fence for the staging buffer\n");
        return false;
    }

    memAllocation.stagingBufferPresent = true;
    memAllocation.stagingBufferCopyPending = false;

    return true;
}

static bool bm_make_generic_buffer(
    BufferManager_t bm,
    BufferAllocationList &bufferList,
    size_t size,
    VkBufferUsageFlags usageFlags,
    VkMemoryPropertyFlags memoryPropertyFlags,
    Buffer **buffer) {
    ZoneScoped;

    if (bufferList.nextUnused == nullptr) {
        ZoneScoped;
        bufferList.arenaBuffers.pushElements(1, &bufferList.nextUnused);
        bufferList.nextUnused->nextUnused = nullptr;
        bufferList.nextUnused->handle = VK_NULL_HANDLE;
        bufferList.nextUnused->index = bufferList.length;
        bufferList.length++;
    }

    if (size == 0) {
        size = 64;
    }

    auto *slot = bufferList.nextUnused;
    assert(slot != nullptr);
    slot->handle = nullptr;
    slot->devMem = nullptr;
    slot->offset = VkDeviceSize(-1);
    slot->stagingBufferPresent = false;

    bufferList.nextUnused = slot->nextUnused;
    VkBufferCreateInfo bufferInfo {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usageFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer bufferDevice;

    if (vkCreateBuffer(bm->topo->device.handle, &bufferInfo, nullptr, &bufferDevice) != VK_SUCCESS) {
        assert(0);
        return false;
    }

    slot->handle = bufferDevice;

    DeviceMemory *memDevice;

    VkMemoryRequirements memRequirements;
    uint32_t offDevice = 0;
    vkGetBufferMemoryRequirements(bm->topo->device.handle, bufferDevice, &memRequirements);
    bm_find_suitable_device_memory(bm, memRequirements, memoryPropertyFlags, &memDevice);
    bm_allocate_from(memDevice, memRequirements, &offDevice, &slot->allocHandle);
    vkBindBufferMemory(bm->topo->device.handle, bufferDevice, memDevice->handle, offDevice);
    slot->devMem = memDevice;
    slot->offset = offDevice;
    slot->size = size;

    *buffer = slot;
    return true;
}

/**
 * Creates an image and allocates memory for it.
 */
static bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *imageInfo,
    ImageAllocationList &bufferList,
    size_t size,
    VkBufferUsageFlags usageFlags,
    Image **buffer) {

    if (bufferList.nextUnused == nullptr) {
        bufferList.arenaBuffers.pushElements(1, &bufferList.nextUnused);
        bufferList.nextUnused->nextUnused = nullptr;
        bufferList.nextUnused->handle = VK_NULL_HANDLE;
        bufferList.nextUnused->index = bufferList.length;
        bufferList.length++;
    }

    auto *slot = bufferList.nextUnused;
    assert(slot != nullptr);
    slot->handle = nullptr;
    bufferList.nextUnused = slot->nextUnused;

    VkImageCreateInfo createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent.width = imageInfo->width;
    createInfo.extent.height = imageInfo->height;
    createInfo.extent.depth = imageInfo->depth;
    createInfo.mipLevels = 1;
    createInfo.arrayLayers = 1;
    createInfo.format = imageInfo->format;
    createInfo.tiling = imageInfo->tiling;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.usage = usageFlags;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkImage imageDevice;

    VkResult res;
    res = vkCreateImage(bm->topo->device.handle, &createInfo, nullptr, &imageDevice);
    if (res != VK_SUCCESS) {
        return false;
    }

    slot->handle = imageDevice;

    DeviceMemory *memDevice;

    VkMemoryRequirements memRequirements;
    uint32_t offDevice = 0;
    vkGetImageMemoryRequirements(bm->topo->device.handle, imageDevice, &memRequirements);
    bm_find_suitable_device_memory(bm, memRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &memDevice);
    bm_allocate_from(memDevice, memRequirements, &offDevice, &slot->allocHandle);
    vkBindImageMemory(bm->topo->device.handle, imageDevice, memDevice->handle, offDevice);
    slot->devMem = memDevice;
    slot->offset = offDevice;
    slot->size = size;
    slot->details.width = imageInfo->width;
    slot->details.height = imageInfo->height;
    slot->details.depth = imageInfo->depth;

    *buffer = slot;
    return true;
}

static void
doLayoutTransition(VkCommandBuffer &cmdBuf, const Image &image, VkImageLayout prev, VkImageLayout next) {
    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    VkImageMemoryBarrier barrier {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = prev;
    barrier.newLayout = next;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.handle;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    if (prev == VK_IMAGE_LAYOUT_UNDEFINED && next == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (
        prev == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && next == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        std::abort();
    }

    vkCmdPipelineBarrier(cmdBuf, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
}

static bool bm_copy(
    BufferManager_t bm,
    Image &image,
    uint32_t mipLevel,
    size_t size,
    const void *data,
    VkDeviceSize offset) {
    (void)offset;

    if (!bm_make_staging_buffer(bm, image)) {
        return false;
    }

    assert(image.stagingBufferPresent);

    // Map the staging area into host address space
    void *mapped;
    uint32_t stagingOffset = 0xFFFFFFFF;
    alloc_get_offset(image.stagingBufferMemory->allocator, image.stagingBufferAllocation, &stagingOffset);
    vkMapMemory(bm->topo->device.handle, image.stagingBufferMemory->handle, stagingOffset, size, 0, &mapped);
    memcpy(mapped, data, size);
    vkUnmapMemory(bm->topo->device.handle, image.stagingBufferMemory->handle);

    // Enqueue transfer from staging area to final location

    VkCommandBuffer cmdBuf;
    VkCommandBufferAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = bm->topo->device.cmdPool;
    allocInfo.commandBufferCount = 1;
    vkAllocateCommandBuffers(bm->topo->device.handle, &allocInfo, &cmdBuf);

    VkCommandBufferBeginInfo beginInfo {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(cmdBuf, &beginInfo);
    doLayoutTransition(cmdBuf, image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    VkBufferImageCopy region {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = mipLevel;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = { 0, 0, 0 };
    region.imageExtent.width = image.details.width;
    region.imageExtent.height = image.details.height;
    region.imageExtent.depth = image.details.depth;
    vkCmdCopyBufferToImage(
        cmdBuf, image.stagingBufferHandle, image.handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    doLayoutTransition(
        cmdBuf, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    vkEndCommandBuffer(cmdBuf);

    VkSubmitInfo submitInfo {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuf;

    vkQueueSubmit(bm->topo->device.queueGraphics, 1, &submitInfo, image.stagingBufferFence);
    vkWaitForFences(bm->topo->device.handle, 1, &image.stagingBufferFence, VK_TRUE, UINT64_MAX);
    vkResetFences(bm->topo->device.handle, 1, &image.stagingBufferFence);
    vkFreeCommandBuffers(bm->topo->device.handle, bm->topo->device.cmdPool, 1, &cmdBuf);
    bm_free_staging_buffer(bm, image);
    return true;
}

static bool bm_copy_async(
    BufferManager_t bm,
    VkCommandBuffer cmdBuf,
    Buffer &buffer,
    size_t size,
    const void *data,
    VkDeviceSize offset) {
    if (!bm_make_staging_buffer(bm, buffer)) {
        return false;
    }

    assert(buffer.stagingBufferPresent);

    auto dev = bm->topo->device.handle;

    // Map the staging area into host address space
    void *mapped;
    uint32_t stagingOffset = 0xFFFFFFFF;
    alloc_get_offset(buffer.stagingBufferMemory->allocator, buffer.stagingBufferAllocation, &stagingOffset);
    vkMapMemory(dev, buffer.stagingBufferMemory->handle, stagingOffset, size, 0, &mapped);
    memcpy(mapped, data, size);
    vkUnmapMemory(dev, buffer.stagingBufferMemory->handle);

    // Enqueue transfer from staging area to final location

    VkBufferCopy copyRegion {};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = offset;
    copyRegion.size = size;
    vkCmdCopyBuffer(cmdBuf, buffer.stagingBufferHandle, buffer.handle, 1, &copyRegion);

    buffer.stagingBufferCopyCmdBuf = VK_NULL_HANDLE;
    return true;
}

static bool
bm_copy_async(BufferManager_t bm, Buffer &buffer, size_t size, const void *data, VkDeviceSize offset) {
    if (!bm_make_staging_buffer(bm, buffer)) {
        return false;
    }

    assert(buffer.stagingBufferPresent);

    auto dev = bm->topo->device.handle;

    if (buffer.stagingBufferCopyPending) {
        vkWaitForFences(dev, 1, &buffer.stagingBufferFence, VK_TRUE, UINT64_MAX);
        vkResetFences(dev, 1, &buffer.stagingBufferFence);
        vkFreeCommandBuffers(dev, bm->topo->device.cmdPool, 1, &buffer.stagingBufferCopyCmdBuf);
        buffer.stagingBufferCopyPending = false;
    }

    // Enqueue transfer from staging area to final location

    VkCommandBuffer cmdBuf;
    VkCommandBufferAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = bm->topo->device.cmdPool;
    allocInfo.commandBufferCount = 1;
    vkAllocateCommandBuffers(dev, &allocInfo, &cmdBuf);

    bool res = true;
    VkCommandBufferBeginInfo beginInfo {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(cmdBuf, &beginInfo);
    res = bm_copy_async(bm, cmdBuf, buffer, size, data, offset);
    vkEndCommandBuffer(cmdBuf);

    VkSubmitInfo submitInfo {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuf;
    vkQueueSubmit(bm->topo->device.queueGraphics, 1, &submitInfo, buffer.stagingBufferFence);

    buffer.stagingBufferCopyCmdBuf = cmdBuf;
    buffer.stagingBufferCopyPending = true;
    return res;
}

static bool bm_copy(BufferManager_t bm, Buffer &buffer, size_t size, const void *data, VkDeviceSize offset) {
    if (!bm_make_staging_buffer(bm, buffer)) {
        return false;
    }

    assert(buffer.stagingBufferPresent);

    // Map the staging area into host address space
    void *mapped;
    uint32_t stagingOffset = 0xFFFFFFFF;
    alloc_get_offset(buffer.stagingBufferMemory->allocator, buffer.stagingBufferAllocation, &stagingOffset);
    vkMapMemory(bm->topo->device.handle, buffer.stagingBufferMemory->handle, stagingOffset, size, 0, &mapped);
    memcpy(mapped, data, size);
    vkUnmapMemory(bm->topo->device.handle, buffer.stagingBufferMemory->handle);

    // Enqueue transfer from staging area to final location

    VkCommandBuffer cmdBuf;
    VkCommandBufferAllocateInfo allocInfo {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = bm->topo->device.cmdPool;
    allocInfo.commandBufferCount = 1;
    vkAllocateCommandBuffers(bm->topo->device.handle, &allocInfo, &cmdBuf);

    VkCommandBufferBeginInfo beginInfo {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(cmdBuf, &beginInfo);
    VkBufferCopy copyRegion {};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = offset;
    copyRegion.size = size;
    vkCmdCopyBuffer(cmdBuf, buffer.stagingBufferHandle, buffer.handle, 1, &copyRegion);
    vkEndCommandBuffer(cmdBuf);

    VkSubmitInfo submitInfo {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuf;

    vkQueueSubmit(bm->topo->device.queueGraphics, 1, &submitInfo, buffer.stagingBufferFence);
    vkWaitForFences(bm->topo->device.handle, 1, &buffer.stagingBufferFence, VK_TRUE, UINT64_MAX);
    vkResetFences(bm->topo->device.handle, 1, &buffer.stagingBufferFence);
    vkFreeCommandBuffers(bm->topo->device.handle, bm->topo->device.cmdPool, 1, &cmdBuf);
    return true;
}

static bool bm_make_generic_buffer(
    BufferManager_t bm,
    BufferAllocationList &bufferList,
    size_t size,
    const void *data,
    VkBufferUsageFlags usageFlags,
    Buffer **buffer) {
    // Create an uninitialized buffer
    auto memoryProperty = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    if (!bm_make_generic_buffer(bm, bufferList, size, usageFlags, memoryProperty, buffer)) {
        return false;
    }

    if (!bm_copy(bm, **buffer, size, data, 0)) {
        return false;
    }

    return true;
}

static bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *imageInfo,
    ImageAllocationList &bufferList,
    size_t size,
    const void *data,
    VkBufferUsageFlags usageFlags,
    Image **buffer) {
    // Create an uninitialized buffer
    if (!bm_make_image(bm, imageInfo, bufferList, size, usageFlags, buffer)) {
        return false;
    }

    if (!bm_copy(bm, **buffer, 0, size, data, 0)) {
        return false;
    }

    return true;
}


static bool bm_release_generic_buffer(BufferManager_t bm, BufferAllocationList &bufferList, BmVertexBuffer handle) {
    auto *vertexBuffers = (Buffer *)bufferList.arenaBuffers.buffer;
    auto &buffer = vertexBuffers[handle];
    if (buffer.handle == VK_NULL_HANDLE) {
        fmt::print(
            stderr, TAG "Warning: tried to release freed buffer #{} (list {})\n", handle,
            (void *)&bufferList);
        return true;
    }

    buffer.isReleased = true;
    return true;
}

static bool
bm_free_generic_buffer(BufferManager_t bm, BufferAllocationList &bufferList, BmVertexBuffer handle) {
    auto *vertexBuffers = (Buffer *)bufferList.arenaBuffers.buffer;
    auto &buffer = vertexBuffers[handle];
    if (buffer.handle == VK_NULL_HANDLE) {
        fmt::print(stderr, TAG "Warning: double-free of buffer #{} (list {})\n", handle, (void *)&bufferList);
        return true;
    }

    if (buffer.stagingBufferPresent) {
        bm_free_staging_buffer(bm, buffer);
    }

    vkDestroyBuffer(bm->topo->device.handle, buffer.handle, nullptr);
    alloc_free(buffer.devMem->allocator, buffer.allocHandle);
    buffer.handle = VK_NULL_HANDLE;
    buffer.isReleased = false;
    buffer.nextUnused = bufferList.nextUnused;
    bufferList.nextUnused = &buffer;

    return true;
}

static bool bm_get_generic_buffer_handle(
    BufferManager_t bm,
    const BufferAllocationList &bufferList,
    BmVertexBuffer idx,
    VkBuffer *handle) {
    if (!handle)
        return false;
    *handle = VK_NULL_HANDLE;
    if (!bm || idx >= bufferList.length)
        return false;
    auto *buffers = (Buffer *)bufferList.arenaBuffers.buffer;
    auto &buffer = buffers[idx];
    assert(buffer.handle != VK_NULL_HANDLE);
    assert(buffer.devMem != nullptr);
    assert(buffer.index == idx);
    *handle = buffer.handle;
    buffer.idxLastFrameUsed = bm->topo->device.currentSwapchain.idxCurrentFrame;
    return true;
}

bool bm_make_vertex_buffer(
    BufferManager_t bm,
    size_t sizBuffer,
    const void *data,
    BmVertexBuffer *out_handle) {
    Buffer *buffer = nullptr;

    if (!bm_make_generic_buffer(
            bm, bm->vertexBuffers, sizBuffer, data, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, &buffer)) {
        return false;
    }

    bm_free_staging_buffer(bm, *buffer);

    *out_handle = buffer->index;
    return true;
}

bool bm_free_vertex_buffer(BufferManager_t bm, BmVertexBuffer handle) {
    return bm_free_generic_buffer(bm, bm->vertexBuffers, handle);
}

bool bm_release_vertex_buffer(BufferManager_t bm, BmVertexBuffer handle) {
    return bm_release_generic_buffer(bm, bm->vertexBuffers, handle);
}

bool bm_make_index_buffer(BufferManager_t bm, size_t sizBuffer, const void *data, BmIndexBuffer *out_handle) {
    Buffer *buffer = nullptr;

    if (!bm_make_generic_buffer(
            bm, bm->indexBuffers, sizBuffer, data, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, &buffer)) {
        return false;
    }

    bm_free_staging_buffer(bm, *buffer);

    *out_handle = buffer->index;
    return true;
}

bool bm_release_index_buffer(BufferManager_t bm, BmIndexBuffer handle) {
    return bm_release_generic_buffer(bm, bm->indexBuffers, handle);
}

bool bm_free_index_buffer(BufferManager_t bm, BmIndexBuffer handle) {
    return bm_free_generic_buffer(bm, bm->indexBuffers, handle);
}

bool bm_get_vertex_buffer_handle(BufferManager_t bm, BmVertexBuffer idx, VkBuffer *handle) {
    ZoneScoped;
    return bm_get_generic_buffer_handle(bm, bm->vertexBuffers, idx, handle);
}

bool bm_get_index_buffer_handle(BufferManager_t bm, BmIndexBuffer idx, VkBuffer *handle) {
    return bm_get_generic_buffer_handle(bm, bm->indexBuffers, idx, handle);
}

bool bm_make_uniform_buffer(
    BufferManager_t bm,
    size_t sizElement,
    size_t numElements,
    VkBufferUsageFlags usage,
    BmUniformBuffer *out_handle,
    uint32_t *out_stride) {
    ZoneScoped;
    Buffer *buffer = nullptr;

    auto alignment = bm->limits.minUniformBufferOffsetAlignment;
    auto mask = alignment - 1;

    auto sizElementPadded = (sizElement + mask) & (~mask);
    auto sizBuffer = numElements * sizElementPadded;

    if (!bm_make_generic_buffer(
            bm, bm->uniformBuffers, sizBuffer, usage,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &buffer)) {
        assert(0);
        return false;
    }

    bm->numLiveUniformBuffers += 1;
    *out_handle = buffer->index;
    assert(sizElementPadded <= 0xFFFFFFFF);
    *out_stride = (uint32_t)sizElementPadded;
    return true;
}

bool bm_map_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer, void **out_addr, size_t *out_size) {
    auto &bufferAlloc = ((Buffer *)bm->uniformBuffers.arenaBuffers.buffer)[buffer];
    assert(bufferAlloc.handle != VK_NULL_HANDLE);
    assert(bufferAlloc.devMem != nullptr);
    assert(bufferAlloc.devMem->handle != VK_NULL_HANDLE);
    void *dData = nullptr;
    if (vkMapMemory(
            bm->topo->device.handle, bufferAlloc.devMem->handle, bufferAlloc.offset, bufferAlloc.size, 0,
            &dData)
        != VK_SUCCESS) {
        return false;
    }

    *out_addr = dData;
    *out_size = bufferAlloc.size;

    return true;
}

bool bm_flush_and_unmap_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer) {
    auto &bufferAlloc = ((Buffer *)bm->uniformBuffers.arenaBuffers.buffer)[buffer];
    assert(bufferAlloc.handle != VK_NULL_HANDLE);
    assert(bufferAlloc.devMem != nullptr);
    assert(bufferAlloc.devMem->handle != VK_NULL_HANDLE);
    VkMappedMemoryRange ranges[1];
    ranges[0].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    ranges[0].pNext = nullptr;
    ranges[0].memory = bufferAlloc.devMem->handle;
    ranges[0].size = bufferAlloc.size;
    ranges[0].offset = bufferAlloc.offset;
    vkFlushMappedMemoryRanges(bm->topo->device.handle, 1, ranges);
    vkUnmapMemory(bm->topo->device.handle, bufferAlloc.devMem->handle);
    return true;
}

bool bm_free_uniform_buffer(BufferManager_t bm, BmUniformBuffer handle) {
    auto res = bm_free_generic_buffer(bm, bm->uniformBuffers, handle);

    if (res) {
        bm->numLiveUniformBuffers -= 1;
    }

    return res;
}

bool bm_get_uniform_buffer_handle(BufferManager_t bm, BmUniformBuffer buffer, VkBuffer *handle) {
    ZoneScoped;
    return bm_get_generic_buffer_handle(bm, bm->uniformBuffers, buffer, handle);
}

bool bm_update_uniform_buffer(
    BufferManager_t bm,
    BmUniformBuffer buffer,
    size_t sizData,
    const void *data,
    VkDeviceSize offset) {
    if (bm->uniformBuffers.length <= buffer) {
        return false;
    }

    auto &bufferAlloc = ((Buffer *)bm->uniformBuffers.arenaBuffers.buffer)[buffer];

    if (!bm_copy_async(bm, bufferAlloc, sizData, data, offset)) {
        return false;
    }

    return true;
}

bool bm_update_uniform_buffer(
    BufferManager_t bm,
    BmUniformBuffer buffer,
    VkCommandBuffer cmdBuf,
    size_t sizData,
    const void *data,
    VkDeviceSize offset) {
    if (bm->uniformBuffers.length <= buffer) {
        return false;
    }

    auto &bufferAlloc = ((Buffer *)bm->uniformBuffers.arenaBuffers.buffer)[buffer];

    if (!bm_copy_async(bm, cmdBuf, bufferAlloc, sizData, data, offset)) {
        return false;
    }

    return true;
}

bool bm_update_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer, size_t sizData, const void *data) {
    auto &bufferAlloc = ((Buffer *)bm->uniformBuffers.arenaBuffers.buffer)[buffer];
    assert(bufferAlloc.handle != VK_NULL_HANDLE);
    assert(bufferAlloc.devMem != nullptr);
    assert(bufferAlloc.devMem->handle != VK_NULL_HANDLE);
    void *dData = nullptr;
    vkMapMemory(
        bm->topo->device.handle, bufferAlloc.devMem->handle, bufferAlloc.offset, bufferAlloc.size, 0, &dData);
    auto sizCopy = sizData < bufferAlloc.size ? sizData : bufferAlloc.size;
    memcpy(dData, data, sizCopy);
    vkUnmapMemory(bm->topo->device.handle, bufferAlloc.devMem->handle);
    return true;
}

bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *info,
    size_t sizBuffer,
    const void *data,
    VkBufferUsageFlags usage,
    BmImage *out_handle) {
    Image *image;
    if (!bm_make_image(bm, info, bm->images, sizBuffer, data, usage, &image)) {
        return false;
    }

    bm_free_staging_buffer(bm, *image);

    *out_handle = image->index;
    return true;
}

bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *info,
    size_t sizBuffer,
    VkBufferUsageFlags usage,
    BmImage *out_handle) {
    Image *image;
    if (!bm_make_image(bm, info, bm->images, sizBuffer, usage, &image)) {
        return false;
    }

    bm_free_staging_buffer(bm, *image);

    *out_handle = image->index;
    return true;
}

bool bm_release_image(BufferManager_t bm, BmImage handle) {
    auto *images = (Image *)bm->images.arenaBuffers.buffer;
    auto &image = images[handle];
    if (image.handle == VK_NULL_HANDLE) {
        fmt::print(TAG "Warning: tried to release freed image #{}\n", handle);
        return true;
    }

    image.isReleased = true;

    return true;
}

bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *info,
    size_t sizBuffer,
    const void *data,
    BmImage *out_handle) {
    auto res = bm_make_image(
        bm, info, sizBuffer, data, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, out_handle);
    auto *images = (Image *)bm->images.arenaBuffers.buffer;
    bm_free_staging_buffer(bm, images[*out_handle]);
    return res;
}

bool bm_make_image(BufferManager_t bm, const ImageInfo *info, size_t sizBuffer, BmImage *out_handle) {
    return bm_make_image(bm, info, sizBuffer, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, out_handle);
}

bool bm_free_image(BufferManager_t bm, BmImage handle) {
    auto *images = (Image *)bm->images.arenaBuffers.buffer;
    auto &image = images[handle];
    if (image.handle == VK_NULL_HANDLE) {
        fprintf(stderr, TAG "Warning: double-free of image #%u\n", handle);
        return true;
    }

    if (image.stagingBufferPresent) {
        bm_free_staging_buffer(bm, image);
    }

    vkDestroyImage(bm->topo->device.handle, image.handle, nullptr);
    alloc_free(image.devMem->allocator, image.allocHandle);
    image.handle = VK_NULL_HANDLE;
    image.isReleased = false;
    image.nextUnused = bm->images.nextUnused;
    bm->images.nextUnused = &image;

    return true;
}

bool bm_get_image_handle(BufferManager_t bm, BmImage idx, VkImage *handle) {
    if (!handle)
        return false;
    *handle = VK_NULL_HANDLE;
    if (!bm || idx >= bm->images.length)
        return false;
    auto *buffers = (Image *)bm->images.arenaBuffers.buffer;
    auto &buffer = buffers[idx];
    *handle = buffer.handle;
    buffer.idxLastFrameUsed = bm->topo->device.currentSwapchain.idxCurrentFrame;
    return true;
}

bool bm_free_released_allocations(BufferManager_t bm) {
    auto idxCurrentFrame = bm->topo->device.currentSwapchain.idxCurrentFrame;
    auto isShuttingDown = bm->topo->shutdown;

    {
        auto *images = (Image *)bm->images.arenaBuffers.buffer;
        auto idxImage = 0;
        auto numImages = bm->images.length;

        while (idxImage < numImages) {
            auto *image = &images[idxImage];
            if (image->handle == VK_NULL_HANDLE) {
                idxImage++;
                continue;
            }

            if (image->isReleased && (image->idxLastFrameUsed == idxCurrentFrame || isShuttingDown)) {
                bm_free_image(bm, idxImage);
                assert(image->handle == VK_NULL_HANDLE);
            }

            idxImage++;
        }
    }

    {
        auto *buffers = (Buffer *)bm->vertexBuffers.arenaBuffers.buffer;
        auto numBuffers = bm->vertexBuffers.length;
        BmVertexBuffer idxBuffer = 0;

        while (idxBuffer < numBuffers) {
            auto *buffer = &buffers[idxBuffer];

            if (buffer->handle == VK_NULL_HANDLE) {
                idxBuffer++;
                continue;
            }

            if (buffer->isReleased && (buffer->idxLastFrameUsed == idxCurrentFrame || isShuttingDown)) {
                bm_free_vertex_buffer(bm, idxBuffer);
                assert(buffer->handle == VK_NULL_HANDLE);
            }

            idxBuffer++;
        }
    }

    {
        auto *buffers = (Buffer *)bm->indexBuffers.arenaBuffers.buffer;
        auto numBuffers = bm->indexBuffers.length;
        BmVertexBuffer idxBuffer = 0;

        while (idxBuffer < numBuffers) {
            auto *buffer = &buffers[idxBuffer];

            if (buffer->handle == VK_NULL_HANDLE) {
                idxBuffer++;
                continue;
            }

            if (buffer->isReleased && (buffer->idxLastFrameUsed == idxCurrentFrame || isShuttingDown)) {
                bm_free_index_buffer(bm, idxBuffer);
                assert(buffer->handle == VK_NULL_HANDLE);
            }

            idxBuffer++;
        }
    }

    return true;
}
