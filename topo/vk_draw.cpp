#include <topo/topo.h>

#include "vk_buffers.h"
#include "vk_draw.h"
#include "vk_instance.h"

#include <array>

#include <tracy/Tracy.hpp>

static std::array<const MaterialDrawHandlerOps *, kTopoMaterialKind_Count> gDrawHandlers = {};
static std::array<void *, kTopoMaterialKind_Count> gDrawHandlerUserPtrs = {};

static void convert(glm::mat4 &out, const Transform &in) {
    auto matScale = glm::scale(glm::mat4(1.0f), in.scale);
    auto matRotate = glm::mat4_cast(in.rotation);
    auto matTranslate = glm::translate(glm::mat4(1.0f), in.position);
    out = matTranslate * matRotate * matScale;
}

static void convert(glm::mat4 &out, const topo_transform &in) {
    Transform t2;
    t2.position = glm::vec3(in.position[0], in.position[1], in.position[2]);
    t2.rotation = glm::quat(in.rotation[0], in.rotation[1], in.rotation[2], in.rotation[3]);
    t2.scale = glm::vec3(in.scale[0], in.scale[1], in.scale[2]);
    convert(out, t2);
}

static void convert(glm::mat4 &out, const topo_camera &in) {
    auto t = in.transform;
    Transform t2;
    t2.position = glm::vec3(t.position[0], t.position[1], t.position[2]);
    t2.rotation = glm::quat(t.rotation[0], t.rotation[1], t.rotation[2], t.rotation[3]);
    t2.scale = glm::vec3(t.scale[0], t.scale[1], t.scale[2]);
    convert(out, t2);
}

bool RegisterMaterialDrawHandler(
    topo_material_kind materialKind,
    const MaterialDrawHandlerOps* ops,
    void* user) {
    if (materialKind >= kTopoMaterialKind_Count || ops == nullptr || gDrawHandlers[materialKind] != nullptr) {
        assert(0);
        return false;
    }

    gDrawHandlers[materialKind] = ops;
    gDrawHandlerUserPtrs[materialKind] = user;

    return true;
}

bool InsertIntoDrawPass(
    DrawPass *pass,
    arena_t &temp,
    topo_mesh_handle mesh,
    const Transform &transform) {
    ZoneScoped;
    auto *pMesh = (Mesh *)mesh;

    assert(!pMesh->released);

    auto &dev = pass->instance->device;
    auto *sm = dev.shaderManager;
    auto *bm = dev.bufferManager;
    auto *tm = dev.textureManager;
    auto idxCurrentFrame = dev.currentSwapchain.idxCurrentFrame;

    pMesh->idxLastFrameUsed = idxCurrentFrame;

    VkResult res;

    for (uint32_t idxSubmesh = 0; idxSubmesh < pMesh->numSubMeshes; idxSubmesh++) {
        auto &submesh = pMesh->subMeshes[idxSubmesh];
        auto hMaterial = submesh.hMaterial;
        auto &material = dev.materials.entries[uint64_t(submesh.hMaterial)];

        DrawBatch *batch = nullptr;
        uint32_t sizDrawUniforms = 0;

        if (!gDrawHandlers[material.kind]->GetDrawUniformSize(
            gDrawHandlerUserPtrs[material.kind], &sizDrawUniforms)) {
            assert(!"Failed to get draw uniform size");
        }

        batch = pass->batches[material.kind];
        DrawBatch *next = nullptr;
        if (batch && batch->numDraws == DRAW_BATCH_SIZE) {
            next = batch;
            batch = nullptr;
        }

        if (batch == nullptr) {
            temp.pushElements(1, &batch);
            batch->next = next;
            batch->numDraws = 0;
            temp.pushElements(DRAW_BATCH_SIZE, &batch->meshes);
            temp.pushElements(DRAW_BATCH_SIZE, &batch->submeshIndices);
            temp.pushElements(DRAW_BATCH_SIZE, &batch->transforms);
            sm_get_draw_uniform_buffer(
                sm, idxCurrentFrame, submesh.namePipeline, sizDrawUniforms, DRAW_BATCH_SIZE,
                &batch->perDrawUniforms, &batch->perDrawUniformsStride);
            assert(batch->numDraws <= DRAW_BATCH_SIZE);

            pass->batches[material.kind] = batch;
        }

        if (batch != nullptr) {
            batch->meshes[batch->numDraws] = pMesh;
            batch->transforms[batch->numDraws] = transform;
            batch->submeshIndices[batch->numDraws] = idxSubmesh;
            batch->numDraws++;
        }
    }

    return true;
}

bool PreparePerDrawUniformsForPass(const DrawPass *pass) {
    ZoneScoped;
    auto &dev = pass->instance->device;
    auto *bm = dev.bufferManager;

    for (int k = 0; k < kTopoMaterialKind_Count; k++) {
        auto *cur = pass->batches[k];
        auto *handler = gDrawHandlers[k];
        auto *handlerUser = gDrawHandlerUserPtrs[k];
        while (cur != nullptr) {
            void *pUniformBuffer;
            size_t sizUniformBuffer;
            if (!bm_map_uniform_buffer(bm, cur->perDrawUniforms, &pUniformBuffer, &sizUniformBuffer)) {
                std::abort();
            }

            if (!handler->ComputePerDrawUniformData(handlerUser, pass, cur, pUniformBuffer)) {
                assert(!"Failed to get comptue per draw uniform data");
            }

            bm_flush_and_unmap_uniform_buffer(bm, cur->perDrawUniforms);

            cur = cur->next;
        }
    }
}

bool BindPerPassUniform(const DrawPass *pass, topo_material_kind materialKind, const char* pipelineId, const ShaderPipeline &pipeline0, VkCommandBuffer cmdBuf) {
    ZoneScoped;
    auto &dev = pass->instance->device;
    auto sm = dev.shaderManager;
    auto bm = dev.bufferManager;

    // Bind the per-pass uniform buffer of this pass
    VkDescriptorSet hPerPassSet;
    VkBuffer hPerPassUniformBuffer;

    auto *cur = pass->batches[materialKind];
    assert(cur->numDraws != 0);

    auto res = dev.descriptorPoolLists[dev.currentSwapchain.idxCurrentFrame].allocate(
        dev.handle, 1, &pipeline0.descriptorSetLayout[0], &hPerPassSet);
    assert(res == VK_SUCCESS);

    auto *handler = gDrawHandlers[materialKind];
    auto *handlerUser = gDrawHandlerUserPtrs[materialKind];

    uint32_t sizUniform;
    handler->GetPassUniformSize(handlerUser, &sizUniform);

    BmUniformBuffer hPerPassUniform;
    if (!sm_get_pass_uniform_buffer(
        sm, dev.currentSwapchain.idxCurrentFrame, pipelineId, sizUniform, &hPerPassUniform)) {
        return false;
    }

    if (!bm_get_uniform_buffer_handle(bm, hPerPassUniform, &hPerPassUniformBuffer)) {
        return false;
    }

    void *pUniformBuffer;
    size_t sizUniformBuffer;

    bm_map_uniform_buffer(bm, hPerPassUniform, &pUniformBuffer, &sizUniformBuffer);
    handler->ComputePerPassUniformData(handlerUser, pass, cur, pUniformBuffer);
    bm_flush_and_unmap_uniform_buffer(bm, hPerPassUniform);

    VkWriteDescriptorSet writes[1] = { {} };
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = hPerPassSet;
    writes[0].dstBinding = 0;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorBufferInfo descBufInfo = {};
    descBufInfo.offset = 0;
    descBufInfo.range = VK_WHOLE_SIZE;
    descBufInfo.buffer = hPerPassUniformBuffer;
    writes[0].pBufferInfo = &descBufInfo;
    vkUpdateDescriptorSets(dev.handle, 1, &writes[0], 0, nullptr);
    vkCmdBindDescriptorSets(
        cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline0.layout, 0, 1, &hPerPassSet, 0, nullptr);
    return true;
}

bool BindPerMaterialUniforms(
    const DrawPass* pass,
    const ShaderPipeline& pipeline,
    const ClientMaterial& material,
    VkCommandBuffer cmdBuf) {
    ZoneScoped;

    if (pipeline.numDescriptorSets < 3) {
        return true;
    }

    auto &dev = pass->instance->device;
    auto sm = dev.shaderManager;
    auto bm = dev.bufferManager;
    auto tm = dev.textureManager;

    VkDescriptorSet hPerMaterialSet;
    auto res = dev.descriptorPoolLists[dev.currentSwapchain.idxCurrentFrame].allocate(
        dev.handle, 1, &pipeline.descriptorSetLayout[2], &hPerMaterialSet);
    assert(res == VK_SUCCESS);


    auto *handler = gDrawHandlers[material.kind];
    auto *handlerUser = gDrawHandlerUserPtrs[material.kind];

    handler->UpdatePerMaterialUniforms(handlerUser, pass, material, hPerMaterialSet);

    vkCmdBindDescriptorSets(
        cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 2, 1, &hPerMaterialSet, 0, nullptr);

    return true;
}

bool InitPipelineDescriptor(arena_t &arena, const ClientMaterial &material, ShaderPipelineDescriptor &descriptor) {
    auto *handler = gDrawHandlers[material.kind];
    auto *handlerUser = gDrawHandlerUserPtrs[material.kind];
    return handler->InitPipelineDescriptor(handlerUser, arena, material, descriptor);
}
