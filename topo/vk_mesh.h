#pragma once

#include "vk_common.h"
#include "vk_forward.h"

bool mm_create(topo_instance *topo, MeshManager_t *out);

bool mm_make(MeshManager_t mm, const topo_mesh_descriptor *descriptor, topo_mesh_handle *out_handle);
bool mm_free(MeshManager_t mm, topo_mesh_handle mesh);
bool mm_release(MeshManager_t mm, topo_mesh_handle mesh);
bool mm_free_released_resources(MeshManager_t mm);

struct MMPass;

bool mm_begin_pass(MeshManager_t mm, arena_t &temp, const topo_camera &camera, MMPass **out);
bool mm_draw(MMPass *pass, arena_t &temp, topo_mesh_handle mesh, const Transform &transform);
bool mm_end_pass(
    MMPass *pass,
    arena_t &temp,
    VkCommandBuffer cmdBuf,
    const VkRenderPassBeginInfo &renderPassInfo);
