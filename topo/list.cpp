#include "list.h"

XXH64_hash_t hash_id(XXH64_hash_t hashSeed, const char *id, size_t len) {
    return XXH64(id, len, hashSeed);
}

void create_ids(
    arena_t *arena,
    XXH64_hash_t hashSeed,
    size_t numIds,
    const char *const *id_strings,
    Id *out_ids) {
    for (size_t idxId = 0; idxId < numIds; idxId++) {
        out_ids[idxId].lenId = strlen(id_strings[idxId]);
    }

    for (size_t idxId = 0; idxId < numIds; idxId++) {
        auto &out = out_ids[idxId];
        out.hash = hash_id(hashSeed, id_strings[idxId], out_ids[idxId].lenId);
    }

    for (size_t idxId = 0; idxId < numIds; idxId++) {
        auto &out = out_ids[idxId];
        char *bufCopy = nullptr;
        arena->pushElements(out.lenId, &bufCopy);
        memcpy(bufCopy, id_strings[idxId], out.lenId);
        out.id = bufCopy;
    }
}
