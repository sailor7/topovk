#include "vk_descriptor_pool_list.h"

void DescriptorPoolList::destroy(VkDevice dev) {
    if (!first)
        return;

    auto *it = first;
    while (it != last) {

        vkDestroyDescriptorPool(dev, *it, nullptr);
        it++;
    }
    vkDestroyDescriptorPool(dev, *last, nullptr);

    first = last = current = nullptr;
    numSets = 0;
    arena.clear();
}

void DescriptorPoolList::reset(VkDevice dev) {
    if (!first) {
        return;
    }

    auto *it = first;

    while (it != last) {

        vkResetDescriptorPool(dev, *it, 0);
        it++;
    }

    vkResetDescriptorPool(dev, *last, 0);

    current = first;
    numSets = 0;
}

VkResult DescriptorPoolList::allocate(
    VkDevice dev,
    uint32_t numSets,
    const VkDescriptorSetLayout *layouts,
    VkDescriptorSet *sets) {
    VkDescriptorSetAllocateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    info.pSetLayouts = layouts;
    uint32_t numRemains = numSets;
    uint32_t idxCurrent = 0;
    VkResult res;

    if (!first) {
        res = pushPool(dev);
        assert(res == VK_SUCCESS);
        current = first;
    }

    while (numRemains > 0) {
        auto numFits = this->numMaxSets - this->numSets;
        assert(numFits != 0);
        info.descriptorPool = *current;
        auto numToAllocate = (numRemains >= numFits) ? numFits : numRemains;
        info.descriptorSetCount = numToAllocate;
        res = vkAllocateDescriptorSets(dev, &info, &sets[idxCurrent]);
        assert(res == VK_SUCCESS);

        numRemains -= numToAllocate;
        idxCurrent += numToAllocate;
        this->numSets += numToAllocate;

        if (this->numSets == numMaxSets) {
            res = pushPool(dev);
            assert(res == VK_SUCCESS);
            current++;
            this->numSets = 0;
        }
    }

    return VK_SUCCESS;
}

VkResult DescriptorPoolList::pushPool(VkDevice dev) {
    VkDescriptorPoolSize poolSizes[2] = { {}, {} };
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = (uint32_t)numMaxSets;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = (uint32_t)numMaxSets;
    VkDescriptorPoolCreateInfo poolInfo {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = 2;
    poolInfo.pPoolSizes = poolSizes;
    poolInfo.maxSets = (uint32_t)numMaxSets;
    VkDescriptorPool *handle;
    arena.pushElements<VkDescriptorPool>(1, &handle);

    auto res = vkCreateDescriptorPool(dev, &poolInfo, nullptr, handle);
    if (res != VK_SUCCESS) {
        return res;
    }

    last = handle;
    if (!first) {
        first = handle;
    }
    return VK_SUCCESS;
}
