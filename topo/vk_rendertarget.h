#pragma once

#include "vk_forward.h"

#include <topo/topo.h>

#include <vulkan/vulkan.h>

struct ColorAttachment {
    VkFormat pixelFormat;
};

struct RenderTargetInfo {
    const char *id;
    uint32_t width, height;

    const char *compatiblePassId;

    uint8_t numColorAttachments;
    const ColorAttachment *pColorAttachments;

    uint8_t numDepthAttachments;
};

topo_result rt_create(topo_instance_t instance, const RenderTargetInfo &info, RenderTarget_t *out);
topo_result rt_resize(RenderTarget_t renderTarget, uint32_t width, uint32_t height);
topo_result rt_destroy(RenderTarget_t renderTarget);
topo_result rt_prepare_for_display(RenderTarget_t renderTarget, uint32_t idxImage, VkCommandBuffer cmdBuffer);
topo_result rt_prepare_for_rendering(RenderTarget_t renderTarget, uint32_t idxImage, VkCommandBuffer cmdBuffer);
topo_result
rt_bind(RenderTarget_t renderTarget, uint32_t idxImage, VkRenderPassBeginInfo &renderPassBeginInfo);
topo_result rt_bind(
    RenderTarget_t renderTarget,
    uint32_t idxImage,
    uint32_t idxAttachment,
    VkDescriptorImageInfo &imageInfo);
topo_result
rt_bind(RenderTarget_t renderTarget, uint32_t idxImage, VkDevice dev, arena_t &temp, VkDescriptorSet set);