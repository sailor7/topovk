#pragma once

#include "vk_instance.h"
#include "vk_shaders.h"

inline constexpr uint32_t DRAW_BATCH_SIZE = 128;

struct SubMesh {
    VkPipelineInputAssemblyStateCreateInfo inputAssembly;

    const char *namePipeline;
    size_t numVertices;

    bool isIndexed;
    BmIndexBuffer indexBuffer;

    ShaderPipelineDescriptor descriptor;

    topo_material_t hMaterial;
};

struct Mesh : ListNode<Mesh> {
    arena_t arena { arena_t::kilobytes(32) };

    bool released = false;
    size_t idxLastFrameUsed = 0;

    VkPipelineVertexInputStateCreateInfo vertexInputState;

    size_t numBuffers;
    BmVertexBuffer *buffers;

    size_t numSubMeshes;
    SubMesh *subMeshes;
};

struct DrawBatch {
    uint32_t numDraws = 0;

    Mesh **meshes = nullptr;
    uint32_t *submeshIndices = nullptr;
    Transform *transforms = nullptr;

    uint32_t perDrawUniformsStride = 0;
    BmUniformBuffer perDrawUniforms;

    DrawBatch *next = nullptr;
};

struct MMPass {
    topo_instance_t instance;

    topo_camera camera;
    DrawBatch *batches[kTopoMaterialKind_Count];

    uint32_t currentPerDrawUniformBufferOffset = 0;

    VkPipeline currentBoundPipeline = VK_NULL_HANDLE;
};

using DrawPass = MMPass;

// ===

using MaterialDrawHandler_GetDrawUniformSize = bool (*)(void *user, uint32_t *out_size);
using MaterialDrawHandler_GetPassUniformSize = bool (*)(void *user, uint32_t *out_size);
using MaterialDrawHandler_ComputePerDrawUniformData
    = bool (*)(void *user, const DrawPass *pass, DrawBatch *batch, void *pUniformBuffer);
using MaterialDrawHandler_ComputePerPassUniformData
    = bool (*)(void *user, const DrawPass *pass, DrawBatch *batch, void *pUniformBuffer);
using MaterialDrawHandler_UpdatePerMaterialUniforms
    = bool (*)(void *user, const DrawPass *pass, const ClientMaterial &material, VkDescriptorSet set);
using MaterialDrawHandler_InitPipelineDescriptor = bool (
        *)(void *user, arena_t &arena, const ClientMaterial &material, ShaderPipelineDescriptor &descriptor);

struct MaterialDrawHandlerOps {
    MaterialDrawHandler_GetDrawUniformSize GetDrawUniformSize;
    MaterialDrawHandler_ComputePerDrawUniformData ComputePerDrawUniformData;
    MaterialDrawHandler_ComputePerPassUniformData ComputePerPassUniformData;
    MaterialDrawHandler_GetPassUniformSize GetPassUniformSize;
    MaterialDrawHandler_UpdatePerMaterialUniforms UpdatePerMaterialUniforms;
    MaterialDrawHandler_InitPipelineDescriptor InitPipelineDescriptor;
};

bool RegisterMaterialDrawHandler(
    topo_material_kind materialKind,
    const MaterialDrawHandlerOps *ops,
    void *user);

#define REGISTER_MATERIAL_DRAW_HANDLER(materialKind, ops, user)                                              \
    static struct MaterialDrawHandlerRegister##materialKind {                                                \
        MaterialDrawHandlerRegister##materialKind() {                                                        \
            RegisterMaterialDrawHandler(materialKind, ops, user);                                            \
        }                                                                                                    \
    } _mdhreg##materialKind;

// ===

bool InsertIntoDrawPass(DrawPass *pass, arena_t &temp, topo_mesh_handle mesh, const Transform &transform);
bool PreparePerDrawUniformsForPass(const DrawPass *pass);
bool BindPerPassUniform(
    const DrawPass *pass,
    topo_material_kind materialKind,
    const char *pipelineId,
    const ShaderPipeline &pipeline0,
    VkCommandBuffer cmdBuf);
bool BindPerMaterialUniforms(
    const DrawPass *pass,
    const ShaderPipeline &pipeline,
    const ClientMaterial &material,
    VkCommandBuffer cmdBuf);
bool InitPipelineDescriptor(
    arena_t &arena,
    const ClientMaterial &material,
    ShaderPipelineDescriptor &descriptor);