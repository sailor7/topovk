#pragma once


#include "list.h"
#include <arena.h>

#include <SDL.h>


#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

#include "vk_forward.h"


struct Transform {
    glm::vec3 position;
    glm::quat rotation;
    glm::vec3 scale;
};

