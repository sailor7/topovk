#pragma once

#include "vk_forward.h"

#include <vulkan/vulkan.h>

struct ImageInfo {
    uint32_t width;
    uint32_t height;
    uint32_t depth;
    VkFormat format;
    VkImageTiling tiling;
};

bool bm_create(BufferManager_t *out, topo_instance *topo);
bool bm_destroy(BufferManager_t bm);
bool bm_free_released_allocations(BufferManager_t bm);

bool bm_make_vertex_buffer(
    BufferManager_t bm,
    size_t sizBuffer,
    const void *data,
    BmVertexBuffer *out_handle);
bool bm_free_vertex_buffer(BufferManager_t bm, BmVertexBuffer handle);
bool bm_release_vertex_buffer(BufferManager_t bm, BmVertexBuffer handle);
bool bm_get_vertex_buffer_handle(BufferManager_t bm, BmVertexBuffer buffer, VkBuffer *handle);

bool bm_make_index_buffer(BufferManager_t bm, size_t sizBuffer, const void *data, BmIndexBuffer *out_handle);
bool bm_release_index_buffer(BufferManager_t bm, BmIndexBuffer handle);
bool bm_free_index_buffer(BufferManager_t bm, BmIndexBuffer handle);
bool bm_get_index_buffer_handle(BufferManager_t bm, BmIndexBuffer buffer, VkBuffer *handle);

bool bm_make_uniform_buffer(
    BufferManager_t bm,
    size_t sizElement,
    size_t numElements,
    VkBufferUsageFlags usage,
    BmUniformBuffer *out_handle,
    uint32_t *out_stride);
bool bm_map_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer, void **out_addr, size_t *out_size);
bool bm_flush_and_unmap_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer);
bool bm_free_uniform_buffer(BufferManager_t bm, BmUniformBuffer handle);
bool bm_get_uniform_buffer_handle(BufferManager_t bm, BmUniformBuffer buffer, VkBuffer *handle);
bool bm_update_uniform_buffer(
    BufferManager_t bm,
    BmUniformBuffer buffer,
    size_t sizData,
    const void *data,
    VkDeviceSize offset);
bool bm_update_uniform_buffer(
    BufferManager_t bm,
    BmUniformBuffer buffer,
    VkCommandBuffer cmdBuf,
    size_t sizData,
    const void *data,
    VkDeviceSize offset);

bool bm_update_uniform_buffer(BufferManager_t bm, BmUniformBuffer buffer, size_t sizData, const void *data);

/**
 * Creates a device image and initializes it with `sizBuffer` number of bytes from `data`.
 */
bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *image,
    size_t sizBuffer,
    const void *data,
    BmImage *out_handle);

/**
 * Creates a device image for a depth buffer.
 */
bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *info,
    size_t sizBuffer,
    BmImage *out_handle);

bool bm_make_image(
    BufferManager_t bm,
    const ImageInfo *info,
    size_t sizBuffer,
    VkBufferUsageFlags usage,
    BmImage *out_handle);
bool bm_release_image(BufferManager_t bm, BmImage handle);
bool bm_free_image(BufferManager_t bm, BmImage handle);
bool bm_get_image_handle(BufferManager_t bm, BmImage buffer, VkImage *handle);
