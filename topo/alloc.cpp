#include "alloc.h"

#include <cassert>
#include <fmt/core.h>
#include <fmt/format.h>
#include <new>

#include <arena.h>

#include <tracy/Tracy.hpp>

static const char *DEFAULT_TAG = "unknown";

#if !defined(WIN32)
#define __debugbreak()
#endif

#undef assert
#define assert(expr)                                                                                         \
    do {                                                                                                     \
        if (!(expr)) {                                                                                       \
            dumpAllocator(alloc);                                                                            \
            __debugbreak();                                                                                  \
        }                                                                                                    \
    } while (0);

#define ENABLE_SANITY_CHECKS (0)

#if ENABLE_SANITY_CHECKS
#define MARK_INVALID(pNode)                                                                                  \
    pNode->isValid = false;                                                                                  \
    pNode->offset = 0;                                                                                       \
    pNode->size = 0
#define MARK_VALID(pNode) pNode->isValid = true
#define ASSERT_VALID(pNode) assert(pNode->isValid)
#define ASSERT_INVALID(pNode) assert(!pNode->isValid)
#else
#define MARK_INVALID(pNode)
#define MARK_VALID(pNode)
#define ASSERT_VALID(pNode)
#define ASSERT_INVALID(pNode)
#endif

struct NodeHandle {
    constexpr NodeHandle()
        : val(0) { }

    constexpr explicit NodeHandle(uint32_t val)
        : val(val) { }

    explicit operator uint32_t() const { return val; }

    constexpr bool operator==(const NodeHandle &rhs) const noexcept { return val == rhs.val; }
    constexpr bool operator!=(const NodeHandle &rhs) const noexcept { return val != rhs.val; }

private:
    uint32_t val;
};

static constexpr NodeHandle INVALID_NODE_HANDLE = NodeHandle(~uint32_t(0));

struct AllocationNode {
    AllocationNode(uint32_t handle)
        : handle(handle) { }

    NodeHandle prev = INVALID_NODE_HANDLE;
    NodeHandle next = INVALID_NODE_HANDLE;

    NodeHandle prevUnused = INVALID_NODE_HANDLE;
    NodeHandle nextUnused = INVALID_NODE_HANDLE;

    const NodeHandle handle;

    bool isUsed = false;
    uint32_t offset = 0;
    uint32_t alignedOffset = 0;
    uint32_t size = 0;

#if ENABLE_SANITY_CHECKS
    bool isValid = false;
#endif
};

enum {
    ALLOCATOR_TAG_SIZ = 64,
};

struct alloc {
    arena_t arenaNodes { arena_t::kilobytes(512) };
    // First node in the list of all valid allocation nodes.
    // The list contains both used and unused blocks.
    NodeHandle firstNode = INVALID_NODE_HANDLE;
    NodeHandle firstUnusedNode = INVALID_NODE_HANDLE;
    // A LIFO stack of invalid nodes
    NodeHandle firstFreeNode = INVALID_NODE_HANDLE;

    char tag[ALLOCATOR_TAG_SIZ];
};

static AllocationNode *resolve_node_handle(alloc_t alloc, NodeHandle handle) {
    auto off = index_to_arena_offset<AllocationNode>((uint32_t)handle);
    return alloc->arenaNodes.resolve<AllocationNode>(off);
}

static void printAll(alloc_t alloc) {
    auto hNode = alloc->firstNode;
    while (hNode != INVALID_NODE_HANDLE) {
        auto *pNode = resolve_node_handle(alloc, hNode);
        fmt::print(
            "  handle {} used {} next {} size {}\n", (uint32_t)pNode->handle, pNode->isUsed,
            (uint32_t)pNode->next, pNode->size);

        hNode = pNode->next;
    }
}

static void dumpAllocator(alloc_t alloc) {
    fmt::print("DUMPING ALLOCATOR\n");
    printAll(alloc);
}

static bool allocate_new_node(alloc_t alloc, NodeHandle *out_handle) {
    ZoneScoped;
    AllocationNode *node;
    arena_offset offNode;
    NodeHandle hNode;
    if (alloc->firstFreeNode != INVALID_NODE_HANDLE) {
        hNode = alloc->firstFreeNode;
        node = resolve_node_handle(alloc, hNode);
        alloc->firstFreeNode = node->next;
        node->isUsed = false;
        node->next = INVALID_NODE_HANDLE;
        node->prev = INVALID_NODE_HANDLE;
        node->offset = 0;
        node->size = 0;
    } else {
        offNode = alloc->arenaNodes.pushElements(1, &node);
        assert(offNode & ARENA_INDEX_VALID_MASK);
        node = alloc->arenaNodes.resolve<AllocationNode>(offNode);
        auto handle = arena_offset_to_index<AllocationNode>(offNode);
        new (node) AllocationNode { handle };
        // fmt::print("[{}] New node with handle={}\n", alloc->tag, (uint32_t)handle);
    }

    MARK_INVALID(node);
    *out_handle = node->handle;
    return true;
}

bool alloc_create(alloc_t *out_handle, size_t sizBlock) {
    if (!out_handle || sizBlock == 0) {
        // assert(0);
        return false;
    }

    auto A = new struct alloc;

    alloc_tag(A, DEFAULT_TAG);
    allocate_new_node(A, &A->firstNode);
    auto *node = resolve_node_handle(A, A->firstNode);
    node->offset = 0;
    node->size = sizBlock;
    node->isUsed = false;
    MARK_VALID(node);
    A->firstUnusedNode = A->firstNode;
    *out_handle = A;
    return true;
}

bool alloc_destroy(alloc_t *handle) {
    if (!handle || !*handle) {
        // assert(0);
        return false;
    }

    auto *alloc = *handle;

    auto cur = alloc->firstNode;
    while (cur != INVALID_NODE_HANDLE) {
        auto *pNode = resolve_node_handle(alloc, cur);
        if (pNode->isUsed) {
            // fmt::print(
            //     "[{}] Block #{} of size {} bytes is still in use\n", alloc->tag, (uint32_t)pNode->handle,
            //     pNode->size);
        }

        cur = pNode->next;
    }

    delete alloc;
    *handle = nullptr;
    return true;
}

bool alloc_tag(alloc_t handle, const char *tag) {
    if (!handle || !tag) {
        // assert(0);
        return false;
    }

    strncpy(handle->tag, tag, ALLOCATOR_TAG_SIZ - 1);
    handle->tag[ALLOCATOR_TAG_SIZ - 1] = '\0';

    return true;
}

static void assert_node_is_not_present_in_lists(alloc_t alloc, const NodeHandle hNode) {
#if ENABLE_SANITY_CHECKS
    auto hCur = alloc->firstNode;
    while (hCur != INVALID_NODE_HANDLE) {
        if (hCur == hNode) {
            __debugbreak();
        }

        auto *pNode = resolve_node_handle(alloc, hCur);
        hCur = pNode->next;
    }
#endif
}

static bool find_unused_block_large_enough(
    alloc_t alloc,
    size_t size,
    size_t alignment,
    NodeHandle *hNode,
    size_t *out_nextAlignedOffset,
    size_t *out_endOffset) {
    ZoneScoped;
    assert(alloc);
    assert(size != 0);
    assert(hNode);
    assert(out_nextAlignedOffset);
    assert(out_endOffset);

    auto prev = INVALID_NODE_HANDLE;
    auto cur = alloc->firstUnusedNode;
    while (cur != INVALID_NODE_HANDLE) {
        auto *pNode = resolve_node_handle(alloc, cur);
        ASSERT_VALID(pNode);
        assert(!pNode->isUsed);

        auto offBlockEnd = pNode->offset + pNode->size;
        auto alignMask = alignment - 1;
        auto nextAlignedOffset = (pNode->offset + alignMask) & (~alignMask);
        auto endOffset = nextAlignedOffset + size;
        auto hasEnoughSpace = endOffset <= offBlockEnd;
        if (hasEnoughSpace) {
            // fmt::print(
            //     "[{}] Allocation of {} bytes (align={}) would fit into block #{} [{} -> {}]\n", alloc->tag,
            //     size, alignment, (uint32_t)cur, pNode->offset, offBlockEnd);
            *hNode = cur;
            *out_nextAlignedOffset = nextAlignedOffset;
            *out_endOffset = endOffset;
            return true;
        }

        prev = cur;
        cur = pNode->nextUnused;
    }

    fmt::print("[{}] Allocation of {} bytes (align={}) cannot fit\n", alloc->tag, size, alignment);
    return false;
}

bool alloc_would_fit(alloc_t alloc, size_t size, size_t alignment, bool *out) {
    ZoneScoped;
    if (!alloc || size == 0 || !out) {
        assert(0);
        return false;
    }

    NodeHandle hDiscard;
    size_t discardSize;
    *out = find_unused_block_large_enough(alloc, size, alignment, &hDiscard, &discardSize, &discardSize);

    return true;
}

static void remove_from_unused_list(alloc_t alloc, NodeHandle hNode, AllocationNode *pNode = nullptr) {
    if (pNode == nullptr) {
        pNode = resolve_node_handle(alloc, hNode);
    }

    auto hPrevUnused = pNode->prevUnused;
    auto hNextUnused = pNode->nextUnused;
    AllocationNode *pPrevUnused = nullptr;
    AllocationNode *pNextUnused = nullptr;

    if (hPrevUnused != INVALID_NODE_HANDLE) {
        pPrevUnused = resolve_node_handle(alloc, hPrevUnused);
    }

    if (hNextUnused != INVALID_NODE_HANDLE) {
        pNextUnused = resolve_node_handle(alloc, hNextUnused);
    }

    assert(pNode->prevUnused == INVALID_NODE_HANDLE || pPrevUnused->nextUnused == hNode);
    assert(pNode->nextUnused == INVALID_NODE_HANDLE || pNextUnused->prevUnused == hNode);

    assert(!pNode->isUsed);

    if (hPrevUnused != INVALID_NODE_HANDLE) {
        assert(!pPrevUnused->isUsed);
        assert(pPrevUnused->nextUnused == hNode);
        pPrevUnused->nextUnused = hNextUnused;
    } else {
        assert(alloc->firstUnusedNode == hNode);
        alloc->firstUnusedNode = hNextUnused;
    }

    if (hNextUnused != INVALID_NODE_HANDLE) {
        assert(!pNextUnused->isUsed);
        assert(pNextUnused->prevUnused == hNode);
        pNextUnused->prevUnused = hPrevUnused;
    }

    pNode->prevUnused = INVALID_NODE_HANDLE;
    pNode->nextUnused = INVALID_NODE_HANDLE;
}

bool alloc_allocate(alloc_t alloc, size_t size, size_t alignment, alloc_handle *out_handle) {
    ZoneScoped;
    if (!alloc || !out_handle) {
        assert(0);
        fmt::print(
            "alloc_allocate: invalid args: alloc {} size {} out_handle {}\n", (void *)alloc, size,
            (void *)out_handle);
        return false;
    }

    if (size == 0) {
        size = 16;
    }

    *out_handle = (alloc_handle)INVALID_NODE_HANDLE;

    NodeHandle hNode;
    size_t alignedOffset, endOffset;
    if (!find_unused_block_large_enough(alloc, size, alignment, &hNode, &alignedOffset, &endOffset)) {
        return false;
    }

    auto *pNode = resolve_node_handle(alloc, hNode);
    assert(!pNode->isUsed);
    assert(pNode->offset <= alignedOffset && endOffset <= pNode->offset + pNode->size);
    ASSERT_VALID(pNode);
    assert(pNode->prev == INVALID_NODE_HANDLE || pNode->prev != pNode->next);
    pNode->alignedOffset = alignedOffset;

    // Size of the allocation after alignment
    auto alignedSize = endOffset - pNode->offset;
    bool fitsExactly = endOffset == pNode->offset + pNode->size;

    remove_from_unused_list(alloc, hNode, pNode);

    if (alloc->firstUnusedNode == INVALID_NODE_HANDLE && fitsExactly) {
        fmt::print("[{}] Last unused node allocated\n", alloc->tag);
    }

    if (!fitsExactly) {
        // There will be some unused space at the end of this block.
        // Split the block into two parts.
        NodeHandle hNodeRemaining;
        if (!allocate_new_node(alloc, &hNodeRemaining)) {
            fmt::print("[{}] Failed to allocate new pNode\n", alloc->tag);
            return false;
        }

        auto *pNodeRemaining = resolve_node_handle(alloc, hNodeRemaining);
        pNodeRemaining->isUsed = false;
        pNodeRemaining->offset = endOffset;
        pNodeRemaining->size = pNode->size - alignedSize;
        // insert into the block list
        pNodeRemaining->prev = hNode;
        pNodeRemaining->next = INVALID_NODE_HANDLE;
        MARK_VALID(pNodeRemaining);
        pNodeRemaining->prevUnused = INVALID_NODE_HANDLE;
        auto hFirstUnusedNode = alloc->firstUnusedNode;
        pNodeRemaining->nextUnused = hFirstUnusedNode;
        if (hFirstUnusedNode != INVALID_NODE_HANDLE) {
            auto *pFirstUnusedNode = resolve_node_handle(alloc, hFirstUnusedNode);
            assert(pFirstUnusedNode->prevUnused == INVALID_NODE_HANDLE);
            pFirstUnusedNode->prevUnused = hNodeRemaining;
        }

        alloc->firstUnusedNode = hNodeRemaining;

        pNode->size = alignedSize;

        // Insert between pNode and pNode->next
        if (pNode->next != INVALID_NODE_HANDLE) {
            auto *pNext = resolve_node_handle(alloc, pNode->next);
            ASSERT_VALID(pNext);
            pNext->prev = hNodeRemaining;
            pNodeRemaining->next = pNode->next;
        }

        pNode->next = hNodeRemaining;

        assert(pNode->next == hNodeRemaining);
        assert(pNodeRemaining->prev == hNode);
        assert(pNode->prev == INVALID_NODE_HANDLE || pNode->prev != pNode->next);

        // fmt::print(
        //     "[{}] Split #0 {} {} #1 {} {}\n", alloc->tag, pNode->offset, pNode->size,
        //     pNodeRemaining->offset, pNodeRemaining->size);
    } else {
    }

    pNode->isUsed = true;

    assert(pNode->prev == INVALID_NODE_HANDLE || pNode->prev != pNode->next);
    assert(pNode->prev == INVALID_NODE_HANDLE || (resolve_node_handle(alloc, pNode->prev)->next == hNode));
    assert(pNode->next == INVALID_NODE_HANDLE || (resolve_node_handle(alloc, pNode->next)->prev == hNode));
    assert(hNode != pNode->prev);
    assert(hNode != pNode->next);

    *out_handle = (alloc_handle)pNode->handle;

    return true;
}

static void alloc_push_free(alloc_t alloc, NodeHandle hNode, AllocationNode *pNode) {
    assert(hNode != INVALID_NODE_HANDLE);
    assert(pNode != nullptr);
    assert_node_is_not_present_in_lists(alloc, hNode);
    ASSERT_INVALID(pNode);
    pNode->prev = INVALID_NODE_HANDLE;
    pNode->next = alloc->firstFreeNode;
    pNode->prevUnused = INVALID_NODE_HANDLE;
    pNode->nextUnused = INVALID_NODE_HANDLE;
    alloc->firstFreeNode = hNode;
    // fmt::print("[{}] Added block #{} to the free list\n", alloc->tag, (uint32_t)hNode);
}

bool alloc_free(alloc_t alloc, alloc_handle handle) {
    ZoneScoped;
    auto hNode = NodeHandle(handle);
    if (!alloc || hNode == INVALID_NODE_HANDLE) {
        assert(0);
        return false;
    }

    auto *const pNode = resolve_node_handle(alloc, hNode);
    ASSERT_VALID(pNode);
    // fmt::print("[{}] Freeing #{}\n", alloc->tag, (uint32_t)hNode);

    pNode->isUsed = false;

    auto hFirstUnused = alloc->firstUnusedNode;
    if (hFirstUnused != INVALID_NODE_HANDLE) {
        auto *pFirstUnused = resolve_node_handle(alloc, hFirstUnused);
        assert(pFirstUnused->prevUnused == INVALID_NODE_HANDLE);
        pFirstUnused->prevUnused = hNode;
    }

    pNode->nextUnused = alloc->firstUnusedNode;
    pNode->prevUnused = INVALID_NODE_HANDLE;
    alloc->firstUnusedNode = hNode;

    while (pNode->prev != INVALID_NODE_HANDLE) {
        auto hPrev = pNode->prev;
        auto *const pPrev = resolve_node_handle(alloc, hPrev);
        ASSERT_VALID(pPrev);
        if (pPrev->isUsed) {
            break;
        }

        // fmt::print("[{}] Merging #{} into #{}\n", alloc->tag, (uint32_t)hPrev, (uint32_t)hNode);

        auto offPrev = pPrev->offset;
        auto sizPrev = pPrev->size;
        auto hPrevPrev = pPrev->prev;

        remove_from_unused_list(alloc, hPrev, pPrev);

        // Merge previous block into current block
        pNode->offset = offPrev;
        pNode->size += sizPrev;

        pPrev->isUsed = false;
        pNode->prev = hPrevPrev;

        if (hPrevPrev != INVALID_NODE_HANDLE) {
            auto pPrevPrev = resolve_node_handle(alloc, hPrevPrev);
            assert(pPrevPrev->next == hPrev);
            pPrevPrev->next = hNode;
        } else {
            alloc->firstNode = hNode;
        }

        MARK_INVALID(pPrev);
        alloc_push_free(alloc, hPrev, pPrev);

        // hPrev node was merged into hNode
        // If it was the first node, replace that with hNode
        if (hPrev == alloc->firstNode) {
            alloc->firstNode = hNode;
        }
    }

    while (pNode->next != INVALID_NODE_HANDLE) {
        auto hNext = pNode->next;
        auto *const pNext = resolve_node_handle(alloc, hNext);
        ASSERT_VALID(pNext);
        if (pNext->isUsed) {
            break;
        }

        // fmt::print("[{}] Merging #{} into #{}\n", alloc->tag, (uint32_t)hNext, (uint32_t)hNode);

        auto offNext = pNext->offset;
        auto sizNext = pNext->size;
        auto hNextNext = pNext->next;

        remove_from_unused_list(alloc, hNext, pNext);

        // Merge previous block into current block
        pNode->size += sizNext;
        assert(pNode->offset < offNext && offNext <= pNode->offset + pNode->size);

        pNext->isUsed = false;
        pNode->next = hNextNext;

        if (hNextNext != INVALID_NODE_HANDLE) {
            auto pNextNext = resolve_node_handle(alloc, hNextNext);
            assert(pNextNext->prev == hNext);
            pNextNext->prev = hNode;
        }

        MARK_INVALID(pNext);
        alloc_push_free(alloc, hNext, pNext);
    }

    assert(pNode->prev == INVALID_NODE_HANDLE || (resolve_node_handle(alloc, pNode->prev)->next == hNode));
    assert(pNode->next == INVALID_NODE_HANDLE || (resolve_node_handle(alloc, pNode->next)->prev == hNode));

    return true;
}

bool alloc_get_metrics(
    alloc_t alloc,
    size_t *sizTotal,
    size_t *sizAllocated,
    size_t *sizFree,
    float *fragmentation) {
    auto hNode = alloc->firstNode;

    *sizTotal = 0;
    *sizAllocated = 0;
    *sizFree = 0;

    size_t quality = 0;

    while (hNode != INVALID_NODE_HANDLE) {
        auto *const pNode = resolve_node_handle(alloc, hNode);
        ASSERT_VALID(pNode);
        *sizTotal += pNode->size;
        if (pNode->isUsed) {
            *sizAllocated += pNode->size;
        } else {
            *sizFree += pNode->size;
            quality += pNode->size * pNode->size;
        }
        hNode = pNode->next;
    }

    double qualityPercent = sqrt(quality) / (double)(*sizFree);
    *fragmentation = 1 - (qualityPercent * qualityPercent);

    return true;
}

bool alloc_get_sizes(alloc_t alloc, size_t *sizTotal, size_t *sizAllocated, size_t *sizFree) {
    auto hNode = alloc->firstNode;

    *sizTotal = 0;
    *sizAllocated = 0;
    *sizFree = 0;

    while (hNode != INVALID_NODE_HANDLE) {
        auto *const pNode = resolve_node_handle(alloc, hNode);
        ASSERT_VALID(pNode);
        *sizTotal += pNode->size;
        if (pNode->isUsed) {
            *sizAllocated += pNode->size;
        } else {
            *sizFree += pNode->size;
        }
        hNode = pNode->next;
    }

    return true;
}

bool alloc_get_offset(alloc_t alloc, alloc_handle allocation, uint32_t *out_offset) {
    auto hNode = NodeHandle(allocation);
    if (!alloc || hNode == INVALID_NODE_HANDLE || !out_offset) {
        assert(0);
        return false;
    }

    auto *pNode = resolve_node_handle(alloc, hNode);
    ASSERT_VALID(pNode);
    assert(pNode->isUsed);
    if (!pNode->isUsed) {
        return false;
    }

    *out_offset = pNode->alignedOffset;

    return true;
}
