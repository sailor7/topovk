#pragma once

#include <arena.h>
#include <vulkan/vulkan_core.h>

struct DescriptorPoolList {
    arena_t arena { arena_t::kilobytes(4) };
    const uint32_t numMaxSets = 512;
    uint32_t numSets = 0;
    VkDescriptorPool *first = nullptr, *last = nullptr, *current = nullptr;

    void destroy(VkDevice dev);
    void reset(VkDevice dev);
    VkResult
    allocate(VkDevice dev, uint32_t numSets, const VkDescriptorSetLayout *layouts, VkDescriptorSet *sets);

    VkResult pushPool(VkDevice dev);
};
