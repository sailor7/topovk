#pragma once

#include "vk_common.h"
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

enum ShaderPipelineInputFlags {
    PIPELINE_INPUT_POSITION = 1,
    PIPELINE_INPUT_TEXCOORD = 2,
    PIPELINE_INPUT_TBN = 4,
};

struct DescriptorSet {
    uint32_t numBindings;
    const VkDescriptorSetLayoutBinding *arrBindings;
};

struct ShaderPipelineDescriptor {
    const char *passId;
    const char *vertexShaderId;
    const char *fragmentShaderId;

    bool drawsToSwapchain;

    uint32_t numDescriptorSets;
    const DescriptorSet *arrDescriptorSets;

    const VkPipelineVertexInputStateCreateInfo *vertexInputState;
};

enum class PassKind {
    World,
    Blit,
};

struct PassDescriptorWorld {
    VkFormat outputFormats[4];
};

struct PassDescriptorBlit {
    VkFormat outputFormat;
    bool isTargetSwapchain;
};

struct PassDescriptor {
    PassKind kind;
    union {
        PassDescriptorWorld world;
        PassDescriptorBlit blit;
    };
};

struct ShaderPipeline {
    VkPipeline pipeline;
    VkPipelineLayout layout;
    uint32_t numDescriptorSets;
    VkDescriptorSetLayout descriptorSetLayout[4];
};

struct Pass {
    VkRenderPass handle;
};

typedef struct ShaderManager *ShaderManager_t;

bool sm_create(ShaderManager_t *out_sm, struct topo_instance *instance);
bool sm_destroy(ShaderManager_t *sm);

bool sm_load_shaders(
    ShaderManager_t sm,
    size_t numShaders,
    const char *ids[],
    const char *cachePaths[],
    const char *paths[]);

/**
 * NOTE: `pipeline` and it's contents must alive for the lifetime of the pipeline
 */
bool sm_create_pipeline(ShaderManager_t sm, const char *id, const ShaderPipelineDescriptor *pipeline);
bool sm_get_pipeline(ShaderManager_t sm, ShaderPipeline *out_pipeline, const char *id);

bool sm_get_pass_uniform_buffer(
    ShaderManager_t sm,
    uint32_t idxFrame,
    const char *id,
    size_t size,
    BmUniformBuffer *handle);
bool sm_get_draw_uniform_buffer(
    ShaderManager_t sm,
    uint32_t idxFrame,
    const char *id,
    size_t sizElement,
    size_t numElements,
    BmUniformBuffer *out_handle,
    uint32_t *out_stride,
    VkBufferUsageFlags usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);
bool sm_release_uniform_buffers(ShaderManager_t sm, uint32_t idxFrame, const char *id);
bool sm_release_uniform_buffers(ShaderManager_t sm, uint32_t idxFrame);

bool sm_create_pass(ShaderManager_t sm, const char *id, const PassDescriptor *descriptor);
bool sm_get_pass(ShaderManager_t sm, const char *id, Pass *pass);

bool sm_swapchain_resized(ShaderManager_t sm);
