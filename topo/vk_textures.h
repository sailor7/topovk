#pragma once

#include "vk_common.h"
#include <vulkan/vulkan.h>

enum class TextureFilter {
	POINT,
	LINEAR,
};

enum class TextureAddressMode {
	REPEAT,
};

enum class TextureAnisotropy {
    OFF,
    ON
};

struct SamplerInfo {
    TextureFilter filterMin;
    TextureFilter filterMag;
    TextureAddressMode addressModeU, addressModeV, addressModeW;
    TextureAnisotropy anisotropy;
};

bool tm_create(TextureManager_t *out_tm, struct topo_instance *instance);
bool tm_destroy(TextureManager_t *tm);
bool tm_free_released_resources(TextureManager_t tm, arena_t &arenaTemp);

[[deprecated]] bool tm_make(
    TextureManager_t tm,
    const char *id,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const void *data,
    const SamplerInfo &samplerInfo,
    Texture *out_handle);
bool tm_make(
    TextureManager_t tm,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const void *data,
    const SamplerInfo &samplerInfo,
    Texture *out_handle);
bool tm_make_color_buffer(
    TextureManager_t tm,
    const char *id,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const SamplerInfo &samplerInfo,
    Texture *out_handle);
bool tm_make_depth_buffer(
    TextureManager_t tm,
    const char *id,
    uint32_t width,
    uint32_t height,
    const SamplerInfo &samplerInfo,
    Texture *out_handle);
bool tm_free(TextureManager_t tm, Texture handle);
bool tm_release(TextureManager_t tm, Texture handle);
[[deprecated]] bool
tm_get_sampler_handle(TextureManager_t tm, Texture handle, VkImageView *out_view, VkSampler *out_sampler);
bool tm_get_texture_handles(
    TextureManager_t tm,
    uint32_t numTextures,
    const Texture *pHandles,
    VkImageView *out_views,
    VkSampler *out_samplers);
bool tm_get_view_handles(TextureManager_t tm, uint32_t numHandles, const Texture *handles, VkImageView *out_views);

bool tm_transition_layout(TextureManager_t tm, Texture handle, VkImageLayout nextLayout, VkCommandBuffer cmdBuffer);
