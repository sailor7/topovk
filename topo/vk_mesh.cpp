#include <topo/topo.h>

#include "list.h"
#include "vk_buffers.h"
#include "vk_debug.h"
#include "vk_draw.h"
#include "vk_instance.h"
#include "vk_mesh.h"
#include "vk_rendertarget.h"
#include "vk_shaders.h"
#include "vk_textures.h"

#include <vulkan/vulkan_core.h>

#include <fmt/core.h>
#include <string>

#include <tracy/Tracy.hpp>

#define TAG "[topo::mesh] "

struct MeshManager {
    topo_instance *topo;

    arena_t arena { arena_t::kilobytes(128) };

    Mesh *firstMesh = nullptr;

    Mesh *firstFreeMesh = nullptr;
};

static void hash(XXH64_state_t *state, const topo_vertex_attribute &attr) {
    XXH64_update(state, &attr, sizeof(attr));
}

static void hash(XXH64_state_t *state, const topo_vertex_buffer &buf) {
    XXH64_update(state, &buf.sizVertexStride, sizeof(buf.sizVertexStride));
}

static void hash(XXH64_state_t *state, const topo_mesh_descriptor &meshDescriptor) {
    XXH64_update(state, &meshDescriptor.numAttributes, sizeof(meshDescriptor.numAttributes));
    for (uint32_t idxAttr = 0; idxAttr < meshDescriptor.numAttributes; idxAttr++) {
        hash(state, meshDescriptor.pAttributes[idxAttr]);
    }

    XXH64_update(state, &meshDescriptor.numVertexBuffers, sizeof(meshDescriptor.numVertexBuffers));
    for (uint32_t idxBuf = 0; idxBuf < meshDescriptor.numVertexBuffers; idxBuf++) {
        hash(state, meshDescriptor.pVertexBuffers[idxBuf]);
    }
}

static void hash(XXH64_state_t *state, const topo_submesh &submesh) {
    XXH64_update(state, &submesh.primitiveTopology, sizeof(submesh.primitiveTopology));
    XXH64_update(state, &submesh.isIndexed, sizeof(submesh.isIndexed));
    XXH64_update(state, &submesh.indexBuffer.type, sizeof(submesh.indexBuffer.type));
}

static void hash(XXH64_state_t *state, const ClientMaterial &material) {
    XXH64_update(state, &material.kind, sizeof(material.kind));
}

bool mm_create(topo_instance *topo, MeshManager_t *out) {
    if (!topo || !out) {
        return false;
    }

    *out = new MeshManager;
    (*out)->topo = topo;

    return true;
}

static VkFormat convert(topo_vertex_element_type type) {
    switch (type) {
    case kTopoVET_F32x2:
        return VK_FORMAT_R32G32_SFLOAT;
    case kTopoVET_F32x3:
        return VK_FORMAT_R32G32B32_SFLOAT;
    case kTopoVET_F32x4:
        return VK_FORMAT_R32G32B32A32_SFLOAT;
    }

    std::abort();
}

static VkPrimitiveTopology convert(topo_primitive_topology topo) {
    switch (topo) {
    case kTopoPT_TriangleList:
        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }

    std::abort();
}

static uint32_t toLocation(topo_vertex_element_semantic sem) {
    switch (sem) {
    case kTopoVES_Position:
        return 0;
    case kTopoVES_Tangent:
        return 1;
    case kTopoVES_Bitangent:
        return 2;
    case kTopoVES_Normal:
        return 3;
    case kTopoVES_Texcoord:
        return 4;
    }

    std::abort();
}

bool mm_make(MeshManager_t mm, const topo_mesh_descriptor *descriptor, topo_mesh_handle *out_handle) {
    bool res;
    if (!mm->firstFreeMesh) {
        mm->arena.pushElements(1, &mm->firstFreeMesh);
        new (mm->firstFreeMesh) Mesh();
    }

    auto *mesh = mm->firstFreeMesh;
    mm->firstFreeMesh = mesh->next;

    *out_handle = (topo_mesh_handle)mesh;

    mesh->released = false;
    mesh->idxLastFrameUsed = 0;
    mesh->next = mm->firstMesh;
    mm->firstMesh = mesh;
    create_ids(&mm->arena, 0, 1, &descriptor->id, &mesh->id);

    // Create input bindings and upload buffers
    VkVertexInputBindingDescription *vertexInputBindingDesc = nullptr;
    mesh->numBuffers = descriptor->numVertexBuffers;
    mesh->arena.pushElements(mesh->numBuffers, &vertexInputBindingDesc);
    mesh->arena.pushElements(mesh->numBuffers, &mesh->buffers);
    for (size_t idxVertexBuffer = 0; idxVertexBuffer < mesh->numBuffers; idxVertexBuffer++) {
        auto &binding = vertexInputBindingDesc[idxVertexBuffer];
        auto &buffer = descriptor->pVertexBuffers[idxVertexBuffer];
        binding.binding = idxVertexBuffer;
        binding.stride = buffer.sizVertexStride;
        binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        res = bm_make_vertex_buffer(
            mm->topo->device.bufferManager, buffer.sizVertexData, buffer.pVertexData,
            &mesh->buffers[idxVertexBuffer]);
        assert(res);
    }

    VkVertexInputAttributeDescription *vertexInputAttrDesc = nullptr;
    mesh->arena.pushElements(descriptor->numAttributes, &vertexInputAttrDesc);

    for (size_t idxAttribute = 0; idxAttribute < descriptor->numAttributes; idxAttribute++) {
        auto &desc = descriptor->pAttributes[idxAttribute];
        auto &attr = vertexInputAttrDesc[idxAttribute];
        attr.binding = desc.idxVertexBuffer;
        attr.offset = desc.offset;
        attr.format = convert(desc.type);
        attr.location = toLocation(desc.semantic);
    }

    VkPipelineVertexInputStateCreateInfo vertexInputInfo {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = descriptor->numVertexBuffers;
    vertexInputInfo.pVertexBindingDescriptions = vertexInputBindingDesc;
    vertexInputInfo.vertexAttributeDescriptionCount = descriptor->numAttributes;
    vertexInputInfo.pVertexAttributeDescriptions = vertexInputAttrDesc;
    mesh->vertexInputState = vertexInputInfo;

    auto *hashState = XXH64_createState();

    mesh->numSubMeshes = descriptor->numSubmeshes;
    mesh->arena.pushElements(mesh->numSubMeshes, &mesh->subMeshes);
    for (uint32_t idxSubMesh = 0; idxSubMesh < descriptor->numSubmeshes; idxSubMesh++) {
        mesh->subMeshes[idxSubMesh] = {};
        auto &inSubmesh = descriptor->pSubmeshes[idxSubMesh];

        VkPipelineInputAssemblyStateCreateInfo inputAssembly {};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = convert(inSubmesh.primitiveTopology);
        inputAssembly.primitiveRestartEnable = VK_FALSE;

        mesh->subMeshes[idxSubMesh].isIndexed = descriptor->pSubmeshes[idxSubMesh].isIndexed;
        mesh->subMeshes[idxSubMesh].inputAssembly = inputAssembly;
        mesh->subMeshes[idxSubMesh].namePipeline = nullptr;
        mesh->subMeshes[idxSubMesh].numVertices = inSubmesh.indexBuffer.numIndices;
        mesh->subMeshes[idxSubMesh].hMaterial = inSubmesh.material;

        if (inSubmesh.isIndexed) {
            uint16_t sizElem = inSubmesh.indexBuffer.type == topo_index_buffer::UINT16 ? 2 : 4;
            size_t sizBuffer = inSubmesh.indexBuffer.numIndices * sizElem;
            res = bm_make_index_buffer(
                mm->topo->device.bufferManager, sizBuffer,
                descriptor->pSubmeshes[idxSubMesh].indexBuffer.pIndices,
                &mesh->subMeshes[idxSubMesh].indexBuffer);
            assert(res);
        }

        auto idxMaterial = (uint64_t)inSubmesh.material;
        assert(idxMaterial < mm->topo->device.materials.idxNextUnused);
        auto &material = mm->topo->device.materials.entries[idxMaterial];
        assert(material.used);

        // Hash the mesh and submesh descriptor (input assembler config, material kind, etc.) and use that as
        // a name for the pipeline to be used to render this submesh.
        XXH64_reset(hashState, 0);
        hash(hashState, *descriptor);
        hash(hashState, descriptor->pSubmeshes[idxSubMesh]);
        hash(hashState, material);
        auto hashPipeline = XXH64_digest(hashState);
        auto pipelineId = fmt::format("mmh-{}", hashPipeline);
        fmt::print(TAG "Mesh {} submesh #{} pipeline ID: {}\n", descriptor->id, idxSubMesh, pipelineId);

        char *pipelineIdCopy = nullptr;
        mesh->arena.pushElements(pipelineId.size() + 1, &pipelineIdCopy);
        strcpy(pipelineIdCopy, pipelineId.c_str());

        ShaderPipeline pipeline;
        if (!sm_get_pipeline(mm->topo->device.shaderManager, &pipeline, pipelineIdCopy)) {
            fmt::print(TAG "Pipeline with ID {} doesn't exists, creating it\n", pipelineIdCopy);
            auto &pipelineDescriptor = mesh->subMeshes[idxSubMesh].descriptor;
			pipelineDescriptor.vertexInputState = &mesh->vertexInputState;
            InitPipelineDescriptor(mesh->arena, material, pipelineDescriptor);
            res = sm_create_pipeline(mm->topo->device.shaderManager, pipelineIdCopy, &pipelineDescriptor);
            assert(res);
        }

        mesh->subMeshes[idxSubMesh].namePipeline = pipelineIdCopy;
    }

    XXH64_freeState(hashState);

    return true;
}

bool mm_free(MeshManager_t mm, topo_mesh_handle mesh) {
    auto *pMesh = (Mesh *)mesh;
    auto bm = mm->topo->device.bufferManager;

    for (size_t idxSubmesh = 0; idxSubmesh < pMesh->numSubMeshes; idxSubmesh++) {
        if (!pMesh->subMeshes[idxSubmesh].isIndexed) {
            continue;
        }

        bm_release_index_buffer(bm, pMesh->subMeshes[idxSubmesh].indexBuffer);
    }

    for (size_t idxBuffer = 0; idxBuffer < pMesh->numBuffers; idxBuffer++) {
        bm_release_vertex_buffer(bm, pMesh->buffers[idxBuffer]);
    }

    // FIXME: we can do better

    auto *pNext = pMesh->next;

    Mesh *pPrev = nullptr;
    Mesh *pCur = mm->firstMesh;

    while (pCur != pMesh && pCur != nullptr) {
        pPrev = pCur;
        pCur = pCur->next;
    }

    assert(pCur != nullptr);

    if (pPrev != nullptr) {
        pPrev->next = pNext;
    } else {
        mm->firstMesh = pNext;
    }

    pMesh->next = mm->firstFreeMesh;
    mm->firstFreeMesh = pMesh;

    pMesh->arena.clear();

    return true;
}

bool mm_release(MeshManager_t mm, topo_mesh_handle mesh) {
    auto *pMesh = (Mesh *)mesh;

    assert(!pMesh->released);
    pMesh->released = true;

    return true;
}

bool mm_free_released_resources(MeshManager_t mm) {
    auto *cur = mm->firstMesh;

    if (!cur) {
        return true;
    }

    auto idxCurrentFrame = mm->topo->device.currentSwapchain.idxCurrentFrame;
    auto shutdown = mm->topo->shutdown;
    while (cur != nullptr) {
        auto *next = cur->next;
        if (shutdown || (cur->released && cur->idxLastFrameUsed == idxCurrentFrame)) {
            mm_free(mm, (topo_mesh_handle)cur);
        }

        cur = next;
    }

    return true;
}

static void convert(glm::mat4 &out, const Transform &in) {
    ZoneScoped;
    auto matScale = glm::scale(glm::mat4(1.0f), in.scale);
    auto matRotate = glm::mat4_cast(in.rotation);
    auto matTranslate = glm::translate(glm::mat4(1.0f), in.position);
    out = matTranslate * matRotate * matScale;
}

static void convert(glm::mat4 &out, const topo_transform &in) {
    ZoneScoped;
    Transform t2;
    t2.position = glm::vec3(in.position[0], in.position[1], in.position[2]);
    t2.rotation = glm::quat(in.rotation[0], in.rotation[1], in.rotation[2], in.rotation[3]);
    t2.scale = glm::vec3(in.scale[0], in.scale[1], in.scale[2]);
    convert(out, t2);
}

static void convert(glm::mat4 &out, const topo_camera &in) {
    ZoneScoped;
    auto t = in.transform;
    Transform t2;
    t2.position = glm::vec3(t.position[0], t.position[1], t.position[2]);
    t2.rotation = glm::quat(t.rotation[0], t.rotation[1], t.rotation[2], t.rotation[3]);
    t2.scale = glm::vec3(t.scale[0], t.scale[1], t.scale[2]);
    convert(out, t2);
}

bool mm_begin_pass(MeshManager_t mm, arena_t &temp, const topo_camera &camera, MMPass **out) {
    MMPass *pass;
    temp.pushElements(1, &pass);
    pass->instance = mm->topo;
    pass->camera = camera;

    for (int i = 0; i < kTopoMaterialKind_Count; i++) {
        pass->batches[i] = nullptr;
    }

    *out = pass;
    return true;
}

bool mm_draw(MMPass *pass, arena_t &temp, topo_mesh_handle mesh, const Transform &transform) {
    return InsertIntoDrawPass(pass, temp, mesh, transform);
}

bool mm_end_pass(
    MMPass *pass,
    arena_t &temp,
    VkCommandBuffer cmdBuf,
    const VkRenderPassBeginInfo &renderPassInfo) {
    auto &dev = pass->instance->device;
    auto sm = dev.shaderManager;
    auto bm = dev.bufferManager;
    auto tm = dev.textureManager;
    auto idxCurrentFrame = dev.currentSwapchain.idxCurrentFrame;

    // Update uniforms
    vkTryBeginDebugUtilsLabelTOPO(cmdBuf, "Update uniforms");
    PreparePerDrawUniformsForPass(pass);
    vkTryEndDebugUtilsLabelTOPO(cmdBuf);

    vkCmdBeginRenderPass(cmdBuf, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (int k = 0; k < kTopoMaterialKind_Count; k++) {
        auto *cur = pass->batches[k];

        if (cur == nullptr || cur->numDraws == 0) {
            continue;
        }

        auto *mesh0 = cur->meshes[0];
        assert(cur->submeshIndices[0] < mesh0->numBuffers);
        auto &submesh0 = mesh0->subMeshes[cur->submeshIndices[0]];

        ShaderPipeline pipeline0;
        if (!sm_get_pipeline(sm, &pipeline0, submesh0.namePipeline)) {
            assert(!"invalid pipeline id");
            return false;
        }

        BindPerPassUniform(pass, topo_material_kind(k), submesh0.namePipeline, pipeline0, cmdBuf);

        while (cur != nullptr) {
            // Bind the per-draw uniform buffer of this batch
            VkDescriptorSet hPerDrawSet;
            VkBuffer hPerDrawUniforms;
            assert(cur->numDraws != 0);

            bm_get_uniform_buffer_handle(dev.bufferManager, cur->perDrawUniforms, &hPerDrawUniforms);

            auto res = dev.descriptorPoolLists[dev.currentSwapchain.idxCurrentFrame].allocate(
                dev.handle, 1, &pipeline0.descriptorSetLayout[1], &hPerDrawSet);
            assert(res == VK_SUCCESS);

            VkWriteDescriptorSet writes[1] = { {} };
            writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            writes[0].dstSet = hPerDrawSet;
            writes[0].dstBinding = 0;
            writes[0].descriptorCount = 1;
            writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;

            VkDescriptorBufferInfo descBufInfo = {};
            descBufInfo.offset = 0;
            descBufInfo.range = cur->perDrawUniformsStride;
            descBufInfo.buffer = hPerDrawUniforms;
            writes[0].pBufferInfo = &descBufInfo;
            vkUpdateDescriptorSets(dev.handle, 1, &writes[0], 0, nullptr);

            uint32_t dynamicOffsets[1] = { 0 };
            vkCmdBindDescriptorSets(
                cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline0.layout, 1, 1, &hPerDrawSet, 1,
                dynamicOffsets);

            for (uint32_t idxDraw = 0; idxDraw < cur->numDraws; idxDraw++) {
                auto *mesh = cur->meshes[idxDraw];
                auto idxSubmesh = cur->submeshIndices[idxDraw];
                assert(idxSubmesh < mesh->numBuffers);
                auto &submesh = mesh->subMeshes[idxSubmesh];
                ShaderPipeline pipeline;
                if (!sm_get_pipeline(sm, &pipeline, submesh.namePipeline)) {
                    assert(!"invalid pipeline id");
                    continue;
                }

				if (pass->currentBoundPipeline != pipeline.pipeline) {
					vkCmdBindPipeline(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);
					pass->currentBoundPipeline = pipeline.pipeline;
				}

				auto hMaterial = mesh->subMeshes[idxSubmesh].hMaterial;
				auto &material = dev.materials.entries[(size_t)hMaterial];
				assert(material.used);
				VkResult res;

				{
					// Update the per-draw uniform binding

					uint32_t dynamicOffsets[1] = { idxDraw * cur->perDrawUniformsStride };
					vkCmdBindDescriptorSets(
						cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 1, 1, &hPerDrawSet, 1,
						dynamicOffsets);
				}

				BindPerMaterialUniforms(pass, pipeline, material, cmdBuf);

				VkBuffer *hVertexBuffers = nullptr;
				temp.pushElements(mesh->numBuffers, &hVertexBuffers);
				for (uint32_t idxBuf = 0; idxBuf < mesh->numBuffers; idxBuf++) {
					bm_get_vertex_buffer_handle(bm, mesh->buffers[idxBuf], &hVertexBuffers[idxBuf]);
					assert(hVertexBuffers[idxBuf] != VK_NULL_HANDLE);
				}

				VkBuffer hIndexBuffer;
				bm_get_index_buffer_handle(bm, submesh.indexBuffer, &hIndexBuffer);

				VkDeviceSize *pOffsets = nullptr;
				temp.pushElements(mesh->numBuffers, &pOffsets);
				vkCmdBindVertexBuffers(cmdBuf, 0, mesh->numBuffers, hVertexBuffers, pOffsets);
				vkCmdBindIndexBuffer(cmdBuf, hIndexBuffer, 0, VK_INDEX_TYPE_UINT16);
				vkCmdDrawIndexed(cmdBuf, submesh.numVertices, 1, 0, 0, 0);
            }
            cur = cur->next;
        }
    }

    vkCmdEndRenderPass(cmdBuf);

    return true;
}
