#pragma once

#include <stddef.h>
#include <stdint.h>

typedef struct alloc *alloc_t;
typedef uint32_t alloc_handle;

bool alloc_create(alloc_t *out_handle, size_t sizBlock);
bool alloc_destroy(alloc_t *handle);
bool alloc_tag(alloc_t handle, const char *tag);

bool alloc_would_fit(alloc_t alloc, size_t size, size_t alignment, bool *out);
bool alloc_allocate(alloc_t alloc, size_t size, size_t alignment, alloc_handle *out_handle);
bool alloc_free(alloc_t alloc, alloc_handle handle);
bool alloc_get_offset(alloc_t alloc, alloc_handle allocation, uint32_t *out_offset);
bool alloc_get_metrics(alloc_t alloc, size_t *sizTotal, size_t *sizAllocated, size_t *sizFree, float *fragmentation);