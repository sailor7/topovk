#include <new>
#include <unordered_map>

#include "list.h"
#include "vk_buffers.h"
#include "vk_debug.h"
#include "vk_instance.h"
#include "vk_textures.h"

#include <tracy/Tracy.hpp>

#define TAG "[topo::textures] "

struct ImageObject {
    BmImage hImage;
    VkImageLayout currentLayout;
};

struct SamplerObject {
    VkSampler hSampler = VK_NULL_HANDLE;
    VkImageView hView = VK_NULL_HANDLE;

    bool released = false;
    uint32_t idxFrameLastUsed = 0;
};

struct TextureManager {
    struct topo_instance *topo = nullptr;
    XXH64_hash_t hashSeed = 0;

    float maxAnisotropy = 0;
    bool samplerWasReleased = false;

    std::unordered_map<Texture, ImageObject> images;
    std::unordered_map<Texture, SamplerObject> samplers;

    uint32_t nextHandle = 0;
};

bool tm_create(TextureManager_t *out_tm, struct topo_instance *instance) {
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(instance->physicalDevice.handle, &props);

    auto *tm = new TextureManager;
    tm->hashSeed = 0;
    tm->topo = instance;
    tm->maxAnisotropy = props.limits.maxSamplerAnisotropy;

    *out_tm = tm;

    return true;
}

bool tm_destroy(TextureManager_t *handle) {
    assert(handle && *handle);
    auto *tm = *handle;

    VkDevice device = tm->topo->device.handle;

    for (auto& [hTexture, sampler] : tm->samplers) {
        vkDestroyImageView(device, sampler.hView, nullptr);
        vkDestroySampler(device, sampler.hSampler, nullptr);
    }

    for (auto& [hTexture, image] : tm->images) {
        bm_release_image(tm->topo->device.bufferManager, image.hImage);
    }

    delete tm;
    *handle = nullptr;

    return true;
}

static VkFilter convertEnum(TextureFilter filter) {
    switch (filter) {
    case TextureFilter::POINT:
        return VK_FILTER_NEAREST;
    case TextureFilter::LINEAR:
        return VK_FILTER_LINEAR;
    default:
        return VK_FILTER_NEAREST;
    }
}

static VkSamplerAddressMode convertEnum(TextureAddressMode mode) {
    switch (mode) {
    case TextureAddressMode::REPEAT:
        return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    default:
        return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    }
}

static VkResult create_sampler_from_info(TextureManager_t tm, const SamplerInfo& samplerInfo, VkSampler& out) {
    VkSamplerCreateInfo samplerCreateInfo {};
    samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCreateInfo.magFilter = convertEnum(samplerInfo.filterMag);
    samplerCreateInfo.minFilter = convertEnum(samplerInfo.filterMin);
    samplerCreateInfo.addressModeU = convertEnum(samplerInfo.addressModeU);
    samplerCreateInfo.addressModeV = convertEnum(samplerInfo.addressModeV);
    samplerCreateInfo.addressModeW = convertEnum(samplerInfo.addressModeW);
    samplerCreateInfo.anisotropyEnable = samplerInfo.anisotropy == TextureAnisotropy::ON ? VK_TRUE : VK_FALSE;
    samplerCreateInfo.maxAnisotropy = tm->maxAnisotropy;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
    samplerCreateInfo.compareEnable = VK_FALSE;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.mipLodBias = 0.0f;
    samplerCreateInfo.minLod = 0.0f;
    samplerCreateInfo.maxLod = 0.0f;

    return vkCreateSampler(tm->topo->device.handle, &samplerCreateInfo, nullptr, &out);
}

template <typename T> static bool get_new_node(T **out, arena_t &arena, T **head, T **headUnused) {
    assert(out);
    assert(head);
    assert(headUnused);
    T *node = *headUnused;
    if (node) {
        *headUnused = node->next;
    } else {
        arena.pushElements(1, &node);
        new (node) T {};
    }

    node->next = *head;
    *head = node;
    *out = node;

    assert(arena.buffer <= (uint8_t*)node && (uint8_t*)node < arena.buffer + arena.lenSize);
    assert(node->next == nullptr || (arena.buffer <= (uint8_t*)node->next && (uint8_t*)node->next < arena.buffer + arena.lenSize));

    return true;
}

bool tm_make(
    TextureManager_t tm,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const void *data,
    const SamplerInfo &samplerInfo,
    Texture *out_handle) {

    // Create new image buffer
    ImageInfo imageInfo = {};
    imageInfo.width = width;
    imageInfo.height = height;
    imageInfo.depth = 1;
    imageInfo.format = format;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    BmImage hImageBuffer;
    if (!bm_make_image(tm->topo->device.bufferManager, &imageInfo, width * height * 4, data, &hImageBuffer)) {
        return false;
    }

    auto hTextureHandle = tm->nextHandle++;
    // Create new image node
    ImageObject objImage;
    objImage.currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    objImage.hImage = hImageBuffer;
    tm->images[hTextureHandle] = objImage;

    VkImage hImage;
    if (!bm_get_image_handle(tm->topo->device.bufferManager, hImageBuffer, &hImage)) {
        return false;
    }

    VkImageViewCreateInfo viewInfo {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = hImage;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    if (vkCreateImageView(tm->topo->device.handle, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
        return false;
    }

    VkSampler hSampler;
    if (create_sampler_from_info(tm, samplerInfo, hSampler) != VK_SUCCESS) {
        vkDestroyImageView(tm->topo->device.handle, imageView, nullptr);
        return false;
    }

    SamplerObject objSampler;
    objSampler.hView = imageView;
    objSampler.hSampler = hSampler;
    tm->samplers[hTextureHandle] = objSampler;

    *out_handle = hTextureHandle;

    return true;
}


bool tm_make(
    TextureManager_t tm,
    const char *,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const void *data,
    const SamplerInfo &samplerInfo,
    Texture *out_handle) {
    return tm_make(tm, width, height, format, data, samplerInfo, out_handle);
}

static uint8_t get_pixel_size(VkFormat format) {
    switch (format) {
    case VK_FORMAT_R32G32B32A32_SFLOAT:
        return 16;
    case VK_FORMAT_R8G8B8A8_SRGB:
    case VK_FORMAT_R8G8B8A8_UINT:
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SINT:
    case VK_FORMAT_R8G8B8A8_SNORM:
    case VK_FORMAT_B8G8R8A8_SRGB:
    case VK_FORMAT_B8G8R8A8_UINT:
    case VK_FORMAT_B8G8R8A8_SINT:
        return 4;
    case VK_FORMAT_B8G8R8_SRGB:
    case VK_FORMAT_B8G8R8_UINT:
    case VK_FORMAT_B8G8R8_SINT:
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_B8G8R8_SNORM:
        return 3;
    case VK_FORMAT_R16G16_UINT:
    case VK_FORMAT_R16G16_SINT:
    case VK_FORMAT_R16G16_UNORM:
    case VK_FORMAT_R16G16_SNORM:
        return 2;
    case VK_FORMAT_R32_UINT:
    case VK_FORMAT_R32_SINT:
        return 1;
    default:
        assert(!"Unhandled pixel format");
        return 4;
    }
}

bool tm_make_color_buffer(
    TextureManager_t tm,
    const char *id,
    uint32_t width,
    uint32_t height,
    VkFormat format,
    const SamplerInfo &samplerInfo,
    Texture *out_handle) {
    auto bm = tm->topo->device.bufferManager;

    auto hTextureHandle = tm->nextHandle++;

	ImageInfo imageInfo = {};
	imageInfo.width = width;
	imageInfo.height = height;
	imageInfo.depth = 1;
	imageInfo.format = format;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	BmImage hImageBuffer;
	auto sizPixel = get_pixel_size(format);
	auto sizBuffer = width * height * sizPixel;
	auto usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	if (!bm_make_image(bm, &imageInfo, sizBuffer, usage, &hImageBuffer)) {
		return false;
	}

	ImageObject objImage;
	objImage.hImage = hImageBuffer;
	objImage.currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	tm->images[hTextureHandle] = objImage;

    VkImage hImage;
    if (!bm_get_image_handle(bm, hImageBuffer, &hImage)) {
        return false;
    }

    VkImageViewCreateInfo viewInfo {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = hImage;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    if (vkCreateImageView(tm->topo->device.handle, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
        return false;
    }

    VkSampler hSampler;
    if (create_sampler_from_info(tm, samplerInfo, hSampler) != VK_SUCCESS) {
        vkDestroyImageView(tm->topo->device.handle, imageView, nullptr);
        return false;
    }

    SamplerObject objSampler;
    objSampler.hSampler = hSampler;
    objSampler.hView = imageView;
    tm->samplers[hTextureHandle] = objSampler;

    *out_handle = hTextureHandle;

    return true;
}

bool tm_make_depth_buffer(
    TextureManager_t tm,
    const char* id,
    uint32_t width,
    uint32_t height,
    const SamplerInfo &samplerInfo,
    Texture *out_handle) {
    auto hTexture = tm->nextHandle++;

    ImageInfo imageInfo = {};
    imageInfo.width = width;
    imageInfo.height = height;
    imageInfo.depth = 1;
    imageInfo.format = VK_FORMAT_D32_SFLOAT;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    BmImage hImageBuffer;
    if (!bm_make_image(tm->topo->device.bufferManager, &imageInfo, width * height * 4, &hImageBuffer)) {
        return false;
    }

    ImageObject objImage;
    objImage.hImage = hImageBuffer;
    objImage.currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    tm->images[hTexture] = objImage;

    VkImage hImage;
    if (!bm_get_image_handle(tm->topo->device.bufferManager, hImageBuffer, &hImage)) {
        return false;
    }

    VkImageViewCreateInfo viewInfo {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = hImage;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = VK_FORMAT_D32_SFLOAT;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    if (vkCreateImageView(tm->topo->device.handle, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
        return false;
    }

    SamplerObject objSampler;
    objSampler.hSampler = VK_NULL_HANDLE;
    objSampler.hView = imageView;
    tm->samplers[hTexture] = objSampler;

    *out_handle = hTexture;

    return true;
}

bool tm_free(TextureManager_t tm, Texture handle) {
    auto device = tm->topo->device.handle;

    if (tm->samplers.count(handle) != 0) {
        auto &objSampler = tm->samplers.at(handle);

		vkDestroySampler(device, objSampler.hSampler, nullptr);
		vkDestroyImageView(device, objSampler.hView, nullptr);
        tm->samplers.erase(handle);
    }

    if (tm->images.count(handle) != 0) {
        auto &objImage = tm->images.at(handle);

        bm_release_image(tm->topo->device.bufferManager, objImage.hImage);
        tm->images.erase(handle);
    }

    return false;
}

bool tm_release(TextureManager_t tm, Texture handle) {
    if (tm->samplers.count(handle) == 0) {
        return false;
    }

    auto &objSampler = tm->samplers.at(handle);
    objSampler.released = true;

    tm->samplerWasReleased = true;

    return true;
}

bool tm_get_view_handles(
    TextureManager_t tm,
    uint32_t numHandles,
    const Texture *handles,
    VkImageView *out_views) {
    auto idxCurrentFrame = tm->topo->device.currentSwapchain.idxCurrentFrame;

    for (uint32_t idxHandle = 0; idxHandle < numHandles; idxHandle++) {
        if (tm->samplers.count(handles[idxHandle]) == 0) {
            return false;
        }

        auto &objSampler = tm->samplers.at(handles[idxHandle]);
        out_views[idxHandle] = objSampler.hView;
        objSampler.idxFrameLastUsed = idxCurrentFrame;
    }

    return true;
}

bool tm_transition_layout(
    TextureManager_t tm,
    Texture handle,
    VkImageLayout nextLayout,
    VkCommandBuffer cmdBuffer) {
    if (tm->images.count(handle) == 0) {
        return false;
    }

    auto &objImage = tm->images.at(handle);

    VkPipelineStageFlags srcStageFlags, dstStageFlags;
    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.pNext = nullptr;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = 0;
    barrier.oldLayout = objImage.currentLayout;
    barrier.newLayout = nextLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    VkImage hImage;
    bm_get_image_handle(tm->topo->device.bufferManager, objImage.hImage, &hImage);
    barrier.image = hImage;

    if (objImage.currentLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        && nextLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        srcStageFlags = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dstStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    } else if (
        objImage.currentLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        && nextLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        srcStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dstStageFlags = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else if (
        objImage.currentLayout == VK_IMAGE_LAYOUT_UNDEFINED
        && nextLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        srcStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dstStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    } else {
        assert(0);
    }

    vkCmdPipelineBarrier(cmdBuffer, srcStageFlags, dstStageFlags, 0, 0, nullptr, 0, nullptr, 1, &barrier);
    objImage.currentLayout = nextLayout;
    return true;
}

bool tm_get_texture_handles(
    TextureManager_t tm,
    uint32_t numTextures,
    const Texture *pHandles,
    VkImageView *out_views,
    VkSampler *out_samplers) {
    ZoneScoped;
    assert(tm);
    if (numTextures == 0) {
        return true;
    }

    assert(pHandles && out_views && out_samplers);

    auto idxCurrentFrame = tm->topo->device.currentSwapchain.idxCurrentFrame;

    for (uint32_t idxTexture = 0; idxTexture < numTextures; idxTexture++) {
        auto hTexture = pHandles[idxTexture];
        if (tm->samplers.count(hTexture) == 0) {
            return false;
        }

        auto &sampler = tm->samplers.at(hTexture);
        sampler.idxFrameLastUsed = idxCurrentFrame;
        out_views[idxTexture] = sampler.hView;
        out_samplers[idxTexture] = sampler.hSampler;
    }

    return true;
}

bool tm_get_sampler_handle(
    TextureManager_t tm,
    Texture handle,
    VkImageView *out_view,
    VkSampler *out_sampler) {
    return tm_get_texture_handles(tm, 1, &handle, out_view, out_sampler);
}

bool tm_free_released_resources(TextureManager_t tm, arena_t &arenaTemp) {
    ZoneScoped;

    if (!tm->samplerWasReleased) {
        return true;
    }

    auto idxCurrentFrame = tm->topo->device.currentSwapchain.idxCurrentFrame;
    auto shutdown = tm->topo->shutdown;

    auto *pGraveyard = arenaTemp.getCursor<Texture>();
    uint32_t lenGraveyard = 0;

    bool anyReleasedSamplers = false;

    for (auto &[hTexture, sampler] : tm->samplers) {
        bool released = sampler.released;
        if (shutdown || (released && sampler.idxFrameLastUsed == idxCurrentFrame)) {
            Texture *pItem;
            arenaTemp.pushElements(1, &pItem);
            *pItem = hTexture;
            lenGraveyard++;
        } else {
            anyReleasedSamplers |= released;
        }
    }

    for (uint32_t idxItem = 0; idxItem < lenGraveyard; idxItem++) { 
        tm_free(tm, pGraveyard[idxItem]);
        tm->samplers.erase(pGraveyard[idxItem]);
        tm->images.erase(pGraveyard[idxItem]);
    }

    if (!anyReleasedSamplers) {
        tm->samplerWasReleased = false;
    }

    return true;
}
