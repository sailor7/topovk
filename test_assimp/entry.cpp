#include <thread>
#include <topo/topo.h>

#include <filesystem>
#include <glm/gtc/quaternion.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <utils/scene_assimp.h>

static void check_working_dir() {
    bool hasBin = false;
    bool hasAssets = false;
    auto cwd = std::filesystem::current_path();

    while (!hasBin || !hasAssets) {
        hasBin = false;
        hasAssets = false;
        for (const auto entry : std::filesystem::directory_iterator(cwd)) {
            if (entry.is_directory()) {
                if (entry.path().filename() == "bin") {
                    hasBin = true;
                }
                if (entry.path().filename() == "assets") {
                    hasAssets = true;
                }
            }
        }

        if (hasBin && hasAssets) {
            break;
        }

        auto parentPath = cwd.parent_path();
        if (cwd == parentPath) {
            fprintf(stderr, "Bad working directory\n");
            abort();
        }

        cwd = parentPath;
    }

    std::filesystem::current_path(cwd);
}

static TopoImage loadTextureFromFile(TopoInstance renderer, const char *path) {
    int lenWidth, lenHeight, numChannels;
    auto *image = stbi_load(path, &lenWidth, &lenHeight, &numChannels, 4);
    TopoImageDescriptor desc;
    desc.format = kTopo_FormatRGBA32_SRGB;
    desc.width = lenWidth;
    desc.height = lenHeight;
    desc.id = path;
    topo_borrow_memory_ex(renderer, lenWidth * lenHeight * 4, (uint8_t **)&desc.pData, image);
    TopoImage handle;
    topo_create_image(renderer, &desc, &handle);
    stbi_image_free(image);
    return handle;
}

static TopoImage loadDefaultTexture(TopoInstance renderer) {
    return loadTextureFromFile(renderer, "assets/thumbs-up-cat.png");
}

static void makeLoadingScene(TopoInstance renderer, TopoScene &out) { 
    TopoImage hTexLoading;
    TopoMaterial hMaterial;
    TopoMesh hMesh;

    hTexLoading = loadTextureFromFile(renderer, "assets/loading.bmp");
    TopoMaterialDescriptor descMat;
    descMat.id = "loading";
    descMat.kind = TopoMK_TextureColor;
    descMat.unlitTextured.imageBaseColor = hTexLoading;

    topo_create_material(renderer, &descMat, &hMaterial);

    const glm::vec3 position[]
        = { { -1.0f, -1.0f, 0.0f }, { 1.0f, -1.0f, 0.0f }, { -1.0f, 1.0f, 0.0f }, { 1.0f, 1.0f, 0.0f } };
    const glm::vec3 tangent[] = { { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 } };
    const glm::vec3 bitangent[] = { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } };
    const glm::vec3 normal[] = { { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 } };
    const glm::vec2 texcoord[] = { { 0.0f, 1.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1.0f, 0.0f } };
    const uint16_t index[] = { 0, 2, 1, 2, 3, 1 };

    TopoMeshDescriptor *descMesh;
    TopoVertexBuffer *buffers;
    TopoVertexAttribute *attributes;
    TopoSubmeshDescriptor *submeshes;
    glm::vec3 *bufPosition;
    glm::vec3 *bufTangent, *bufBitangent, *bufNormal;
    glm::vec3 *bufTexcoord;
    uint16_t *bufIndex;
    const char *id;

    topo_borrow_memory_ex(renderer, &descMesh);

    topo_borrow_memory_ex(renderer, &id, "loading");
    descMesh->id = id;

    // Buffers

    topo_borrow_memory_ex(renderer, 5, &buffers);
    descMesh->numVertexBuffers = 5;
    descMesh->pVertexBuffers = buffers;

    topo_borrow_memory_ex(renderer, 4, &bufPosition, position);
    buffers[0].pVertexData = bufPosition;
    buffers[0].sizVertexData = sizeof(position);
    buffers[0].sizVertexStride = sizeof(glm::vec3);

    topo_borrow_memory_ex(renderer, 4, &bufTexcoord, texcoord);
    buffers[1].pVertexData = bufTexcoord;
    buffers[1].sizVertexData = sizeof(texcoord);
    buffers[1].sizVertexStride = sizeof(glm::vec2);

    topo_borrow_memory_ex(renderer, 4, &bufTangent, tangent);
    buffers[2].pVertexData = bufTangent;
    buffers[2].sizVertexData = sizeof(tangent);
    buffers[2].sizVertexStride = sizeof(glm::vec3);

    topo_borrow_memory_ex(renderer, 4, &bufBitangent, bitangent);
    buffers[3].pVertexData = bufBitangent;
    buffers[3].sizVertexData = sizeof(bitangent);
    buffers[3].sizVertexStride = sizeof(glm::vec3);

    topo_borrow_memory_ex(renderer, 4, &bufNormal, normal);
    buffers[4].pVertexData = bufNormal;
    buffers[4].sizVertexData = sizeof(normal);
    buffers[4].sizVertexStride = sizeof(glm::vec3);

    // Attributes

    topo_borrow_memory_ex(renderer, 5, &attributes);
    descMesh->numAttributes = 5;
    descMesh->pAttributes = attributes;

    attributes[0].idxVertexBuffer = 0;
    attributes[0].offset = 0;
    attributes[0].semantic = TopoVES_Position;
    attributes[0].type = TopoVET_F32x3;

    attributes[1].idxVertexBuffer = 1;
    attributes[1].offset = 0;
    attributes[1].semantic = TopoVES_Texcoord;
    attributes[1].type = TopoVET_F32x2;

    attributes[2].idxVertexBuffer = 2;
    attributes[2].offset = 0;
    attributes[2].semantic = TopoVES_Tangent;
    attributes[2].type = TopoVET_F32x3;

    attributes[3].idxVertexBuffer = 3;
    attributes[3].offset = 0;
    attributes[3].semantic = TopoVES_Bitangent;
    attributes[3].type = TopoVET_F32x3;

    attributes[4].idxVertexBuffer = 4;
    attributes[4].offset = 0;
    attributes[4].semantic = TopoVES_Normal;
    attributes[4].type = TopoVET_F32x3;

    // Submeshes
    topo_borrow_memory_ex(renderer, 1, &submeshes);
    descMesh->numSubmeshes = 1;
    descMesh->pSubmeshes = submeshes;

    topo_borrow_memory_ex(renderer, 6, &bufIndex, index);
    submeshes[0].isIndexed = true;
    submeshes[0].material = hMaterial;
    submeshes[0].primitiveTopology = TopoPT_TriangleList;
    submeshes[0].indexBuffer.numIndices = 6;
    submeshes[0].indexBuffer.pIndices = bufIndex;
    submeshes[0].indexBuffer.type = TopoIndexBuffer::UINT16;

    topo_create_mesh(renderer, descMesh, &hMesh);

    auto *meshes = new TopoMesh[1];
    meshes[0] = hMesh;

    auto *transforms = new TopoTransform[1];
    memset(&transforms[0], 0, sizeof(transforms[0]));
    transforms[0].position[2] = 3.0f;
    transforms[0].rotation[0] = 1.0f;
    transforms[0].scale[0] = 1.0f;
    transforms[0].scale[1] = 1.0f;
    transforms[0].scale[2] = 1.0f;

    out.pNext = nullptr;

    out.numObjects = 1;
    out.meshes = meshes;
    out.transforms = transforms;

    // Setup camera
    out.mainCamera.farClip = 16.0f;
    out.mainCamera.nearClip = 0.01f;
    out.mainCamera.fieldOfView = glm::radians(90.0f);
    memset(&out.mainCamera.transform, 0, sizeof(out.mainCamera.transform));
    out.mainCamera.transform.rotation[0] = 1.0f;
    out.mainCamera.transform.scale[0] = 1.0f;
    out.mainCamera.transform.scale[1] = 0.5f;
    out.mainCamera.transform.scale[2] = 1.0f;

    out.numLights = 1;
    auto *lights = new TopoLightSource[1];
    lights[0].kind = TopoLSK_Ambient;
    lights[0].color[0] = 1;
    lights[0].color[1] = 1;
    lights[0].color[2] = 1;
    lights[0].intensity = 1;
    out.lights = lights;
}

int main(int argc, char **argv) {
    check_working_dir();

    TopoInstance topoInstance = nullptr;
    TopoInstanceDescriptor topoDescriptor = {
        "test_assimp",
        1280,
        720,
        1,
    };

    topo_create(&topoInstance, &topoDescriptor);

    TopoImage hDefaultTexture = loadDefaultTexture(topoInstance);
    TopoScene sceneLoading;
    makeLoadingScene(topoInstance, sceneLoading);
    const TopoScene *outScene = nullptr;
    topo_set_scene(topoInstance, &sceneLoading, &outScene);

    const char *pInputPath = "assets/cube.glb";
    if (argc > 1) {
        pInputPath = argv[1];
    }

    utils::Objects objects;
    utils::Resources resources;
    utils::Renderer renderer;
    renderer.hDefaultTexture = hDefaultTexture;
    renderer.instance = topoInstance;

    printf("Loading scene from '%s'\n", pInputPath);
    if (utils::loadSceneFromFile(pInputPath, renderer, resources, objects) != kTopo_OK) {
        printf("Failed to load scene!\n");
        topo_destroy(&topoInstance);
        return -1;
    }

    TopoScene tScene = {};
    tScene.meshes = objects.meshes.data();
    tScene.transforms = objects.transforms.data();
    tScene.numObjects = objects.meshes.size();
    tScene.mainCamera = resources.cameras[0];
    tScene.lights = objects.lights.data();
    tScene.numLights = objects.lights.size();

    topo_set_scene(topoInstance, &tScene, &outScene);

    float turns = 0.0f;
    bool exiting = false;
    while (!exiting) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        auto &camRot = tScene.mainCamera.transform.rotation;
        auto camRotQuat = glm::quat(camRot[0], camRot[1], camRot[2], camRot[3]);
        auto addRot = glm::angleAxis(10 / 1000.0f, glm::vec3(0.0f, 1.0f, 0.0f));
        camRotQuat = camRotQuat * addRot;
        tScene.mainCamera.transform.rotation[0] = camRotQuat.w;
        tScene.mainCamera.transform.rotation[1] = camRotQuat.x;
        tScene.mainCamera.transform.rotation[2] = camRotQuat.y;
        tScene.mainCamera.transform.rotation[3] = camRotQuat.z;
        topo_exiting(topoInstance, &exiting);
    }

    topo_set_scene(topoInstance, nullptr, &outScene);

    for (uint32_t idxMesh = 0; idxMesh < objects.meshes.size(); idxMesh++) {
        topo_destroy_mesh(topoInstance, &objects.meshes[idxMesh]);
    }

    topo_destroy(&topoInstance);
    return 0;
}