add_executable(test_assimp entry.cpp)
target_link_libraries(test_assimp PUBLIC topo::main topo::utils)
set_property(TARGET test_assimp PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${TOPO_ROOT}")