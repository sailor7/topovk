#include <utils/scene_dyn.h>

#include <cassert>
#include <unordered_map>

#include <tracy/Tracy.hpp>

struct ResourceReleaseQueue {
    std::vector<TopoMesh> meshes;
    std::vector<TopoMaterial> materials;
    std::vector<TopoImage> images;
};

struct ObjectGroupBacking {
    bool isClosed = false;

    std::vector<TopoMesh> meshes;
    std::vector<TopoTransform> transforms;
    std::vector<TopoLightSource> lights;
};

struct SceneBacking {
    ResourceReleaseQueue releaseQueue;

    std::unique_ptr<TopoScene> scene;
    std::unique_ptr<TopoScene2> sceneExt;
    std::vector<TopoCamera> cameras;

    std::unordered_map<uint32_t, ObjectGroupBacking> objectGroupBacking;
    std::vector<TopoObjectGroup> objectGroups;
};

class ImmediateSceneImpl : public IImmediateScene {
public:
    ~ImmediateSceneImpl() override { 
        const TopoScene *pPrev = nullptr;
        topoSetSceneAndWait(_renderer, nullptr, &pPrev);

        while (_scenes.size() > 0) {
            eraseScene(_scenes.begin()->first);
        }
    }

    ImmediateSceneImpl(TopoInstance hRenderer)
        : _renderer(hRenderer) {
    }

    bool beginScene() override {
        ZoneScoped;
        assert(_buildingScene == nullptr);

        if (_scenes.size() > 4) {
            const TopoScene *prev = nullptr;
            if (topoPollOutcomingScene(_renderer, &prev) == Topo_OK) {
                eraseScene(prev);
            }

            if (_scenes.size() > 4) {
                return false;
            }
        }

        SceneBacking sceneBacking = {};
        sceneBacking.scene = std::make_unique<TopoScene>();
        sceneBacking.sceneExt = std::make_unique<TopoScene2>();

        _buildingScene = sceneBacking.scene.get();
        _buildingScene->numObjects = 0;
        _buildingScene->numLights = 0;
        _buildingScene->pNext = sceneBacking.sceneExt.get();

        _scenes[_buildingScene] = std::move(sceneBacking);

        return true;
    }

    void submitScene() override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes[_buildingScene];
        sceneBacking.sceneExt->numObjectGroups = sceneBacking.objectGroups.size();
        sceneBacking.sceneExt->pObjectGroups = sceneBacking.objectGroups.data();

        sceneBacking.sceneExt->numCameras = sceneBacking.cameras.size();
        sceneBacking.sceneExt->pCameras = sceneBacking.cameras.data();

        const TopoScene *prev = nullptr;
        topo_set_scene(_renderer, _buildingScene, &prev);
        _frontScene = _buildingScene;
        _buildingScene = nullptr;

        if (prev != nullptr && _scenes.count(prev) != 0) {
            eraseScene(prev);
        }
    }

    void eraseScene(const TopoScene *prev) {
        ZoneScoped;
        auto &sceneBacking = _scenes[prev];

        for (auto mesh : sceneBacking.releaseQueue.meshes) {
            topo_destroy_mesh(_renderer, &mesh);
        }

        for (auto material : sceneBacking.releaseQueue.materials) {
            topo_destroy_material(_renderer, &material);
        }

        for (auto image : sceneBacking.releaseQueue.images) {
            topo_destroy_image(_renderer, &image);
        }

        _scenes.erase(prev);
    }

    uint32_t beginObjectGroup() override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);
        auto idxBacking = sceneBacking.objectGroups.size();
        sceneBacking.objectGroups.push_back({});
        auto &objectGroup = sceneBacking.objectGroups.back();
        sceneBacking.objectGroupBacking[idxBacking] = {};

        return idxBacking;
    }

    void addToObjectGroup(TopoMesh hMesh, const TopoTransform &transform) override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);
        assert(sceneBacking.objectGroups.size() != 0);
        auto idxGroup = sceneBacking.objectGroups.size() - 1;
        auto &objectGroupBacking = sceneBacking.objectGroupBacking.at(idxGroup);

        assert(!objectGroupBacking.isClosed);

        objectGroupBacking.meshes.push_back(hMesh);
        objectGroupBacking.transforms.push_back(transform);
    }

    void addToObjectGroup(const TopoLightSource &lightSource) override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);
        assert(sceneBacking.objectGroups.size() != 0);
        auto idxGroup = sceneBacking.objectGroups.size() - 1;
        auto &objectGroupBacking = sceneBacking.objectGroupBacking.at(idxGroup);

        assert(!objectGroupBacking.isClosed);

        objectGroupBacking.lights.push_back(lightSource);
    }

    void endObjectGroup() override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);
        assert(sceneBacking.objectGroups.size() != 0);
        auto idxGroup = sceneBacking.objectGroups.size() - 1;
        auto &objectGroup = sceneBacking.objectGroups.back();
        auto &objectGroupBacking = sceneBacking.objectGroupBacking.at(idxGroup);

        objectGroup.numLights = objectGroupBacking.lights.size();
        objectGroup.pLights = objectGroupBacking.lights.data();

        assert(objectGroupBacking.meshes.size() == objectGroupBacking.transforms.size());
        objectGroup.numObjects = objectGroupBacking.meshes.size();
        objectGroup.pMeshes = objectGroupBacking.meshes.data();
        objectGroup.pTransforms = objectGroupBacking.transforms.data();

        objectGroupBacking.isClosed = true;
    }

    uint32_t addCamera(const TopoCamera &camera) override {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);

        auto ret = uint32_t(sceneBacking.cameras.size());
        sceneBacking.cameras.push_back(camera);

        return ret;
    }

    void setMainCamera(uint32_t idxCamera) {
        ZoneScoped;
        assert(_buildingScene != nullptr);

        auto &sceneBacking = _scenes.at(_buildingScene);
        assert(idxCamera < sceneBacking.cameras.size());
        sceneBacking.sceneExt->idxMainCamera = idxCamera;
    }

    void release(uint32_t numHandles, const TopoMesh *pMeshes) override {
        ZoneScoped;
        auto &sceneBacking = _scenes.at(_frontScene);

        for (uint32_t idxHandle = 0; idxHandle < numHandles; idxHandle++) {
            sceneBacking.releaseQueue.meshes.push_back(pMeshes[idxHandle]);
        }
    }

    void release(uint32_t numHandles, const TopoMaterial *pMaterials) override {
        ZoneScoped;
        auto &sceneBacking = _scenes.at(_frontScene);

        for (uint32_t idxHandle = 0; idxHandle < numHandles; idxHandle++) {
            sceneBacking.releaseQueue.materials.push_back(pMaterials[idxHandle]);
        }
    }

    void release(uint32_t numHandles, const TopoImage *pImages) override {
        ZoneScoped;
        auto &sceneBacking = _scenes.at(_frontScene);

        for (uint32_t idxHandle = 0; idxHandle < numHandles; idxHandle++) {
            sceneBacking.releaseQueue.images.push_back(pImages[idxHandle]);
        }
    }

private:
    TopoInstance _renderer;
    const TopoScene *_frontScene = nullptr;
    TopoScene *_buildingScene = nullptr;
    std::unordered_map<const TopoScene *, SceneBacking> _scenes;
};

UTILS_API std::unique_ptr<IImmediateScene> makeDynScene(TopoInstance renderer) {
    return std::make_unique<ImmediateSceneImpl>(renderer);
}