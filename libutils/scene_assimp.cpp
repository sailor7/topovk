#include <utils/scene_assimp.h>

#include <assimp/GltfMaterial.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include <uuid.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <glm/gtx/matrix_decompose.hpp>

namespace utils {

void convert(topo_transform &out, const aiMatrix4x4 &in) {
    aiVector3D trans, scale;
    aiQuaternion rotation;
    in.Decompose(scale, rotation, trans);

    out.position[0] = trans.x;
    out.position[1] = trans.y;
    out.position[2] = trans.z;

    out.rotation[0] = rotation.w;
    out.rotation[1] = rotation.x;
    out.rotation[2] = rotation.y;
    out.rotation[3] = rotation.z;

    out.scale[0] = scale.x;
    out.scale[1] = scale.y;
    out.scale[2] = scale.z;
}

static void traverseNode(
    const aiMatrix4x4 &parentTransform,
    const std::vector<topo_mesh_handle> meshes,
    const aiNode *node,
    Objects &objects) {
    auto currentTransform = parentTransform * node->mTransformation;

    if (node->mNumMeshes > 0) {
        topo_transform transform;
        convert(transform, currentTransform);

        for (size_t idxMesh = 0; idxMesh < node->mNumMeshes; idxMesh++) {
            auto hMesh = meshes[node->mMeshes[idxMesh]];
            objects.meshes.push_back(hMesh);
            objects.transforms.push_back(transform);
        }
    }

    for (size_t idxChild = 0; idxChild < node->mNumChildren; idxChild++) {
        traverseNode(currentTransform, meshes, node->mChildren[idxChild], objects);
    }
}

static void clipV3(size_t num, size_t numDstComp, void *dst, const void *src) {
    auto *dstf = (float *)dst;
    auto *srcf = (const float *)src;

    size_t idx = 0;

    while (idx < num) {
        dstf[0] = srcf[0];
        dstf[1] = srcf[1];
        dstf += numDstComp;
        srcf += 3;
        idx++;
    }
}

static topo_image_t makeColorTexture(
    topo_instance_t topo,
    uuids::uuid_random_generator &gen,
    float r,
    float g,
    float b,
    std::vector<topo_image_t> &textures) {
    float rgba[4] = { r, g, b, 0.0f };
    auto name = uuids::to_string(gen());

    topo_image_descriptor dImage;
    dImage.width = 1;
    dImage.height = 1;
    dImage.format = kTopo_FormatRGBA32;
    dImage.id = name.c_str();
    topo_borrow_memory_ex(topo, sizeof(rgba), (uint8_t **)&dImage.pData, rgba);

    topo_image_t hImage;
    auto res = topo_create_image(topo, &dImage, &hImage);
    assert(res == kTopo_OK);

    textures.push_back(hImage);

    return hImage;
}

UTILS_API topo_result loadSceneFromFile(const char *pInputPath, const Renderer& renderer, Resources &outResources, Objects& outObjects) {
    std::random_device rd;
    auto seed_data = std::array<int, std::mt19937::state_size> {};
    std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    std::mt19937 generator(seq);
    uuids::uuid_random_generator genUuid { generator };

    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(
        pInputPath,
        aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_ConvertToLeftHanded);

    if (scene == nullptr) {
        return kTopo_Failure;
    }

    auto topoInstance = renderer.instance;
    auto hDefaultTexture = renderer.hDefaultTexture;

    std::vector<topo_image_t> textures(scene->mNumTextures);
    std::vector<topo_material_t> materials(scene->mNumMaterials);
    std::vector<topo_mesh_handle> meshes(scene->mNumMeshes);

    for (size_t idxTex = 0; idxTex < scene->mNumTextures; idxTex++) {
        auto *tex = scene->mTextures[idxTex];

        void *pImageData = tex->pcData;
        topo_image_descriptor dImage;
        if (tex->mHeight == 0) {
            int numWidth, numHeight, numChannels;
            pImageData = stbi_load_from_memory(
                (const stbi_uc *)tex->pcData, tex->mWidth, &numWidth, &numHeight, &numChannels, 4);
            if (!pImageData) {
                fprintf(stderr, "Failed to parse texture #%llu\n", idxTex);
            }
            dImage.width = numWidth;
            dImage.height = numHeight;
        } else {
            dImage.width = tex->mWidth;
            dImage.height = tex->mHeight;
        }

        if (tex->mHeight == 0) {
			dImage.format = kTopo_FormatRGBA32_SRGB;
        } else {
			dImage.format = kTopo_FormatBGRA32_SRGB;
        }

        dImage.id = tex->mFilename.C_Str();
        topo_borrow_memory_ex(
            topoInstance, dImage.width * dImage.height * 4, (uint8_t **)&dImage.pData, pImageData);
        topo_image_t handle;
        topo_create_image(topoInstance, &dImage, &handle);
        textures[idxTex] = handle;

        if (tex->mHeight == 0) {
            stbi_image_free(pImageData);
        }
    }

    auto findTexture = [](const aiString &name, size_t numSceneTextures, aiTexture **sceneTextures,
                          const std::vector<topo_image_t> &textures, topo_image_t &out) -> bool {
        if (name.length > 0 && name.C_Str()[0] == '*') {
            auto *namec = name.C_Str() + 1;
            auto idxTex = std::atoi(namec);
            if (idxTex < textures.size()) {
                out = textures[idxTex];
                return true;
            }
        } else {
            for (size_t idxTex = 0; idxTex < numSceneTextures; idxTex++) {
                auto &tex = sceneTextures[idxTex];
                if (tex->mFilename == name) {
                    assert(idxTex < textures.size());
                    out = textures[idxTex];
                    return true;
                }
            }
        }

        return false;
    };

    for (size_t idxMat = 0; idxMat < scene->mNumMaterials; idxMat++) {
        auto *mat = scene->mMaterials[idxMat];

        topo_material_descriptor testMaterial;
        testMaterial.id = mat->GetName().C_Str();

        aiString texBaseColor, texNormal, texMetallicRoughness;
        aiReturn res;

        bool hasBaseColor = false;
        bool hasNormalMap = false;
        bool hasRoughnessMap = false;

        hasBaseColor = (mat->GetTexture(AI_MATKEY_BASE_COLOR_TEXTURE, &texBaseColor) == aiReturn_SUCCESS);
        hasNormalMap = (mat->GetTexture(aiTextureType_NORMALS, 0, &texNormal) == aiReturn_SUCCESS);
        hasRoughnessMap
            = (mat->GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE, &texMetallicRoughness) == aiReturn_SUCCESS);

        if (hasBaseColor) { 
            topo_image_t hTextureBaseColor = nullptr;
            bool found;

            found = findTexture(
                texBaseColor, scene->mNumTextures, scene->mTextures, textures, hTextureBaseColor);
            assert(found);

            if (hasNormalMap) {
                topo_image_t hTextureNormal = nullptr;
                topo_image_t hTextureMetallicRoughness = nullptr;

                found
                    = findTexture(texNormal, scene->mNumTextures, scene->mTextures, textures, hTextureNormal);
                assert(found);

                if (hasRoughnessMap) {
                    found = findTexture(
                        texMetallicRoughness, scene->mNumTextures, scene->mTextures, textures,
                        hTextureMetallicRoughness);
                    assert(found);
                } else {
                    ai_real metallic = 0.0f;
                    ai_real roughness = 0.5f;
                    mat->Get(AI_MATKEY_METALLIC_FACTOR, metallic);
                    mat->Get(AI_MATKEY_ROUGHNESS_FACTOR, roughness);
                    hTextureMetallicRoughness = makeColorTexture(topoInstance, genUuid, 0.0f, metallic, roughness, textures);
                }

                testMaterial.kind = kTopoMaterialKind_NormalMapped;
                testMaterial.normalMapped.imageBaseColor = hTextureBaseColor;
                testMaterial.normalMapped.imageNormalMap = hTextureNormal;
                testMaterial.normalMapped.imageMetallicRoughness = hTextureMetallicRoughness;
            } else {
                testMaterial.kind = kTopoMaterialKind_UnlitTextured;
                testMaterial.unlitTextured.imageBaseColor = hTextureBaseColor;
            }
        } else {
            testMaterial.kind = kTopoMaterialKind_UnlitColor;
			testMaterial.unlitColor.colorR = 1.0f;
			testMaterial.unlitColor.colorG = 1.0f;
			testMaterial.unlitColor.colorB = 1.0f;
            aiColor3D colorDiffuse;
            if (mat->Get(AI_MATKEY_COLOR_DIFFUSE, colorDiffuse) == aiReturn_SUCCESS) {
				testMaterial.unlitColor.colorR = colorDiffuse.r;
				testMaterial.unlitColor.colorG = colorDiffuse.g;
				testMaterial.unlitColor.colorB = colorDiffuse.b;
            }
        }

        topo_material_t hMaterial;
        topo_create_material(topoInstance, &testMaterial, &hMaterial);
        materials[idxMat] = hMaterial;
    }

    for (size_t idxMesh = 0; idxMesh < scene->mNumMeshes; idxMesh++) {
        auto *mesh = scene->mMeshes[idxMesh];

        topo_mesh_descriptor *dMesh;
        topo_borrow_memory_ex(topoInstance, &dMesh);
        auto name = uuids::to_string(genUuid());
        topo_borrow_memory_ex(topoInstance, &dMesh->id, name.c_str());

        // Vertex buffers

        struct AttributeCopy {
            const uint8_t *pSource;
            size_t srcStride;
            size_t size;
            size_t offset;
        };

        std::vector<AttributeCopy> attributeCopies;
        std::vector<TopoVertexAttribute> attributes;
        size_t offNextOffset = 0;

        {
            auto size = 3 * sizeof(float);
            auto srcStride = size;
            attributeCopies.push_back(
                { (uint8_t *)mesh->mVertices, srcStride, size, offNextOffset });
            attributes.push_back({ 0, offNextOffset, TopoVES_Position, TopoVET_F32x3 });
            offNextOffset += 3 * sizeof(float);
        }

        if (mesh->GetNumUVChannels() > 0) {
            auto size = mesh->mNumUVComponents[0] * sizeof(float);
            auto srcStride = 3 * sizeof(float);
            auto type = mesh->mNumUVComponents[0] == 2 ? kTopoVET_F32x2 : kTopoVET_F32x3;
            attributeCopies.push_back({ (uint8_t *)mesh->mTextureCoords[0], srcStride, size, offNextOffset });
            attributes.push_back({ 0, offNextOffset, kTopoVES_Texcoord, type });
            offNextOffset += size;
        }

        if (mesh->HasNormals()) {
            auto size = 3 * sizeof(float);
            auto srcStride = size;
            attributeCopies.push_back({ (uint8_t *)mesh->mNormals, srcStride, size, offNextOffset });
            attributes.push_back({ 0, offNextOffset, kTopoVES_Normal, kTopoVET_F32x3 });
            offNextOffset += size;
        }

        if (mesh->HasTangentsAndBitangents()) {
            auto size = 3 * sizeof(float);
            auto srcStride = size;

            attributeCopies.push_back({ (uint8_t *)mesh->mTangents, srcStride, size, offNextOffset });
            attributes.push_back({ 0, offNextOffset, kTopoVES_Tangent, kTopoVET_F32x3 });
            offNextOffset += size;

            attributeCopies.push_back({ (uint8_t *)mesh->mBitangents, srcStride, size, offNextOffset });
            attributes.push_back({ 0, offNextOffset, kTopoVES_Bitangent, kTopoVET_F32x3 });
            offNextOffset += size;
        }

        auto sizVertex = offNextOffset;

        topo_vertex_buffer *buffer;
        topo_borrow_memory_ex(topoInstance, 1, &buffer);
        auto numVertices = mesh->mNumVertices;

        topo_borrow_memory_ex(
            topoInstance, sizVertex * numVertices, (uint8_t **)&buffer->pVertexData);
        buffer->sizVertexData = sizVertex * numVertices;
        buffer->sizVertexStride = sizVertex;

        memset((void *)buffer->pVertexData, 0xCDCDCDCD, sizVertex * numVertices);

        // Reinterleave the vertices
        for (size_t idxVertex = 0; idxVertex < numVertices; idxVertex++) {
            auto offVertex = idxVertex * sizVertex;
            assert(offVertex + sizVertex <= numVertices * sizVertex);
            auto *pVertex = (uint8_t *)buffer->pVertexData + idxVertex * sizVertex;
            for (auto &copy : attributeCopies) {
                memcpy(pVertex + copy.offset, copy.pSource + idxVertex * copy.srcStride, copy.size);
            }
        }

        dMesh->numVertexBuffers = 1;
        dMesh->pVertexBuffers = buffer;

        // Vertex attributes
        topo_borrow_memory_ex(topoInstance, attributes.size(), &dMesh->pAttributes, attributes.data());
        dMesh->numAttributes = attributes.size();

        // Submesh

        topo_submesh *submesh;
        topo_borrow_memory_ex(topoInstance, &submesh);
        dMesh->pSubmeshes = submesh;
        dMesh->numSubmeshes = 1;

        submesh->isIndexed = true;
        submesh->primitiveTopology = kTopoPT_TriangleList;
        assert(mesh->mMaterialIndex < materials.size());
        submesh->material = materials[mesh->mMaterialIndex];

        // Submesh index buffer
        if (mesh->mNumVertices > 65536) {
            uint32_t *pIndices;
            topo_borrow_memory_ex(topoInstance, mesh->mNumFaces * 3, &pIndices);

            auto &indexBuffer = submesh->indexBuffer;
            indexBuffer.numIndices = 3 * size_t(mesh->mNumFaces);
            indexBuffer.pIndices = pIndices;
            indexBuffer.type = topo_index_buffer::UINT32;

            for (size_t idxFace = 0; idxFace < mesh->mNumFaces; idxFace++) {
                assert(mesh->mFaces[idxFace].mNumIndices == 3);
                pIndices[idxFace * 3 + 0] = mesh->mFaces[idxFace].mIndices[0];
                pIndices[idxFace * 3 + 1] = mesh->mFaces[idxFace].mIndices[1];
                pIndices[idxFace * 3 + 2] = mesh->mFaces[idxFace].mIndices[2];
            }
        } else {
            uint16_t *pIndices;
            topo_borrow_memory_ex(topoInstance, mesh->mNumFaces * 3, &pIndices);

            auto &indexBuffer = submesh->indexBuffer;
            indexBuffer.numIndices = 3 * size_t(mesh->mNumFaces);
            indexBuffer.pIndices = pIndices;
            indexBuffer.type = topo_index_buffer::UINT16;

            for (size_t idxFace = 0; idxFace < mesh->mNumFaces; idxFace++) {
                assert(mesh->mFaces[idxFace].mNumIndices == 3);
                pIndices[idxFace * 3 + 0] = uint16_t(mesh->mFaces[idxFace].mIndices[0]);
                pIndices[idxFace * 3 + 1] = uint16_t(mesh->mFaces[idxFace].mIndices[1]);
                pIndices[idxFace * 3 + 2] = uint16_t(mesh->mFaces[idxFace].mIndices[2]);
            }
        }

        topo_create_mesh(topoInstance, dMesh, &meshes[idxMesh]);
    }

    traverseNode(aiMatrix4x4(), meshes, scene->mRootNode, outObjects);

    // Load cameras
    std::vector<topo_camera> cameras(scene->mNumCameras);

    for (size_t idxCamera = 0; idxCamera < scene->mNumCameras; idxCamera++) {
        auto *camera = scene->mCameras[idxCamera];
        auto *cameraNode = scene->mRootNode->FindNode(camera->mName.data);
        aiMatrix4x4 matView;

        while (cameraNode != scene->mRootNode) {
            matView = cameraNode->mTransformation * matView;
            cameraNode = cameraNode->mParent;
        }

		matView = cameraNode->mTransformation * matView;

        aiMatrix4x4 matCamera;
        camera->GetCameraMatrix(matCamera);
        matView.Inverse();
        // matView = matView.Inverse() * matCamera;
        aiMatrix4x4 matFlip;
        aiMatrix4x4::RotationY(AI_MATH_PI, matFlip);
        // matView = matView * matFlip;

        topo_camera tCamera;
        tCamera.nearClip = camera->mClipPlaneNear;
        tCamera.farClip = camera->mClipPlaneFar;
        convert(tCamera.transform, matView);
        tCamera.fieldOfView = camera->mHorizontalFOV;
        cameras[idxCamera] = tCamera;
    }

    if (cameras.size() == 0) {
        // Default camera
        auto lookAt = glm::lookAt(glm::vec3(7.3589f, 6.9258f, 4.9583f), glm::vec3(), glm::vec3(0, 1, 0));
        glm::vec3 scale, position, skew;
        glm::vec4 perspective;
        glm::quat orient;
        glm::decompose(lookAt, scale, orient, position, skew, perspective);
        topo_camera mainCamera;
        mainCamera.transform.position[0] = position.x;
        mainCamera.transform.position[1] = position.y;
        mainCamera.transform.position[2] = position.z;
        mainCamera.transform.rotation[0] = orient.w;
        mainCamera.transform.rotation[1] = orient.x;
        mainCamera.transform.rotation[2] = orient.y;
        mainCamera.transform.rotation[3] = orient.z;
        mainCamera.transform.scale[0] = 1.0f;
        mainCamera.transform.scale[1] = 1.0f;
        mainCamera.transform.scale[2] = 1.0f;
        mainCamera.nearClip = 0.01f;
        mainCamera.farClip = 1000.0f;
        mainCamera.fieldOfView = glm::radians(39.6f);
        cameras.push_back(mainCamera);
    }

    std::vector<topo_light_source> lights;
    for (size_t idxLight = 0; idxLight < scene->mNumLights; idxLight++) {
        auto *light = scene->mLights[idxLight];
        auto *lightNode = scene->mRootNode->FindNode(light->mName.data);
        aiMatrix4x4 matView;

        while (lightNode != scene->mRootNode) {
            matView = lightNode->mTransformation * matView;
            lightNode = lightNode->mParent;
        }

		matView = lightNode->mTransformation * matView;

        topo_light_source tLight = {};

        // TODO(danielm): although gltf has separate color and intensity values for each light source, assimp
        // only exposes the light's color multiply by the intensity. since the color isn't a normalized
        // vector, there is no way to recover the intensity value.
        // As a workaround we set the power 
        auto color = glm::vec3(light->mColorDiffuse.r, light->mColorDiffuse.g, light->mColorDiffuse.b);
        float intensity = 1.0;
		tLight.color[0] = color.r;
        tLight.color[1] = color.g;
        tLight.color[2] = color.b;
        tLight.intensity = intensity;

        switch (light->mType) {
        case aiLightSource_POINT: {
            tLight.kind = kTopoLight_Point;
            aiVector3D transl = matView * light->mPosition;

            tLight.point.position[0] = transl.x;
            tLight.point.position[1] = transl.y;
            tLight.point.position[2] = transl.z;
            break;
        }
        case aiLightSource_AMBIENT: {
            tLight.kind = kTopoLight_Ambient;
            break;
        }
        }

        lights.push_back(tLight);
    }

    if (lights.empty()) {
        topo_light_source tLight = {};
        tLight.kind = kTopoLight_Ambient;
        tLight.color[0] = 1;
        tLight.color[1] = 1;
        tLight.color[2] = 1;
        tLight.intensity = 1;
        lights.push_back(tLight);
    }

    outResources.cameras = std::move(cameras);
    outResources.materials = std::move(materials);
    outResources.textures = std::move(textures);
    outResources.meshes = std::move(meshes);
    outObjects.lights = std::move(lights);

    return kTopo_OK;
}
}
