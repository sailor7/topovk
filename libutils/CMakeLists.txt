add_library(utils SHARED
  ../public/utils/base.h
  ../public/utils/scene_assimp.h
  ../public/utils/scene_dyn.h

  scene_assimp.cpp
  scene_dyn.cpp
)
target_link_libraries(utils
  PRIVATE
    CONAN_PKG::hedley
    CONAN_PKG::glm
    CONAN_PKG::assimp
    CONAN_PKG::stduuid
  PUBLIC
    topo::main
    Tracy::TracyClient
)
target_compile_definitions(utils PRIVATE UTILS_BUILD=1 GLM_FORCE_DEPTH_ZERO_TO_ONE=1)
add_library(topo::utils ALIAS utils)

if(MSVC)
  target_compile_options(utils PRIVATE /W4)
else()
  target_compile_options(utils PRIVATE -Wall -Wextra -Wpedantic)
endif()

set_target_properties(utils PROPERTIES CXX_VISIBILITY_PRESET hidden)

