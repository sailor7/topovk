#version 450
#pragma shader_stage(vertex)

layout(set = 0, binding = 0) uniform PerPass {
    mat4 matViewProj;
}
perPass;

layout(set = 1, binding = 0) uniform PerDraw {
    mat4 matModel;
}
perDraw;

layout(location = 0) in vec4 isPosition;
layout(location = 4) in vec4 isTexcoord;

layout(location = 0) out vec2 fTexcoord;

void main() {
    gl_Position = isPosition;
    fTexcoord = isTexcoord.xy;
}
