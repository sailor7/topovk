#version 450
#pragma shader_stage(fragment)

layout(set = 1, binding = 0) uniform PerDraw {
    //
    mat4 matModel;
    vec4 baseColor;
}
perDraw;

layout(location = 1) in vec3 fPosition;
layout(location = 2) in vec3 fNormal;

layout(location = 0) out vec4 omColor;
layout(location = 1) out vec4 omPosition;
layout(location = 2) out vec4 omNormalWorld;
layout(location = 3) out vec4 omX;

void main() {
    omColor = vec4(perDraw.baseColor.rgb, 1.0);
    omPosition = vec4(fPosition, 1.0);
    omNormalWorld = vec4(fNormal, 0.0);
    omX = vec4(1.0, 1.0, 1.0, 1.0);
}
