#version 450
#pragma shader_stage(fragment)

layout(set = 2, binding = 0) uniform sampler2D texColor;
layout(set = 2, binding = 1) uniform sampler2D texNormal;
layout(set = 2, binding = 2) uniform sampler2D texMetallicRoughness;

layout(location = 0) in vec2 fTexcoord;
layout(location = 1) in vec3 fPosition;
layout(location = 2) in mat3 fTBN;

layout(location = 0) out vec4 omColor;
layout(location = 1) out vec4 omPosition;
layout(location = 2) out vec4 omNormalWorld;
layout(location = 3) out vec4 omX;

void main() {
    vec3 rawNormalVecInTangentSpace = texture(texNormal, fTexcoord).xyz;
    vec3 normalVecInTangentSpace = 2.0 * rawNormalVecInTangentSpace - 1.0;

    vec3 metallicRoughness = texture(texMetallicRoughness, fTexcoord).rgb;

    omColor = vec4(texture(texColor, fTexcoord).rgb, 1.0);
    omPosition = vec4(fPosition, metallicRoughness.b);
    omNormalWorld = vec4(normalize(fTBN * normalVecInTangentSpace), metallicRoughness.g);
    omX = vec4(1.0, 1.0, 1.0, 1.0);
}
