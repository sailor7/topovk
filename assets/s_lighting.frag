#version 450
#pragma shader_stage(fragment)

#define PI 3.14159265358979323846264338327950288f

struct PointLight {
    vec4 origin;
    vec4 color;
    float intensity;
};

layout(set = 2, binding = 0) uniform sampler2D texColor;
layout(set = 2, binding = 1) uniform sampler2D texPosition;
layout(set = 2, binding = 2) uniform sampler2D texNormal;
layout(set = 2, binding = 3) uniform sampler2D texX;

layout(set = 2, binding = 4) uniform LightInfo {
    vec4 viewPosition;

    vec4 ambientColor;
    uint hasAmbient;
    float ambientPower;

    uint numPointLights;
    uint numAreaLights;
}
lightInfo;

layout(set = 2, binding = 5) buffer PointLights { PointLight lights[]; }
pointLights;

layout(location = 0) in vec2 fTexcoord;

layout(location = 0) out vec4 fColor;

float saturate(float x) {
  return max(0, min(1, x));
}

vec3 reinhard(vec3 c) {
    float luminance0 = dot(c, vec3(0.2126f, 0.7152f, 0.0722f));
    float luminance1 = luminance0 / (1.0f + luminance0);
    return c * (luminance1 / luminance0);
}

// https://google.github.io/filament/Filament.md.html#materialsystem/diffusebrdf
float Fd_Lambert() {
    return 1.0 / PI;
}

vec3 computeF0(vec3 baseColor, float metallic) {
    return baseColor.rgb * metallic + (0.04 * (1.0 - metallic));
}

vec3 computeDiffuseColor(const vec3 baseColor, float metallic) {
    return baseColor.rgb * (1.0 - metallic);
}

// https://google.github.io/filament/Filament.md.html#materialsystem/specularbrdf/fresnel(specularf)
vec3 F_Schlick(const vec3 f0, float f90, float VoH) {
    // Schlick 1994, "An Inexpensive BRDF Model for Physically-Based Rendering"
    return f0 + (f90 - f0) * pow(1.0 - VoH, 5);
}

// https://google.github.io/filament/Filament.md.html#materialsystem/specularbrdf/geometricshadowing(specularg)
float V_SmithGGXCorrelated(float roughness, float NoV, float NoL) {
    // Heitz 2014, "Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs"
    float a2 = roughness * roughness;
    // TODO: lambdaV can be pre-computed for all the lights, it should be moved out of this function
    float lambdaV = NoL * sqrt((NoV - a2 * NoV) * NoV + a2);
    float lambdaL = NoV * sqrt((NoL - a2 * NoL) * NoL + a2);
    float v = 0.5 / (lambdaV + lambdaL);
    // a2=0 => v = 1 / 4*NoL*NoV   => min=1/4, max=+inf
    // a2=1 => v = 1 / 2*(NoL+NoV) => min=1/4, max=+inf
    // clamp to the maximum value representable in mediump
    return saturate(v);
}

// https://google.github.io/filament/Filament.md.html#materialsystem/specularbrdf/normaldistributionfunction(speculard)
float D_GGX(float roughness, float NoH, const vec3 h, const vec3 n) {
    float oneMinusNoHSquared = 1.0 - NoH * NoH;

    float a = NoH * roughness;
    float k = roughness / (oneMinusNoHSquared + a * a);
    float d = k * k * (1.0 / PI);
    return saturate(d);
}

void main() {
    vec4 texelPosition = texture(texPosition, fTexcoord);
    vec4 texelNormal = texture(texNormal, fTexcoord);
    vec3 baseColor = texture(texColor, fTexcoord).rgb;
    vec3 position = texelPosition.xyz;
    vec3 normal = texelNormal.xyz;
    float roughness = texelPosition.w * texelPosition.w;
    float metallic  = texelNormal.w;
    vec3 accum = vec3(0.0, 0.0, 0.0);

    if (lightInfo.hasAmbient != 0) {
        accum += lightInfo.ambientPower * lightInfo.ambientColor.rgb;
    }

    vec3 f0 = computeF0(baseColor, metallic);
    vec3 diffuseColor = computeDiffuseColor(baseColor, metallic);
    float f90 = saturate(dot(f0, vec3(50.0 * 0.33)));

    for (uint idxLight = 0; idxLight < lightInfo.numPointLights; idxLight++) {
        vec3 pointToLight = pointLights.lights[idxLight].origin.xyz - position;
        vec3 viewToLight = pointLights.lights[idxLight].origin.xyz - lightInfo.viewPosition.xyz;
        vec3 pointToView = lightInfo.viewPosition.xyz - position;
        float dist = length(pointToLight);
        vec3 dir = normalize(pointToLight);
        float LdotN = max(0.0, dot(dir, normal));

        if(LdotN == 0.0) {
            continue;
        }

        vec3 vecHalfway = normalize(pointToLight + pointToView);
        float NoH = saturate(dot(normal, vecHalfway));
        float NoV = abs(dot(normal, pointToView)) + 1e-5;

        float V = V_SmithGGXCorrelated(roughness, NoV, LdotN);
        vec3 F  = F_Schlick(f0, f90, NoH);
		float D = D_GGX(roughness, NoH, vecHalfway, normal);

        vec3 Fr = (D * V) * F;
        vec3 Fd = diffuseColor * Fd_Lambert();

        // https://google.github.io/filament/Filament.md.html#materialsystem/improvingthebrdfs/energylossinspecularreflectance
		vec3 energyCompensation = vec3(1.0) + f0;

        float flux = LdotN * pointLights.lights[idxLight].intensity / 1000.0 / (dist * dist);
        accum += flux * pointLights.lights[idxLight].color.rgb * (Fr * energyCompensation + Fd);
    }

    fColor = vec4(reinhard(accum) * baseColor, 1.0);
}
