#version 450
#pragma shader_stage(vertex)

layout(set = 0, binding = 0) uniform PerPass {
    //
    mat4 matViewProj;
}
perPass;

layout(set = 1, binding = 0) uniform PerDraw {
    //
    mat4 matModel;
    vec4 baseColor;
}
perDraw;

layout(location = 0) in vec4 isPosition;
layout(location = 1) in vec3 isTangent;
layout(location = 2) in vec3 isBitangent;
layout(location = 3) in vec3 isNormal;
layout(location = 4) in vec2 isTexcoord;

layout(location = 1) out vec3 fPosition;
layout(location = 2) out vec3 fNormal;

void main() {
    gl_Position = perPass.matViewProj * perDraw.matModel * isPosition;
    fPosition = (perDraw.matModel * isPosition).xyz;

    fNormal = normalize(vec3(mat3(perDraw.matModel) * isNormal));
}
