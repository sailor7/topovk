#version 450
#pragma shader_stage(fragment)

layout(set = 2, binding = 0) uniform sampler2D texColor;

layout(location = 0) in vec2 fTexcoord;
layout(location = 1) in vec3 fPosition;
layout(location = 2) in mat3 fTBN;

layout(location = 0) out vec4 omColor;
layout(location = 1) out vec4 omPosition;
layout(location = 2) out vec4 omNormalWorld;
layout(location = 3) out vec4 omX;

void main() {
    //
    omColor = vec4(texture(texColor, fTexcoord).rgb, 1.0);
    omPosition = vec4(fPosition, 1.0);
    omNormalWorld = vec4(fTBN * vec3(0.0, 0.0, 1.0), 0.0);
    omX = vec4(1.0, 1.0, 1.0, 1.0);
}
