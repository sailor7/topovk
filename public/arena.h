#pragma once

#include <cassert>
#include <cstdint>
#include <cstring>
#include <utility>

using arena_offset = uint32_t;
constexpr auto ARENA_INDEX_VALID_MASK = uint32_t(1 << 31);

inline bool is_valid_arena_offset(arena_offset v) { return (v & ARENA_INDEX_VALID_MASK) != 0; }
template <typename T> inline uint32_t arena_offset_to_index(arena_offset v) {
    return (v & (~ARENA_INDEX_VALID_MASK)) / sizeof(T);
}

template <typename T> inline arena_offset index_to_arena_offset(uint32_t idx) {
    return (idx * sizeof(T)) | ARENA_INDEX_VALID_MASK;
}

struct arena_t {
    arena_t(uint32_t lenSize, uint32_t lenScratch)
        : buffer(new uint8_t[lenSize])
        , idxCursor(0)
        , lenSize(lenSize)
        , scratch(new uint8_t[lenScratch]) {
        memset(buffer, 0, lenSize);
        memset(scratch, 0, lenScratch);
    }

    arena_t(uint32_t lenSize)
        : arena_t(lenSize, 4096) { }

    ~arena_t() {
        if (buffer) {
            delete[] buffer;
        }
        if (scratch) {
            delete[] scratch;
        }
    }

    arena_t(const arena_t &) = delete;
    arena_t(arena_t &&other)
        : buffer(nullptr)
        , idxCursor(0)
        , lenSize(0)
        , scratch(new uint8_t[4096]) {
        std::swap(idxCursor, other.idxCursor);
        std::swap(buffer, other.buffer);
        std::swap(lenSize, other.lenSize);
        std::swap(scratch, other.scratch);
    }

    void operator=(const arena_t &) = delete;
    arena_t &operator=(arena_t &&other) {
        if (buffer) {
            delete[] buffer;
            buffer = nullptr;
        }

        if (scratch) {
            delete[] scratch;
            scratch = nullptr;
        }

        std::swap(buffer, other.buffer);
        std::swap(scratch, other.scratch);

        return *this;
    }

    arena_offset push(uint32_t numBytes, void **ptr) {
        arena_offset res = 0;
        if (idxCursor + numBytes <= lenSize) {
            res = idxCursor | ARENA_INDEX_VALID_MASK;
            idxCursor += numBytes;
        }

        if (ptr != nullptr) {
            *ptr = resolve(res);
        }

        return res;
    }

    template <typename T> arena_offset pushElements(uint32_t numElements, T **ptr) {
        void *temp;
        auto ret = push(numElements * sizeof(T), &temp);
        *ptr = (T *)temp;
        return ret;
    }

    void *resolveImpl(arena_offset off) {
        if (off == 0) {
            return scratch;
        }
        assert((off & ARENA_INDEX_VALID_MASK) != 0);
        auto realOffset = off & (~ARENA_INDEX_VALID_MASK);
        assert(realOffset < lenSize);
        return buffer + realOffset;
    }

    template <typename T = void> T *resolve(arena_offset off) { return (T *)resolveImpl(off); }

    template <typename T> T *getCursor() { return (T *)(buffer + idxCursor); }

    void discard() { idxCursor = 0; }

    void clear() {
        memset(buffer, 0, lenSize);
        discard();
    }

    uint32_t size() const noexcept { return idxCursor; }

    static uint32_t kilobytes(uint32_t num) { return num * 1024; }
    static uint32_t megabytes(uint32_t num) { return kilobytes(num) * 1024; }

    uint8_t *buffer;
    uint32_t idxCursor;
    uint32_t lenSize;
    uint8_t *scratch;
};