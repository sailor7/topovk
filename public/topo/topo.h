#pragma once

#include <hedley.h>

#include <stddef.h>
#include <stdint.h>

#if defined(TOPO_BUILD)
#define TOPO_API HEDLEY_C_DECL HEDLEY_PUBLIC
#else
#define TOPO_API HEDLEY_IMPORT
#endif

#define TOPO_VERSION_1_0 HEDLEY_VERSION_ENCODE(1, 0, 0)
#define TOPO_VERSION_1_1 HEDLEY_VERSION_ENCODE(1, 1, 0)

#define TOPO_VERSION TOPO_VERSION_1_1
#if !defined(TOPO_VERSION_TARGET)
#define TOPO_VERSION_TARGET TOPO_VERSION
#endif

#if TOPO_VERSION_TARGET < TOPO_VERSION_1_0
#define TOPO_1_0 HEDLEY_UNAVAILABLE(1.0)
#else
#define TOPO_1_0 TOPO_API
#endif

#if TOPO_VERSION_TARGET < TOPO_VERSION_1_1
#define TOPO_1_1 HEDLEY_UNAVAILABLE(1.1)
#else
#define TOPO_1_1 TOPO_API
#endif

typedef struct topo_instance *TopoInstance;
typedef struct topo_mesh *TopoMesh;
typedef struct topo_material *TopoMaterial;
typedef struct topo_image *TopoImage;
typedef struct topo_rendertarget *TopoRenderTarget;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoInstance topo_instance_t;
HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMesh topo_mesh_handle;
HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterial topo_material_t;
HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoImage topo_image_t;
HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoRenderTarget topo_rendertarget_t;

typedef struct {
    const char *title;
    unsigned window_width;
    unsigned window_height;
    int enable_validation;
} TopoInstanceDescriptor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoInstanceDescriptor topo_instance_descriptor;

typedef enum {
    Topo_OK = 0,
    Topo_Failure,
    Topo_Pending,
    Topo_EmptyResult,

    kTopo_OK = Topo_OK,
    kTopo_Failure = Topo_Failure,
    kTopo_Pending = Topo_Pending,
} TopoResult;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoResult topo_result;

typedef enum {
    TopoIF_RGBA32_SRGB,
    TopoIF_RGBA32,
    TopoIF_BGRA32_SRGB,
    TopoIF_BGRA32,

    kTopo_FormatRGBA32_SRGB = TopoIF_RGBA32_SRGB,
    kTopo_FormatRGBA32 = TopoIF_RGBA32,
    kTopo_FormatBGRA32_SRGB = TopoIF_BGRA32_SRGB,
    kTopo_FormatBGRA32 = TopoIF_BGRA32,
} TopoImageFormat;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoImageFormat topo_image_format;

typedef struct {
    const char *id;

    uint32_t width;
    uint32_t height;
    TopoImageFormat format;
    const void *pData;
} TopoImageDescriptor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoImageDescriptor topo_image_descriptor;

typedef enum {
    TopoMK_SolidColor,
    TopoMK_TextureColor,
    TopoMK_RenderTargetReceiver,
    TopoMK_NormalMapped,
    TopoMK_Count,

    kTopoMaterialKind_SolidColor = TopoMK_SolidColor,
    kTopoMaterialKind_TextureColor = TopoMK_TextureColor,
    kTopoMaterialKind_RenderTargetReceiver = TopoMK_RenderTargetReceiver,
    kTopoMaterialKind_NormalMapped = TopoMK_NormalMapped,
    kTopoMaterialKind_Count = TopoMK_Count,

    kTopoMaterialKind_UnlitColor = kTopoMaterialKind_SolidColor,
    kTopoMaterialKind_UnlitTextured = kTopoMaterialKind_TextureColor,
} TopoMaterialKind;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialKind topo_material_kind;

typedef struct {
    float colorR, colorG, colorB;
} TopoMaterialUnlitColor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialUnlitColor topo_material_unlit_color;

typedef struct {
    TopoImage imageBaseColor;
} TopoMaterialUnlitTextured;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialUnlitTextured topo_material_unlit_textured;

typedef struct {
    TopoRenderTarget renderTarget;
} TopoMaterialRenderTargetReceiver;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialRenderTargetReceiver topo_material_render_target_receiver;

typedef struct {
    TopoImage imageBaseColor;
    TopoImage imageNormalMap;
    TopoImage imageMetallicRoughness;
} TopoMaterialNormalMapped;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialNormalMapped topo_material_normal_mapped;

typedef struct {
    const char *id;
    TopoMaterialKind kind;

    union {
        TopoMaterialUnlitColor unlitColor;
        TopoMaterialUnlitTextured unlitTextured;
        TopoMaterialRenderTargetReceiver rtReceiver;
        TopoMaterialNormalMapped normalMapped;
    };
} TopoMaterialDescriptor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMaterialDescriptor topo_material_descriptor;

typedef struct {
    const void *pVertexData;
    uint64_t sizVertexData;
    uint64_t sizVertexStride;
} TopoVertexBuffer;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoVertexBuffer topo_vertex_buffer;

typedef struct {
    const void *pIndices;
    uint64_t numIndices;
    enum {
        UINT16,
        UINT32,
    } type;
} TopoIndexBuffer;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoIndexBuffer topo_index_buffer;

typedef enum {
    TopoPT_TriangleList,

    kTopoPT_TriangleList = TopoPT_TriangleList,
} TopoPrimitiveTopology;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoPrimitiveTopology topo_primitive_topology;

typedef struct {
    TopoPrimitiveTopology primitiveTopology;

    bool isIndexed;

    TopoIndexBuffer indexBuffer;

    TopoMaterial material;
} TopoSubmeshDescriptor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoSubmeshDescriptor topo_submesh;

typedef enum {
    TopoVES_Position,
    TopoVES_Tangent,
    TopoVES_Bitangent,
    TopoVES_Normal,
    TopoVES_Texcoord,

    kTopoVES_Position = TopoVES_Position,
    kTopoVES_Tangent = TopoVES_Tangent,
    kTopoVES_Bitangent = TopoVES_Bitangent,
    kTopoVES_Normal = TopoVES_Normal,
    kTopoVES_Texcoord = TopoVES_Texcoord,
} TopoVertexElementSemantic;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoVertexElementSemantic topo_vertex_element_semantic;

typedef enum {
    TopoVET_F32x2,
    TopoVET_F32x3,
    TopoVET_F32x4,

    kTopoVET_F32x2 = TopoVET_F32x2,
    kTopoVET_F32x3 = TopoVET_F32x3,
    kTopoVET_F32x4 = TopoVET_F32x4,
} TopoVertexElementType;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoVertexElementType topo_vertex_element_type;

typedef struct {
    uint64_t idxVertexBuffer;
    uint64_t offset;
    TopoVertexElementSemantic semantic;
    TopoVertexElementType type;
} TopoVertexAttribute;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoVertexAttribute topo_vertex_attribute;

typedef struct {
    const char *id;

    uint32_t numVertexBuffers;
    const TopoVertexBuffer *pVertexBuffers;

    uint32_t numAttributes;
    const TopoVertexAttribute *pAttributes;

    uint32_t numSubmeshes;
    const TopoSubmeshDescriptor *pSubmeshes;
} TopoMeshDescriptor;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoMeshDescriptor topo_mesh_descriptor;

typedef struct {
    float position[3];
    float rotation[4];
    float scale[3];
} TopoTransform;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoTransform topo_transform;

typedef struct {
    TopoTransform transform;

    float nearClip;
    float farClip;
    float fieldOfView;
} TopoCamera;

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
typedef TopoCamera topo_camera;

typedef enum {
    TopoLSK_Ambient,
    TopoLSK_Point,
    TopoLSK_Area,

    kTopoLight_Ambient = TopoLSK_Ambient,
    kTopoLight_Point = TopoLSK_Point,
    kTopoLight_Area = TopoLSK_Area,
} TopoLightSourceKind;

typedef TopoLightSourceKind topo_light_source_kind;

typedef struct {
    TopoLightSourceKind kind;
    float color[3];
    float intensity;

    union {
        struct {
        } ambient;
        struct {
            float position[3];
        } point;
        struct {
            float positions[3][4];
        } area;
    };
} TopoLightSource;

typedef TopoLightSource topo_light_source;

typedef struct {
    uint32_t numObjects;
    const TopoTransform *pTransforms;
    const TopoMesh *pMeshes;

    uint32_t numLights;
    const TopoLightSource *pLights;
} TopoObjectGroup;

typedef struct {
    uint32_t numObjectGroups;
    const TopoObjectGroup *pObjectGroups;

    uint32_t numCameras;
    const TopoCamera *pCameras;

    uint32_t idxMainCamera;

    uint32_t enableShadows;
    float shadowCasterDir[3];
} TopoScene2;

typedef struct {
    TopoCamera mainCamera;

    uint32_t numObjects;

    const TopoTransform *transforms;
    const TopoMesh *meshes;

    uint32_t numLights;
    const TopoLightSource *lights;

    const TopoScene2 *pNext;
} TopoScene;

typedef TopoScene topo_scene;


#if __cplusplus
extern "C" {
#endif

TOPO_1_0 HEDLEY_NON_NULL(1, 2) TopoResult
    topo_create(TopoInstance *out_instance, const TopoInstanceDescriptor *descriptor);
TOPO_1_0 HEDLEY_NON_NULL(1) TopoResult topo_destroy(TopoInstance *instance);
TOPO_1_0 TopoResult topo_exiting(TopoInstance instance, bool *exiting);

TOPO_1_0 TopoResult
topo_create_mesh(TopoInstance instance, const TopoMeshDescriptor *descriptor, TopoMesh *out);

TOPO_1_0 HEDLEY_NON_NULL(1, 2) TopoResult topo_destroy_mesh(TopoInstance instance, TopoMesh *handle);

TOPO_1_0 TopoResult topo_create_material(
    TopoInstance instance,
    const TopoMaterialDescriptor *descriptor,
    TopoMaterial *out_handle);

TOPO_1_0 TopoResult topo_destroy_material(TopoInstance instance, TopoMaterial *handle);

TOPO_1_0 TopoResult
topo_create_image(TopoInstance instance, const TopoImageDescriptor *descriptor, TopoImage *out_handle);

TOPO_1_0 topo_result topo_destroy_image(TopoInstance instance, TopoImage *handle);

TOPO_1_0 topo_result topo_borrow_memory(TopoInstance instance, size_t size, void **out_ptr);

TOPO_1_0 TopoResult
topo_set_scene(TopoInstance instance, const TopoScene *nextScene, const TopoScene **prevScene);

HEDLEY_DEPRECATED(TOPO_VERSION_1_1)
TOPO_1_0 TopoResult topo_wait_for_frame(TopoInstance instance);

TOPO_1_1 TopoResult
topoSetSceneAndWait(TopoInstance hInstance, const TopoScene *pNextScene, const TopoScene **ppPrevScene);
TOPO_1_1 TopoResult topoPollOutcomingScene(TopoInstance hInstance, const TopoScene **ppPrevScene);

#if __cplusplus
}
#endif

#if __cplusplus
#include <cstddef>
#include <cstring>

template <typename T>
inline TopoResult topo_borrow_memory_ex(TopoInstance instance, size_t numElements, T **out_ptr) {
    return topo_borrow_memory(instance, numElements * sizeof(T), (void **)out_ptr);
}

template <typename T> inline TopoResult topo_borrow_memory_ex(TopoInstance instance, T **out_ptr) {
    return topo_borrow_memory(instance, sizeof(T), (void **)out_ptr);
}

template <>
inline TopoResult topo_borrow_memory_ex<void>(TopoInstance instance, size_t size, void **out_ptr) {
    return topo_borrow_memory(instance, size, (void **)out_ptr);
}

template <typename T>
inline TopoResult
topo_borrow_memory_ex(TopoInstance instance, size_t numElements, T **out_ptr, const void *src) {
    auto ret = topo_borrow_memory_ex(instance, numElements, out_ptr);
    memcpy((void *)*out_ptr, src, numElements * sizeof(T));
    return ret;
}

inline TopoResult topo_borrow_memory_ex(TopoInstance instance, const char **out_ptr, const char *src) {
    auto len = strlen(src);
    auto size = len + 1;
    auto ret = topo_borrow_memory_ex(instance, size, out_ptr);
    memcpy((char *)*out_ptr, src, size);
    return ret;
}
#endif
