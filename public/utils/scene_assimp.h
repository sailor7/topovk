#pragma once

#include <utils/base.h>

#include <topo/topo.h>

#include <vector>

namespace utils {
struct Resources {
    std::vector<topo_image_t> textures;
    std::vector<topo_material_t> materials;
    std::vector<topo_mesh_handle> meshes;
    std::vector<topo_camera> cameras;
};

struct Objects {
    std::vector<topo_mesh_handle> meshes;
    std::vector<topo_transform> transforms;
    std::vector<topo_light_source> lights;
};

struct Renderer {
    topo_instance_t instance;
    topo_image_t hDefaultTexture;
};

UTILS_API topo_result loadSceneFromFile(
    const char *pInputPath,
    const Renderer &renderer,
    Resources &outResources,
    Objects &outObjects);
}