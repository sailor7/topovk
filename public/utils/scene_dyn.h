#pragma once

#include <utils/base.h>
#include <topo/topo.h>

#include <memory>

class IImmediateScene {
public:
    virtual ~IImmediateScene() = default;

    [[nodiscard]] virtual bool beginScene() = 0;
    virtual void submitScene() = 0;

    virtual uint32_t beginObjectGroup() = 0;
    virtual void addToObjectGroup(TopoMesh hMesh, const TopoTransform &transform) = 0;
    virtual void addToObjectGroup(const TopoLightSource &lightSource) = 0;
    virtual void endObjectGroup() = 0;

    virtual uint32_t addCamera(const TopoCamera &camera) = 0;
    virtual void setMainCamera(uint32_t idxCamera) = 0;

    virtual void release(uint32_t numHandles, const TopoMesh *pMeshes) = 0;
    virtual void release(uint32_t numHandles, const TopoMaterial *pMaterials) = 0;
    virtual void release(uint32_t numHandles, const TopoImage *pImages) = 0;
};

UTILS_API std::unique_ptr<IImmediateScene> makeDynScene(TopoInstance renderer);