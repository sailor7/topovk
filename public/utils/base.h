#pragma once

#include <hedley.h>

#if UTILS_BUILD
#define UTILS_API HEDLEY_PUBLIC
#else
#define UTILS_API HEDLEY_IMPORT
#endif