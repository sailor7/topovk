#include <AppCore/Platform.h>
#include <Ultralight/Ultralight.h>
#include <utils/base.h>

#include <filesystem>

#include <topo/topo.h>

#include <glm/glm.hpp>

#include <uuid.h>

#include <tracy/Tracy.hpp>

#include <utils/scene_dyn.h>

struct ResourceReleaseQueue {
    std::vector<TopoMesh> meshes;
    std::vector<TopoMaterial> materials;
    std::vector<TopoImage> images;
};

struct SceneBacking {
    ResourceReleaseQueue releaseQueue;

    std::unique_ptr<TopoScene> scene;
    std::vector<TopoMesh> meshes;
    std::vector<TopoTransform> transforms;
    std::vector<TopoLightSource> lights;
};

class BrowserWindow {
public:
    BrowserWindow(
        TopoInstance instance,
        ultralight::Ref<ultralight::View> &&view,
        uuids::uuid_random_generator &uuidGen)
        : _instance(instance)
        , _view(std::move(view))
        , _uuidGen(uuidGen) {
        memset(&_transform, 0, sizeof(_transform));
        _transform.rotation[0] = 1.0f;

        auto w = float(_view->width());
        auto h = float(_view->height());

        _transform.scale[0] = w / h;
        _transform.scale[1] = 1;
        _transform.scale[2] = 1;
    }

    bool AddToScene(std::unique_ptr<IImmediateScene> &scene) {
        ZoneScoped;
        if (!_meshValid) {
            return false;
        }

        scene->addToObjectGroup(_mesh, _transform);

        return true;
    }

    void SetPosition(float x, float y, float z) {
        _transform.position[0] = x;
        _transform.position[1] = y;
        _transform.position[2] = z;
    }

    void Recreate(std::unique_ptr<IImmediateScene> &scene) {
        ZoneScoped;
        if (_meshValid) {
            scene->release(1, &_mesh);
            scene->release(1, &_material);
            scene->release(1, &_image);
            _mesh = TopoMesh(-1);
            _meshValid = false;
        }

        auto idTexture = uuids::to_string(_uuidGen());
        auto idMat = uuids::to_string(_uuidGen());
        auto idMesh = uuids::to_string(_uuidGen());

        auto *surface = _view->surface();
        auto *bitmap_surface = (ultralight::BitmapSurface *)surface;
        auto bitmap = bitmap_surface->bitmap();
        auto *pixels = bitmap->LockPixels();
        uint32_t width = bitmap->width();
        uint32_t height = bitmap->height();

        TopoImageDescriptor imgDesc = {};
        imgDesc.width = width;
        imgDesc.height = height;
        imgDesc.id = idTexture.c_str();

        switch (bitmap->format()) {
        case ultralight::BitmapFormat::kBitmapFormat_BGRA8_UNORM_SRGB:
            imgDesc.format = TopoIF_BGRA32_SRGB;
            break;
        }

        topo_borrow_memory_ex(_instance, width * height * 4, (uint8_t **)&imgDesc.pData, pixels);

        topo_create_image(_instance, &imgDesc, &_image);

        bitmap->UnlockPixels();

        TopoMaterialDescriptor matDesc;
        matDesc.id = idMat.c_str();
        matDesc.kind = kTopoMaterialKind_TextureColor;
        matDesc.unlitTextured.imageBaseColor = _image;
        topo_create_material(_instance, &matDesc, &_material);

        const glm::vec3 position[]
            = { { -1.0f, -1.0f, 0.0f }, { 1.0f, -1.0f, 0.0f }, { -1.0f, 1.0f, 0.0f }, { 1.0f, 1.0f, 0.0f } };
        const glm::vec3 tangent[] = { { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 } };
        const glm::vec3 bitangent[] = { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } };
        const glm::vec3 normal[] = { { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 } };
        const glm::vec2 texcoord[] = { { 0.0f, 1.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1.0f, 0.0f } };
        const uint16_t index[] = { 0, 2, 1, 2, 3, 1 };

        TopoMeshDescriptor *descMesh;
        TopoVertexBuffer *buffers;
        TopoVertexAttribute *attributes;
        TopoSubmeshDescriptor *submeshes;
        glm::vec3 *bufPosition;
        glm::vec3 *bufTangent, *bufBitangent, *bufNormal;
        glm::vec3 *bufTexcoord;
        uint16_t *bufIndex;
        const char *id;

        topo_borrow_memory_ex(_instance, &descMesh);

        topo_borrow_memory_ex(_instance, &id, idMesh.c_str());
        descMesh->id = id;

        // Buffers

        topo_borrow_memory_ex(_instance, 5, &buffers);
        descMesh->numVertexBuffers = 5;
        descMesh->pVertexBuffers = buffers;

        topo_borrow_memory_ex(_instance, 4, &bufPosition, position);
        buffers[0].pVertexData = bufPosition;
        buffers[0].sizVertexData = sizeof(position);
        buffers[0].sizVertexStride = sizeof(glm::vec3);

        topo_borrow_memory_ex(_instance, 4, &bufTexcoord, texcoord);
        buffers[1].pVertexData = bufTexcoord;
        buffers[1].sizVertexData = sizeof(texcoord);
        buffers[1].sizVertexStride = sizeof(glm::vec2);

        topo_borrow_memory_ex(_instance, 4, &bufTangent, tangent);
        buffers[2].pVertexData = bufTangent;
        buffers[2].sizVertexData = sizeof(tangent);
        buffers[2].sizVertexStride = sizeof(glm::vec3);

        topo_borrow_memory_ex(_instance, 4, &bufBitangent, bitangent);
        buffers[3].pVertexData = bufBitangent;
        buffers[3].sizVertexData = sizeof(bitangent);
        buffers[3].sizVertexStride = sizeof(glm::vec3);

        topo_borrow_memory_ex(_instance, 4, &bufNormal, normal);
        buffers[4].pVertexData = bufNormal;
        buffers[4].sizVertexData = sizeof(normal);
        buffers[4].sizVertexStride = sizeof(glm::vec3);

        // Attributes

        topo_borrow_memory_ex(_instance, 5, &attributes);
        descMesh->numAttributes = 5;
        descMesh->pAttributes = attributes;

        attributes[0].idxVertexBuffer = 0;
        attributes[0].offset = 0;
        attributes[0].semantic = kTopoVES_Position;
        attributes[0].type = kTopoVET_F32x3;

        attributes[1].idxVertexBuffer = 1;
        attributes[1].offset = 0;
        attributes[1].semantic = kTopoVES_Texcoord;
        attributes[1].type = kTopoVET_F32x2;

        attributes[2].idxVertexBuffer = 2;
        attributes[2].offset = 0;
        attributes[2].semantic = kTopoVES_Tangent;
        attributes[2].type = kTopoVET_F32x3;

        attributes[3].idxVertexBuffer = 3;
        attributes[3].offset = 0;
        attributes[3].semantic = kTopoVES_Bitangent;
        attributes[3].type = kTopoVET_F32x3;

        attributes[4].idxVertexBuffer = 4;
        attributes[4].offset = 0;
        attributes[4].semantic = kTopoVES_Normal;
        attributes[4].type = kTopoVET_F32x3;

        // Submeshes
        topo_borrow_memory_ex(_instance, 1, &submeshes);
        descMesh->numSubmeshes = 1;
        descMesh->pSubmeshes = submeshes;

        topo_borrow_memory_ex(_instance, 6, &bufIndex, index);
        submeshes[0].isIndexed = true;
        submeshes[0].material = _material;
        submeshes[0].primitiveTopology = kTopoPT_TriangleList;
        submeshes[0].indexBuffer.numIndices = 6;
        submeshes[0].indexBuffer.pIndices = bufIndex;
        submeshes[0].indexBuffer.type = topo_index_buffer::UINT16;

        _meshValid = true;
        topo_create_mesh(_instance, descMesh, &_mesh);
    }

    void Update(std::unique_ptr<IImmediateScene> &scene) {
        ZoneScoped;
        auto *surface = (ultralight::BitmapSurface *)(_view->surface());
        if (!surface->dirty_bounds().IsEmpty()) {
            Recreate(scene);
            surface->ClearDirtyBounds();
        }
    }

private:
    TopoInstance _instance;
    TopoImage _image = nullptr;
    TopoMaterial _material = nullptr;
    bool _meshValid = false;
    TopoMesh _mesh = nullptr;

    TopoTransform _transform;

    uuids::uuid_random_generator &_uuidGen;

    ultralight::Ref<ultralight::View> _view;
};

static void check_working_dir() {
    bool hasBin = false;
    bool hasAssets = false;
    auto cwd = std::filesystem::current_path();

    while (!hasBin || !hasAssets) {
        hasBin = false;
        hasAssets = false;
        for (const auto entry : std::filesystem::directory_iterator(cwd)) {
            if (entry.is_directory()) {
                if (entry.path().filename() == "bin") {
                    hasBin = true;
                }
                if (entry.path().filename() == "assets") {
                    hasAssets = true;
                }
            }
        }

        if (hasBin && hasAssets) {
            break;
        }

        auto parentPath = cwd.parent_path();
        if (cwd == parentPath) {
            fprintf(stderr, "Bad working directory\n");
            abort();
        }

        cwd = parentPath;
    }

    std::filesystem::current_path(cwd);
}

int main(int argc, char **argv) {
    std::random_device rd;
    auto seed_data = std::array<int, std::mt19937::state_size> {};
    std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    std::mt19937 generator(seq);
    uuids::uuid_random_generator genUuid { generator };

    check_working_dir();

    //

    TopoInstance instance;
    TopoInstanceDescriptor desc;
    desc.title = "Ultralight Demo";
    desc.window_width = 1280;
    desc.window_height = 720;
    desc.enable_validation = 1;
    if (topo_create(&instance, &desc) != Topo_OK) {
        return 1;
    }

    //
    ultralight::Config config;
    config.resource_path = "./assets/resources/";
    config.use_gpu_renderer = false;
    config.device_scale = 1.0;
    auto &platform = ultralight::Platform::instance();
    platform.set_config(config);
    platform.set_font_loader(ultralight::GetPlatformFontLoader());
    platform.set_file_system(ultralight::GetPlatformFileSystem("."));
    platform.set_logger(ultralight::GetDefaultLogger("ultralight.log"));
    auto renderer = ultralight::Renderer::Create();
    renderer->Update();
    renderer->Render();

    std::vector<BrowserWindow> windows;

    for (int i = 0; i < 4; i++) {
        auto view = renderer->CreateView(1280, 720, false, nullptr);
        view->LoadURL("https://easimer.net/homepage/index.html?ver=2");
        view->Focus();

        windows.push_back(BrowserWindow(instance, std::move(view), genUuid));
        windows.back().SetPosition(2 * i - 8, 0, 8.0f);
    }

    auto scene = makeDynScene(instance);

    TopoLightSource ambientLight = {};
    ambientLight.kind = kTopoLight_Ambient;
    ambientLight.intensity = 10;
    ambientLight.color[0] = 1;
    ambientLight.color[1] = 1;
    ambientLight.color[2] = 1;

    TopoCamera mainCamera = {};
    mainCamera.farClip = 16.0f;
    mainCamera.nearClip = 0.01f;
    mainCamera.fieldOfView = glm::radians(90.0f);
    memset(&mainCamera.transform, 0, sizeof(mainCamera.transform));
    mainCamera.transform.rotation[0] = 1.0f;
    mainCamera.transform.scale[0] = 1.0f;
    mainCamera.transform.scale[1] = 1.0f;
    mainCamera.transform.scale[2] = 1.0f;

    bool isExiting = false;
    while (!isExiting) {
        ZoneScoped;
        topo_exiting(instance, &isExiting);

        renderer->Update();

        if (scene->beginScene()) {
            renderer->Render();

            for (auto &window : windows) {
                window.Update(scene);
            }

            scene->beginObjectGroup();
            scene->addToObjectGroup(ambientLight);
            for (auto &window : windows) {
                window.AddToScene(scene);
            }
            scene->endObjectGroup();
            scene->addCamera(mainCamera);
            scene->setMainCamera(0);
            scene->submitScene();

			std::this_thread::sleep_for(std::chrono::milliseconds(16));
        } else {
			std::this_thread::sleep_for(std::chrono::milliseconds(32));
        }
    }

    scene.reset();

    topo_destroy(&instance);

    return 0;
}